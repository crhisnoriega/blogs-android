# Blogs MContigo

## Active blog flavors 
* MCS
* LMEM


## How to clone this project

```sh
$ git clone --recursive https://git.mcontigo.com/apps/blog-android.git
```


## Sync submodules

```sh
$ git submodule sync --recursive
```


