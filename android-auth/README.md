![MContigo](https://mcontigo.com/vendor/images/logo.png)

# Android Authentication Module
The purpose of this library is to facilitate the implementation of authentication.


## Tech

This module uses a number of open source projects:

* [RxJava](https://github.com/ReactiveX/RxJava)
* [Retrofit](https://square.github.io/retrofit/)
* [Google GSON](https://github.com/google/gson)

### How to implement
Import the *androiauthmodule* into your project after this, for you to access the library methods you need to create an instance AuthUtil

```gradle
//add depedency in your gradle app
def googleGson_version = "2.8.5"
implementation "com.google.code.gson:gson:$googleGson_version"
```

```kotlin
val authUtil = AuthUtil(context, your_url_app)
```

### Get token information

```kotlin
authUtil.getTokenAccess() //get the last token authentication in your app
authUtil.getLastLoginDate() //get the last date authentication

authUtil.getTypeLogin() // get the last type authentication

//constants types login
TypeAuth.EMAIL
TypeAuth.FACEBOOK
TypeAuth.GOOGLE
TypeAuth.BIOMETRIC
```


### Authentication

```kotlin
//EMAIL
authUtil.userEmailAuth(  
            yourHashMapHeader, //add here your HashMap<String,String>  
  user, //add here type UserBodySignIn or HashMap<String,String>  
  object :  
                AuthCallBack {  
                override fun onBegin() {  
                    //init process validation  
  }  
  
                override fun onSuccess(jsonObject: JsonObject) {  
                    //onSuccess return the jsonObject  
  }  
  
                override fun onFailed(error: String) {  
                    //onError return the error HttpException  
  }  
            })  
  
//FACEBOOK  
  authUtil.facebookAuth(  
            yourHashMapHeader, //add here your HashMap<String,String>  
  body, //add here type FacebookAuthBody or HashMap<String,String>  
  object :
  AuthCallBack {  
                override fun onBegin() {  
                    //init process validation  
  }  
  
                override fun onSuccess(jsonObject: JsonObject) {  
                    //onSuccess return the jsonObject  
  }  
  
                override fun onFailed(error: String) {  
                    //onError return the error HttpException  
  }  
            })  
  
//GOOGLE  
  authUtil.googleAuth(
			yourHashMapHeader, //add here your HashMap<String,String>  
            tokenIdGoogle, //add here TokenId returned by GoogleSDK  
  object : 
  AuthCallBack {  
                override fun onBegin() {  
                    //init process validation  
  }  
  
                override fun onSuccess(jsonObject: JsonObject) {  
                    //onSuccess return the jsonObject  
  }  
  
                override fun onFailed(error: String) {  
                    //onError return the error HttpException  
  }  
            })

//BIOMETRIC
/* if you use MVVM
We need to call in view, because Android 6.0 require view context, and in viewModel, we only have applicationContext, call in viewModel works only in Android P */
authUtil.biometricAuth(
            context = yourViewContext,
            title = "Title",
            cancelButtonText = "Cancel",
            callback = object : BiometricCallback {
                override fun onSdkVersionNotSupported() 
				{
                    Log.e(TAG, "no version sdk supported biometric")
                }
                override fun onBiometricAuthenticationNotSupported()
				{
                    Log.e(TAG, "no suported biometric")
                }
                override fun onBiometricAuthenticationNotAvailable() 
				{
                    Log.e(TAG, "no avaiable biometric")
                }
                override fun onBiometricAuthenticationPermissionNotGranted() 
                {
                    Log.e(TAG, "no permission granted")
                }
                override fun onBiometricAuthenticationInternalError(error: String)
                {
                    Log.e(TAG, "error: $error")
                }
                override fun onAuthenticationFailed() {
                    Log.d(TAG, "auth failed")
                }

                override fun onAuthenticationCancelled() {
                    Log.d(TAG, "cancelled auth biometric")
                }
                override fun onAuthenticationSuccessful() {
                    Log.d(TAG, "success auth biometric")
                }

                override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence) {
                    Log.d(TAG, "help auth biometric   helpCode: $helpCode  helpString: $helpString")

                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    Log.e(TAG, "error auth biometric   errorCode: $errorCode  errString: $errString")

                }
            })
```

### SignUp
```kotlin
authUtil.userEmailRegister(
	yourHashMapHeader, //add here your HashMap<String,String>
	body, //add here type UserSignUpBody or HashMap<String,String>
	object :  
	    AuthCallBack {  
	    override fun onBegin() {  
	        //init process signup  
	    }  
	  
	    override fun onSuccess(jsonObject: JsonObject) {  
	       //onSuccess return the jsonObject  
	    }  
	  
	    override fun onFailed(error: String) {  
	         //onFailed return the error HttpException  
	    }  
	}
)
```

### Get User

```kotlin
authUtil.getUser(
    yourHashMapHeaderWithUserResponseToken, //add here your HashMap<String,String>
    object :  
    UserCallBack {  
    override fun onBegin() {  
        //init process signup  
    }  
  
    override fun onSuccess(jsonObject: JsonObject) {  
		//return the jsonObject
    }  
  
    override fun onFailed(error: String) {  
         //onFailed return the error HttpException 
    }  
  
})
```


### Change Password
```kotlin
authUtil.changePassword(  
     yourHashMapHeaderWithUserResponseToken, //add here your HashMap<String,String>,  
  body, //add here type NewPasswordBody or HashMap<String,String>  
 object : CallBack {  
        override fun onBegin() {  
            //init
        }  
  
        override fun onSuccess(jsonObject: JsonObject) {  
           //success
        }  
  
        override fun onFailed(error: String) {  
         //onFailed return the error HttpException
        }  
  
    })
```


### For more examples and show use case
```
git clone https://git.mcontigo.com/libs/android-auth.git
```
Has a implemented example in this project.



