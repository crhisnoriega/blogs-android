package com.mcontigo.androidauthmodule

import android.util.Log
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.mcontigo.androidauthmodule.body.UserSignInBody
import com.mcontigo.androidauthmodule.callback.AuthCallBack
import com.mcontigo.androidauthmodule.model.UserResponse
import com.mcontigo.androidauthmodule.util.AuthUtil
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Before


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class EmailUnitTest {

    val TAG = "EmailUnitTest"

    val BASE_URL = "https://api.dev.lamenteesmaravillosa.com/"//"https://api.unycos.com/"

    val SECRET_KEY_APP_KEY = "x-mejor-key" //"unycos"
    val TOKEN_HEADER_KEY = "TokenHeader"
    val SECRET_KEY_APP_VALUE = "courses" //"unycos"
    val TOKEN_HEADER_VALUE = "H0y0y7jHEjgGSYvlLTmMIZg8CKH8xuGR"

    val HEADERMAP = hashMapOf(
        Pair(SECRET_KEY_APP_KEY,SECRET_KEY_APP_VALUE),
        Pair(TOKEN_HEADER_KEY,TOKEN_HEADER_VALUE)
    )


    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        Assert.assertEquals("com.mcontigo.authandroid", appContext.packageName)
    }



    @Test
    fun emailSignInSuccess(){

        val user = UserSignInBody(
            "agarcia@mcontigo.com", "12345678"
        )

        val appContext = InstrumentationRegistry.getTargetContext()

        val authUtil = AuthUtil(appContext , BASE_URL)

        authUtil.userEmailAuth(HEADERMAP, user, object :
            AuthCallBack {
            override fun onBegin() {
                Log.d(TAG, "init validation")
            }

            override fun onSuccess(userResponse: UserResponse) {
                Log.d(TAG, "$userResponse")
                Assert.assertEquals(true, true)

            }

            override fun onFailed(error: String) {
                Log.e(TAG, error)
                Assert.assertEquals(true, false)
            }
        })

        Assert.assertEquals(true, false)

    }



}
