package com.mcontigo.androidauthmodule

import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.mcontigo.androidauthmodule.auth.UserAuth
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

internal class AuthService(private val urlApp: String) {

    companion object {


        private fun provideRetrofit(baseUrl: String): Retrofit {

            val logging = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
                Log.i("AuthUtil", it)
            })
            logging.level = HttpLoggingInterceptor.Level.BODY

            val httpClient = OkHttpClient.Builder()
            httpClient.addInterceptor(logging)
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                .build()
        }
    }


    internal fun userAuth() = provideRetrofit(urlApp).create(UserAuth::class.java)


}