package com.mcontigo.androidauthmodule.auth

internal object TokenAccess {

    var tokenAccess : String? = null
    var loginType : String? = null
    var lastLoginDate : String? = null

    fun clearTokenAccess(){
        tokenAccess = null
        loginType = null
    }

}