package com.mcontigo.androidauthmodule.auth

import com.mcontigo.androidauthmodule.body.NewPasswordBody
import com.mcontigo.androidauthmodule.body.UserSignInBody
import com.mcontigo.androidauthmodule.body.UserSignUpBody
import com.mcontigo.androidauthmodule.body.FacebookAuthBody
import com.mcontigo.androidauthmodule.body.GoogleAuthBody
import com.mcontigo.androidauthmodule.model.Email
import com.mcontigo.androidauthmodule.model.Message
import com.mcontigo.androidauthmodule.model.User
import com.mcontigo.androidauthmodule.model.UserResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*


internal interface UserAuth {

    @POST("v2/auth/signin")
    fun signIn(@HeaderMap headerMap: Map<String, String>, @Body body: UserSignInBody): Observable<UserResponse>

    @POST("v2/auth/signin")
    fun signIn(@HeaderMap headerMap: Map<String, String>, @Body body: Map<String, String>): Observable<UserResponse>

    @POST("v2/auth/signup")
    fun signUp(@HeaderMap headerMap: Map<String, String>, @Body body: UserSignUpBody): Observable<UserResponse>

    @POST("v2/auth/signup")
    fun signUp(@HeaderMap headerMap: Map<String, String>, @Body body: Map<String, String>): Observable<UserResponse>

    @POST("auth/confirmation")
    fun confirmUser(@HeaderMap headerMap: Map<String, String>, @Body body: Map<String, String>): Observable<String>

    @GET("v2/users/me")
    fun getUser(@HeaderMap headerMap: Map<String, String>): Observable<User?>

    @PUT("v2/users/me")
    fun updateUser(@HeaderMap headerMap: Map<String, String>, @Body body: User): Observable<String>

    @POST("auth/forgot")
    fun forgotPassword(@HeaderMap headerMap: Map<String, String>, @Body body: String): Observable<String>

    @POST("auth/forgot")
    fun forgotPassword(@HeaderMap headerMap: Map<String, String>, @Body body: Email): Observable<Message>

    @POST("auth/reset/{token}")
    fun resetToken(@HeaderMap headerMap: Map<String, String>, @Path("token") tokenForgotPassword: String, @Body body: Map<String, String>): Observable<String>

    @GET("auth/reset/{token}")
    fun validateToken(@HeaderMap headerMap: Map<String, String>, @Path("token") tokenForgotPassword: String): Observable<String>

    @POST("v2/auth/facebook")
    fun facebookAuth(@HeaderMap headerMap: Map<String, String>, @Body body: FacebookAuthBody): Observable<UserResponse>

    @POST("v2/auth/facebook")
    fun facebookAuth(@HeaderMap headerMap: Map<String, String>, @Body body: Map<String, String>): Observable<UserResponse>

    @POST("v2/auth/google/mobile")
    fun googleAuth(@HeaderMap headerMap: Map<String, String>, @Body body: Map<String, String>): Observable<UserResponse>

    @POST("auth/logout")
    fun logout(@HeaderMap headerMap: Map<String, String>): Observable<String>

    @PUT("v2/users/me/password")
    fun changePassword(@HeaderMap headerMap: Map<String, String>,@Body body: NewPasswordBody): Observable<String>

    @PUT("v2/users/me/password")
    fun changePassword(@HeaderMap headerMap: Map<String, String>,@Body body: Map<String, String>) : Observable<String>

    @PUT("v2/users/me/photo")
    fun updateUserAvatar(@HeaderMap headerMap: Map<String, String>,@Body body: Map<String, String>) : Observable<String>


}
