package com.mcontigo.androidauthmodule.body

data class FacebookAuthBody(
    val access_token: String? = "",
    val code: String? = "",
    val redirectUri: String? = "",
    val token_type: String? = "",
    val expires_in: String? = ""
)