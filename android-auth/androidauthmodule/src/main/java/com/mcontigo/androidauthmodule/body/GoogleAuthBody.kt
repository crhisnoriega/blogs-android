package com.mcontigo.androidauthmodule.body

data class GoogleAuthBody(
    val access_token: String? = "",
    val clientId: String? = "",
    val code: String? = "",
    val redirectUri: String? = ""
)