package com.mcontigo.androidauthmodule.body

data class NewPasswordBody(
    val currentPassword: String,
    val newPassword: String
)