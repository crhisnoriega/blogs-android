package com.mcontigo.androidauthmodule.body

data class UserSignInBody(
    var email : String,
    var password: String
)
