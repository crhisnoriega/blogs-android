package com.mcontigo.androidauthmodule.body

data class UserSignUpBody(
        val displayName: String,
        val email: String,
        val password: String,
        val gdpr: Boolean = false


)