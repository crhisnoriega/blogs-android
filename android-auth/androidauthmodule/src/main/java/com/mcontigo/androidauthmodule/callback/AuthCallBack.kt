package com.mcontigo.androidauthmodule.callback

import com.mcontigo.androidauthmodule.model.UserResponse

interface AuthCallBack {
    fun onSuccess(userResponse: UserResponse)


    fun onFailed(error: String)

    fun onBegin()
}