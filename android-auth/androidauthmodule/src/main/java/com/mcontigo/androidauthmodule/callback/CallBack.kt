package com.mcontigo.androidauthmodule.callback

interface CallBack {

    fun onBegin()

    fun onSuccess(msg : String = "")

    fun onFailed(error: String)

}