package com.mcontigo.androidauthmodule.callback

import com.mcontigo.androidauthmodule.model.User

interface UserCallBack {

    fun onBegin()

    fun onSuccess(user : User)

    fun onFailed(error : String)
}