package com.mcontigo.androidauthmodule.model

data class ActiveSession(
    val _id: String? = "",
    val created: String? = "",
    val isAdminLogin: Boolean? = false,
    val last_activity: String? = "",
    val location: Location? = Location(),
    val provider: String? = "",
    val token: String? = "",
    val user_agent: UserAgent? = UserAgent()
)