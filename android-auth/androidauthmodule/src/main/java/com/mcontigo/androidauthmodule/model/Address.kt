package com.mcontigo.androidauthmodule.model
import com.google.gson.annotations.SerializedName
data class Address(
    @SerializedName("billing")
    var billing: Billing? = Billing()
)
data class Billing(
    @SerializedName("city")
    var city: String? = "",
    @SerializedName("country")
    var country: String? = "",
    @SerializedName("province")
    var province: String? = "",
    @SerializedName("street")
    var street: String? = "",
    @SerializedName("zip_code")
    var zipCode: String? = ""
)