package com.mcontigo.androidauthmodule.model

data class CallNotification(
    val general: Boolean? = false
)
