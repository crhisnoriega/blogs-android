package com.mcontigo.androidauthmodule.model


data class Configuration(
    val call_notification: CallNotification? = CallNotification(),
    val email_notification: EmailNotification? = EmailNotification(),
    val push_notification: PushNotification? = PushNotification()
)