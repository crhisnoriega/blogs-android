package com.mcontigo.androidauthmodule.model

data class DevicesAllowed(
    val _id: String? = "",
    val browser: String? = "",
    val os: String? = ""
)