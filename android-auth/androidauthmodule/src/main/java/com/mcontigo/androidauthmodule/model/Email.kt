package com.mcontigo.androidauthmodule.model


import com.google.gson.annotations.SerializedName

data class Email(
    @SerializedName("email")
    var email: String? = ""
)