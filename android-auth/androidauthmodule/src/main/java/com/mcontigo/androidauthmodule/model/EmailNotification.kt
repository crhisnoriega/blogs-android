package com.mcontigo.androidauthmodule.model

data class EmailNotification(
    val forum_digest: Boolean? = false,
    val homework_reviewed: Boolean? = false,
    val login: Boolean? = false,
    val marketing_permissions: Boolean? = false,
    val new_certificate: Boolean? = false,
    val new_coupon: Boolean? = false,
    val new_course: Boolean? = false,
    val new_enrollment: Boolean? = false,
    val new_invoice: Boolean? = false,
    val new_notification: Boolean? = false,
    val new_payment: Boolean? = false,
    val new_review: Boolean? = false,
    val offers: Boolean? = false,
    val payment_pending: Boolean? = false,
    val pending_notifications: Boolean? = false,
    val subscription: Boolean? = false,
    val user_interaction_in_progress: Boolean? = false,
    val user_interaction_not_started: Boolean? = false
)