package com.mcontigo.androidauthmodule.model

data class Location(
    val city: String? = "",
    val country: String? = "",
    val ip: String? = ""
)