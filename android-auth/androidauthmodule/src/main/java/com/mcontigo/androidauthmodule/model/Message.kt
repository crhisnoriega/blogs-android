package com.mcontigo.androidauthmodule.model


import com.google.gson.annotations.SerializedName

data class Message(
    @SerializedName("message")
    var message: String? = ""
)