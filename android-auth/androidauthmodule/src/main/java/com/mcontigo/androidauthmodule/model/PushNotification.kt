package com.mcontigo.androidauthmodule.model

data class PushNotification(
    val activities: Boolean? = false,
    val messages: Boolean? = false,
    val new_certificate: Boolean? = false,
    val new_coupon: Boolean? = false,
    val new_course: Boolean? = false,
    val reservations: Boolean? = false
)