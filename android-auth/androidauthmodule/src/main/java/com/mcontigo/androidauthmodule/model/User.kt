package com.mcontigo.androidauthmodule.model

data class User(
        var __v: Int? = 0,
        var _id: String? = "",
        var active: Boolean? = false,
        var active_sessions: List<ActiveSession?>? = listOf(),
        var configuration: Configuration? = Configuration(),
        var created: String? = "",
        var currency: String? = "",
        var currentApps: CurrentApps? = CurrentApps(),
        var devices_allowed: List<DevicesAllowed?>? = listOf(),
        var displayName: String? = "",
        var email: String? = "",
        var email_verified: Boolean? = false,
        var firstName: String? = "",
        var lastName: String? = "",
        var followed_list: List<Any?>? = listOf(),
        var followers_list: List<Any?>? = listOf(),
        var gender: String? = "",
        var id_list: List<Any?>? = listOf(),
        var last_ip: String? = "",
        var merges: List<Any?>? = listOf(),
        var my_services: List<Any?>? = listOf(),
        var onesignal: List<Any?>? = listOf(),
        var payment_methods: List<Any?>? = listOf(),
        var phone_list: List<Any?>? = listOf(),
        var providers: Providers? = Providers(),
        var public_profile: Boolean? = false,
        var pushDeviceIds: List<Any?>? = listOf(),
        var reason_leaving: List<Any?>? = listOf(),
        var reviews: List<Any?>? = listOf(),
        var roles: List<Any?>? = listOf(),
        var section_history: List<Any?>? = listOf(),
        var subscribe: String? = "",
        var test_user: Boolean? = false,
        var username: String? = "",
        var verified: Boolean? = false,
        var wishes: Wishes? = Wishes(),


        var picture: String? = "",
        var address: Address? = null,

        var birth_date: String? = ""

)