package com.mcontigo.androidauthmodule.model


data class UserAgent(
    val browser: String? = "",
    val os: String? = ""
)