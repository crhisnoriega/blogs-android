package com.mcontigo.androidauthmodule.model

data class UserResponse(
    val token: String? = "",
    val user: User? = User()
)