package com.mcontigo.androidauthmodule.model

data class Wishes(
    val courses: List<Any?>? = listOf(),
    val services: List<Any?>? = listOf()
)