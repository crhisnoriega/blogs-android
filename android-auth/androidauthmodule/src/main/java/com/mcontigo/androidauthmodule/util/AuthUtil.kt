package com.mcontigo.androidauthmodule.util

import android.content.Context
import android.util.Base64
import android.util.JsonReader
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.mcontigo.androidauthmodule.AuthService
import com.mcontigo.androidauthmodule.auth.TokenAccess
import com.mcontigo.androidauthmodule.body.NewPasswordBody
import com.mcontigo.androidauthmodule.body.UserSignInBody
import com.mcontigo.androidauthmodule.body.UserSignUpBody
import com.mcontigo.androidauthmodule.callback.AuthCallBack
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException


import com.mcontigo.androidauthmodule.biometric.BiometricCallback
import com.mcontigo.androidauthmodule.biometric.BiometricManager
import com.mcontigo.androidauthmodule.body.FacebookAuthBody
import com.mcontigo.androidauthmodule.callback.CallBack
import com.mcontigo.androidauthmodule.callback.UserCallBack
import com.mcontigo.androidauthmodule.model.Email
import com.mcontigo.androidauthmodule.model.User
import com.mcontigo.androidauthmodule.model.UserResponse
import io.reactivex.disposables.CompositeDisposable
import java.lang.Exception


class AuthUtil(val context: Context, val urlApp: String) {

    private val TAG = this@AuthUtil.javaClass.simpleName
    private val compositeDisposable = CompositeDisposable()


    init {
        TokenAccess.tokenAccess = ManagerTokenAccess.getTokenAccess(context)
        TokenAccess.loginType = ManagerTokenAccess.getLoginType(context)
    }

    fun getTypeLogin() = TokenAccess.loginType
    fun getLastLoginDate() = TokenAccess.lastLoginDate
    fun getTokenAccess() = TokenAccess.tokenAccess

    private fun updateTokenAccess(userResponse: UserResponse, typeLogin: String) {
        TokenAccess.tokenAccess = userResponse.token
        TokenAccess.loginType = typeLogin
        TokenAccess.lastLoginDate = DateTimeUtil.getDateAtualFormated()

        ManagerTokenAccess.saveLoginType(context, typeLogin)
        ManagerTokenAccess.saveLastDateLogin(context, DateTimeUtil.getDateAtualFormated())
        ManagerTokenAccess.saveUserAccess(context, userResponse.user?.email
                ?: "", userResponse.token ?: "")

    }

    fun logout() {
        ManagerTokenAccess.clearAccessSaved(context)
        TokenAccess.clearTokenAccess()
    }

    fun userEmailAuth(header: HashMap<String, String>, user: UserSignInBody, callBack: AuthCallBack) {
        compositeDisposable.add(

                AuthService(urlApp).userAuth().signIn(header, user)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ userResponse ->
                            Log.d(TAG, "$userResponse")

                            updateTokenAccess(userResponse, TypeAuth.EMAIL)

                            callBack.onSuccess(userResponse)
                            compositeDisposable.clear()


                        }, { throwable ->

                            Log.d(TAG, throwable.javaClass.canonicalName);
                            if (throwable is com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
                                var errorBody1 = throwable.response().errorBody()?.string()
                                var message = JsonParser().parse(errorBody1).asJsonObject.get("message");
                                callBack.onFailed(message.asString)
                                compositeDisposable.clear()
                            } else {
                                Log.e(TAG, "$throwable")
                                callBack.onFailed(throwable.toString())
                                compositeDisposable.clear()
                            }
                        })


        )
    }


    fun userEmailAuth(header: HashMap<String, String>, user: HashMap<String, String>, callBack: AuthCallBack) {
        compositeDisposable.add(


                AuthService(urlApp).userAuth().signIn(header, user)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ userResponse ->
                            Log.d(TAG, "$userResponse")

                            updateTokenAccess(userResponse, TypeAuth.EMAIL)

                            callBack.onSuccess(userResponse)
                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.e(TAG, "$throwable")
                            callBack.onFailed(throwable.toString())
                            compositeDisposable.clear()
                        })
        )
    }


    fun facebookAuth(header: HashMap<String, String>, facebookAuthBody: FacebookAuthBody, callBack: AuthCallBack) {
        compositeDisposable.add(
                AuthService(urlApp).userAuth().facebookAuth(header, facebookAuthBody)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ userResponse ->
                            Log.d(TAG, "$userResponse")
                            updateTokenAccess(userResponse, TypeAuth.FACEBOOK)
                            callBack.onSuccess(userResponse)
                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.e(TAG, "$throwable")
                            callBack.onFailed(throwable.toString())
                            compositeDisposable.clear()
                        })

        )
    }

    fun facebookAuth(

            header: HashMap<String, String>,
            facebookAuthBody: HashMap<String, String>,
            callBack: AuthCallBack
    ) {
        compositeDisposable.add(
                AuthService(urlApp).userAuth().facebookAuth(header, facebookAuthBody)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ userResponse ->
                            Log.d(TAG, "$userResponse")
                            updateTokenAccess(userResponse, TypeAuth.FACEBOOK)
                            callBack.onSuccess(userResponse)
                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.e(TAG, "$throwable")
                            callBack.onFailed(throwable.toString())
                            compositeDisposable.clear()
                        })
        )
    }


    fun googleAuth(header: HashMap<String, String>, tokenId: String, callBack: AuthCallBack) {

        var body = HashMap<String, String>();
        body.put("id_token", tokenId);
        compositeDisposable.add(
                AuthService(urlApp).userAuth().googleAuth(header, body)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ userResponse ->
                            updateTokenAccess(userResponse, TypeAuth.GOOGLE)
                            callBack.onSuccess(userResponse)
                            compositeDisposable.clear()
                        }, { throwable ->
                            Log.d(TAG, throwable.javaClass.canonicalName);
                            if (throwable is com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
                                var errorBody1 = throwable.response().errorBody()?.string()
                                var message = JsonParser().parse(errorBody1).asJsonObject.get("message");
                                callBack.onFailed(message.asString)
                                compositeDisposable.clear()
                            } else {
                                Log.e(TAG, "$throwable")
                                callBack.onFailed(throwable.toString())
                                compositeDisposable.clear()
                            }
                        })

        )
    }


    fun updateUser(header: HashMap<String, String>, user: User , callBack: CallBack) {
        compositeDisposable.add(
            AuthService(urlApp).userAuth().updateUser(header, user)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { callBack.onBegin() }
                .subscribe({ user ->
                    Log.d(TAG, "$user")
                    user?.let { callBack.onSuccess(it) } ?: run {
                        callBack.onFailed(
                            "error while fetching user"
                        )
                    }
                    compositeDisposable.clear()

                }, { throwable ->
                    Log.d(TAG, throwable.javaClass.canonicalName);
                    if (throwable is com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
                        var errorBody1 = throwable.response().errorBody()?.string()
                        callBack.onFailed(errorBody1.toString())
                        compositeDisposable.clear()
                    } else {
                        Log.e(TAG, "$throwable")
                        callBack.onFailed(throwable.toString())
                        compositeDisposable.clear()
                    }

                    if (throwable is HttpException) {
                        var errorBody1 = throwable.response().errorBody()?.string()
                        callBack.onFailed(errorBody1.toString())
                        compositeDisposable.clear()
                    } else {
                        Log.e(TAG, "$throwable")
                        callBack.onFailed(throwable.toString())
                        compositeDisposable.clear()
                    }
                })

        )
    }


    fun getUser(header: HashMap<String, String>, callBack: UserCallBack) {
        compositeDisposable.add(
                AuthService(urlApp).userAuth().getUser(header)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ user ->
                            Log.d(TAG, "$user")
                            user?.let { callBack.onSuccess(it) } ?: run {
                                callBack.onFailed(
                                        "error while fetching user"
                                )
                            }
                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.d(TAG, throwable.javaClass.canonicalName);
                            if (throwable is com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
                                var errorBody1 = throwable.response().errorBody()?.string()
                                callBack.onFailed(errorBody1.toString())
                                compositeDisposable.clear()
                            } else {
                                Log.e(TAG, "$throwable")
                                callBack.onFailed(throwable.toString())
                                compositeDisposable.clear()
                            }

                            if (throwable is HttpException) {
                                var errorBody1 = throwable.response().errorBody()?.string()
                                callBack.onFailed(errorBody1.toString())
                                compositeDisposable.clear()
                            } else {
                                Log.e(TAG, "$throwable")
                                callBack.onFailed(throwable.toString())
                                compositeDisposable.clear()
                            }
                        })

        )
    }


    fun userEmailRegister(header: HashMap<String, String>, user: UserSignUpBody, callBack: AuthCallBack) {

        Log.d(TAG, urlApp);
        Log.d(TAG, header.toString());
        Log.d(TAG, header.toString());

        compositeDisposable.add(
                AuthService(urlApp).userAuth().signUp(header, user)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ userResponse ->
                            Log.d(TAG, "$userResponse")

                            updateTokenAccess(userResponse, TypeAuth.EMAIL)
                            val userLogged = UserSignInBody(user.email, user.password)

                            callBack.onSuccess(userResponse)

                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.d(TAG, throwable.javaClass.canonicalName);
                            if (throwable is com.jakewharton.retrofit2.adapter.rxjava2.HttpException) {
                                var message : String? = null
                                try {
                                    val errorBody1 = throwable.response().errorBody()?.string()
                                    message =
                                        JsonParser().parse(errorBody1).asJsonObject.get("message").asString
                                    callBack.onFailed(message)
                                }catch (e : Exception){
                                    message = e.message.toString()
                                    callBack.onFailed(message)
                                }
                                compositeDisposable.clear()
                            } else {
                                Log.e(TAG, "$throwable")
                                callBack.onFailed(throwable.toString())
                                compositeDisposable.clear()
                            }


                        })

        )
    }

    fun userEmailRegister(header: HashMap<String, String>, body: HashMap<String, String>, callBack: AuthCallBack) {
        compositeDisposable.add(

                AuthService(urlApp).userAuth().signUp(header, body)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({ userResponse ->
                            Log.d(TAG, "$userResponse")

                            updateTokenAccess(userResponse, TypeAuth.EMAIL)

                            callBack.onSuccess(userResponse)

                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.e(TAG, "$throwable")
                            callBack.onFailed(throwable.toString())
                            compositeDisposable.clear()
                        })

        )
    }

    fun forgotPassword(header: HashMap<String, String>, email: String, callBack: CallBack) {
        compositeDisposable.add(

                AuthService(urlApp).userAuth().forgotPassword(header, email)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({
                            Log.d(TAG, it)

                            callBack.onSuccess(it)
                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.e(TAG, "$throwable")
                            callBack.onFailed(throwable.toString())
                            compositeDisposable.clear()
                        })
        )
    }


    fun forgotPasswordMessage(header: HashMap<String, String>, email: String, callBack: CallBack) {
        compositeDisposable.add(

            AuthService(urlApp).userAuth().forgotPassword(header, Email(email))
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { callBack.onBegin() }
                .subscribe({
                    Log.d(TAG, it.message)

                    callBack.onSuccess(it.message ?: "")
                    compositeDisposable.clear()

                }, { throwable ->
                    Log.e(TAG, "$throwable")
                    callBack.onFailed(throwable.toString())
                    compositeDisposable.clear()
                })
        )
    }


    fun changePassword(header: HashMap<String, String>, body: NewPasswordBody, callBack: CallBack) {
        compositeDisposable.add(

                AuthService(urlApp).userAuth().changePassword(header, body)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({
                            Log.d(TAG, it)
                            callBack.onSuccess(it)
                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.e(TAG, "$throwable")
                            callBack.onFailed(throwable.toString())
                            compositeDisposable.clear()
                        })
        )
    }


    fun changePassword(header: HashMap<String, String>, body: HashMap<String, String>, callBack: CallBack) {
        compositeDisposable.add(
                AuthService(urlApp).userAuth().changePassword(header, body)
                        .subscribeOn(Schedulers.io())
                        .doOnSubscribe { callBack.onBegin() }
                        .subscribe({
                            Log.d(TAG, it)
                            callBack.onSuccess(it)
                            compositeDisposable.clear()

                        }, { throwable ->
                            Log.e(TAG, "$throwable")
                            callBack.onFailed(throwable.toString())
                            compositeDisposable.clear()
                        })
        )
    }

    fun updateUserAvatar(header: HashMap<String, String>, body: ByteArray, callBack: CallBack){

        val imgBase64String = Base64.encodeToString(body,Base64.DEFAULT)

        val map = mutableMapOf<String,String>()
        map["picture"] = imgBase64String

        compositeDisposable.add(
            AuthService(urlApp).userAuth().updateUserAvatar(header, map)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { callBack.onBegin() }
                .subscribe({
                    Log.d(TAG, it)
                    callBack.onSuccess(it)
                    compositeDisposable.clear()

                }, { throwable ->
                    Log.e(TAG, "$throwable")
                    callBack.onFailed(throwable.toString())
                    compositeDisposable.clear()
                })
        )
    }

    fun confirmUser(header: HashMap<String, String>, email: String , callBack: CallBack){

        val map = mutableMapOf<String,String>()
        map["email"] = email

        compositeDisposable.add(
            AuthService(urlApp).userAuth().confirmUser(header, map)
                .subscribeOn(Schedulers.io())
                .doOnSubscribe { callBack.onBegin() }
                .subscribe({
                    Log.d(TAG, it)
                    callBack.onSuccess(it)
                    compositeDisposable.clear()

                }, { throwable ->
                    Log.e(TAG, "$throwable")
                    callBack.onFailed(throwable.toString())
                    compositeDisposable.clear()
                })
        )
    }


    fun biometricAuth(
             context: Context,
            title: String,
            subtitle: String = "",
            description: String = "",
            cancelButtonText: String,
            callback: BiometricCallback
    ) {
        BiometricManager.BiometricBuilder(context)
                .setTitle(title)
                .setSubtitle(subtitle)
                .setDescription(description)
                .setNegativeButtonText(cancelButtonText)
                .build()
                .authenticate(callback)

    }
}