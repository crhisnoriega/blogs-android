package com.mcontigo.androidauthmodule.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

internal object DateTimeUtil{

    private val simpleDateFormat = SimpleDateFormat()

    fun getDateForXLabels(milliseconds: Long): String {

        simpleDateFormat.applyPattern("HH:mm")
        simpleDateFormat.timeZone = TimeZone.getDefault()

        return simpleDateFormat.format(convertMilli(milliseconds))
    }


    fun getMilliToDateREST(milliseconds: Long): String {

        simpleDateFormat.applyPattern("yyyy-MM-dd HH:mm:ss")
        simpleDateFormat.timeZone = TimeZone.getDefault()

        return simpleDateFormat.format(convertMilli(milliseconds))
    }


    fun dateToEpochFromServer(date: String): Long {

        try {
            simpleDateFormat.applyPattern("dd/MM/yyyy HH:mm:ss")
            return simpleDateFormat.parse(date).time / 1000
        } catch (e: ParseException) {

            e.printStackTrace()
        }

        return 0
    }

    fun getDateMinusHour(hours: Int): Long {

        val calendar = Calendar.getInstance()

        return calendar.timeInMillis / 1000 - hours * 3600
    }

    private fun convertMilli(milliseconds: Long): Long {

        return Date(milliseconds).time * 1000
    }

    fun getDateAtual(): Long {
        return Calendar.getInstance().timeInMillis / 1000
    }

    fun getDateAtualFormated(): String {
        return  getMilliToDateREST(getDateAtual())
    }

}