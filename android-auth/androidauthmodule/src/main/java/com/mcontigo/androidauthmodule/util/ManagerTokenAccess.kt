package com.mcontigo.androidauthmodule.util

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.mcontigo.androidauthmodule.body.UserSignInBody


internal object ManagerTokenAccess {

    private val APP_TOKEN_LIB_PREFERENCES = "libAuth"
    private val USER = "user"
    private val TOKEN = "token"
    private val TYPE_LOGIN = "loginType"
    private val DATE_LAST_LOGIN = "lastLogin"

    fun saveUserAccess(context: Context, user: String, token: String) {

        val sharedPreferences = context.getSharedPreferences(
            APP_TOKEN_LIB_PREFERENCES,
            Context.MODE_PRIVATE
        )

        val editDados = sharedPreferences.edit()
        editDados.putString(USER, user)
        editDados.putString(TOKEN, token)
        editDados.apply()

    }

    fun getTokenAccess(context: Context): String? {

        val sharedPreferences = context.getSharedPreferences(
            APP_TOKEN_LIB_PREFERENCES,
            Context.MODE_PRIVATE
        )

        val token = sharedPreferences.getString(TOKEN, "")

        return if (token.isNullOrEmpty())
            null
        else
            token


    }


    fun saveLastDateLogin(context: Context, date: String) {

        val sharedPreferences = context.getSharedPreferences(
            APP_TOKEN_LIB_PREFERENCES,
            Context.MODE_PRIVATE
        )

        val editDados = sharedPreferences.edit()
        editDados.putString(DATE_LAST_LOGIN, date)
        editDados.apply()

    }

    fun getLastDateLogin(context: Context): String? {

        val sharedPreferences = context.getSharedPreferences(
            APP_TOKEN_LIB_PREFERENCES,
            Context.MODE_PRIVATE
        )


        return sharedPreferences.getString(DATE_LAST_LOGIN, "")

    }


    fun saveLoginType(context: Context, type: String) {

        val sharedPreferences = context.getSharedPreferences(
            APP_TOKEN_LIB_PREFERENCES,
            Context.MODE_PRIVATE
        )

        val editDados = sharedPreferences.edit()
        editDados.putString(TYPE_LOGIN, type)
        editDados.apply()

    }

    fun getLoginType(context: Context): String? {

        val sharedPreferences = context.getSharedPreferences(
            APP_TOKEN_LIB_PREFERENCES,
            Context.MODE_PRIVATE
        )


        return sharedPreferences.getString(TYPE_LOGIN, "")

    }

    fun clearAccessSaved(context: Context) {
        val sharedPreferences = context.getSharedPreferences(
            APP_TOKEN_LIB_PREFERENCES,
            Context.MODE_PRIVATE
        )

        val editDados = sharedPreferences.edit()
        editDados.clear()
        editDados.apply()
    }


}