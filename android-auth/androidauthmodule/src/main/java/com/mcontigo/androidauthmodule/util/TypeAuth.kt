package com.mcontigo.androidauthmodule.util

object TypeAuth{
    val FACEBOOK = "facebook"
    val EMAIL = "email"
    val GOOGLE = "google"
    val BIOMETRIC = "biometric"
}