package com.mcontigo.authandroid.configuration

import com.mcontigo.authandroid.BuildConfig

object ConfigurationLibAuth {

    val BASE_URL = "https://api.dev.lamenteesmaravillosa.com/"//"https://api.unycos.com/"

    val SECRET_KEY_MEJOR_KEY = "x-mejor-key" //"unycos"
    val SECRET_KEY_MEJOR_VALUE = "courses" //"unycos"
    val SECRET_KEY_APP_KEY = "x-mejor-app"
    val SECRET_KEY_APP_VALUE = "mobile_courses"
    val APP_VERSION_KEY = "x-app-version"
    val APP_VERSION_VALUE = BuildConfig.VERSION_NAME



    fun getHeaderWithAuthorizationToken(token: String) = hashMapOf(
        Pair(SECRET_KEY_MEJOR_KEY, SECRET_KEY_MEJOR_VALUE),
        Pair("Authorization", "Mejor $token"),
        Pair(SECRET_KEY_APP_KEY, SECRET_KEY_APP_VALUE),
        Pair(APP_VERSION_KEY, APP_VERSION_VALUE)
    )

    fun getHeaderDefault() = hashMapOf(
        Pair(SECRET_KEY_MEJOR_KEY, SECRET_KEY_MEJOR_VALUE),
        Pair(SECRET_KEY_APP_KEY, SECRET_KEY_APP_VALUE),
        Pair(APP_VERSION_KEY, APP_VERSION_VALUE)
    )


}