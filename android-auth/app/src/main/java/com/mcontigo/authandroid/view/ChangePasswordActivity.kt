package com.mcontigo.authandroid.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.mcontigo.authandroid.R
import com.mcontigo.authandroid.databinding.ActivityChangePasswordBinding
import com.mcontigo.authandroid.viewmodel.ChangePasswordViewModel

class ChangePasswordActivity : AppCompatActivity() {

    lateinit var viewModel : ChangePasswordViewModel
    lateinit var binding : ActivityChangePasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password)
        viewModel = ViewModelProviders.of(this).get(ChangePasswordViewModel::class.java)

        binding.viewModel = viewModel



    }
}
