package com.mcontigo.authandroid.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.mcontigo.authandroid.R
import com.mcontigo.authandroid.databinding.ActivityMainBinding
import com.mcontigo.authandroid.viewmodel.MainViewModel
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.mcontigo.androidauthmodule.biometric.BiometricCallback


class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"

    val RC_SIGN_IN = 905

    lateinit var viewModel: MainViewModel
    lateinit var binding: ActivityMainBinding
    lateinit var callbackManager: CallbackManager
    lateinit var googleSignIn: GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = viewModel
        lifecycle.addObserver(viewModel)

        callbackManager = CallbackManager.Factory.create()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken("797135710900-amqftvnuo3efjme75defnilqdma03dkn.apps.googleusercontent.com")
            .requestEmail()
            .build()

        googleSignIn = GoogleSignIn.getClient(this, gso);

        viewModel.successLogin.observe(this, Observer {
            if (it) {
                Toast.makeText(this@MainActivity, "Success", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this@MainActivity, viewModel.errorMsg, Toast.LENGTH_SHORT).show()
            }
        })


        viewModel.authBiometric.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (viewModel.authBiometric.get()) {
                    callAuthBiometricScreen()
                    viewModel.authBiometric.set(false)
                }
            }

        })



        binding.btnRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        binding.btnFacebook.setReadPermissions("email")

        binding.btnFacebook.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    viewModel.authFacebook(loginResult.accessToken)
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    Log.e(TAG, exception.toString())
                }
            })

        binding.btnGoogle.setSize(SignInButton.SIZE_STANDARD)

        binding.btnGoogle.setOnClickListener {
            val signInIntent = googleSignIn.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        binding.btnChangePassword.setOnClickListener {
            val intent = Intent(this, ChangePasswordActivity::class.java)
            startActivity(intent)
        }
    }


    fun callAuthBiometricScreen() {
        // if you use MVVM
        //We need to call in view, because Android 6.0 require view context, and in viewModel, we only have applicationContext, call in viewModel works only in Android P
        viewModel.authUtil.biometricAuth(
            context = this@MainActivity,
            title = "Teste",
            cancelButtonText = "Cancelar",
            callback = object : BiometricCallback {
                override fun onSdkVersionNotSupported() {
                    Log.e(TAG, "no version sdk supported biometric")
                }

                override fun onBiometricAuthenticationNotSupported() {
                    Log.e(TAG, "no suported biometric")
                }

                override fun onBiometricAuthenticationNotAvailable() {
                    Log.e(TAG, "no avaiable biometric")
                }

                override fun onBiometricAuthenticationPermissionNotGranted() {
                    Log.e(TAG, "no permission granted")
                }

                override fun onBiometricAuthenticationInternalError(error: String) {

                    Log.e(TAG, "error: $error")

                }

                override fun onAuthenticationFailed() {

                    Log.d(TAG, "cancelled auth failed")

                }

                override fun onAuthenticationCancelled() {

                    Log.d(TAG, "cancelled auth biometric")
                    finish()

                }

                override fun onAuthenticationSuccessful() {
                    Log.d(TAG, "success auth biometric")
                    viewModel.getUser()


                }

                override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence) {
                    Log.d(TAG, "help auth biometric   helpCode: $helpCode  helpString: $helpString")

                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    Log.e(TAG, "error auth biometric   errorCode: $errorCode  errString: $errString")

                }


            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }


        super.onActivityResult(requestCode, resultCode, data)
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
//        try {
//            val account = completedTask.getResult(ApiException::class.java)
//            account?.let { viewModel.googleAuth(it)
//            }
//        } catch (e: ApiException) {
//            // The ApiException status code indicates the detailed failure reason.
//            // Please refer to the GoogleSignInStatusCodes class reference for more information.
//            Log.e(TAG, e.toString())
//            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
//        }

    }


}
