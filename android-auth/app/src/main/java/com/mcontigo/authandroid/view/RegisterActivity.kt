package com.mcontigo.authandroid.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mcontigo.authandroid.R
import com.mcontigo.authandroid.databinding.ActivityRegisterBinding
import com.mcontigo.authandroid.viewmodel.RegisterViewModel

class RegisterActivity : AppCompatActivity() {

    lateinit var viewModel: RegisterViewModel
    lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)
        binding.viewModel = viewModel

        viewModel.success.observe(this, Observer {
            if (it) {
                Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, viewModel.errorMsg, Toast.LENGTH_SHORT).show()
            }
        })

    }
}
