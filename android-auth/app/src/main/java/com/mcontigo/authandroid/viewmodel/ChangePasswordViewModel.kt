package com.mcontigo.authandroid.viewmodel

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.google.gson.JsonObject
import com.mcontigo.androidauthmodule.body.NewPasswordBody
import com.mcontigo.androidauthmodule.callback.JsonObjectResponseCallback
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.authandroid.configuration.ConfigurationLibAuth

class ChangePasswordViewModel(application: Application) : AndroidViewModel(application) {

    val TAG = "ChangePasswordViewModel"

    val authUtil = AuthUtil(getApplication(), ConfigurationLibAuth.BASE_URL)

    val oldPassword = ObservableField("")
    val newPassword = ObservableField("")
    val confirmPassword = ObservableField("")

    fun resetPassword() {

        val newPasswordBody = NewPasswordBody(
            oldPassword.get() ?: "", newPassword.get() ?: ""
        )

        authUtil.changePassword(
            ConfigurationLibAuth.getHeaderWithAuthorizationToken(authUtil.getTokenAccess() ?: ""),
            newPasswordBody,
            object : JsonObjectResponseCallback {
                override fun onBegin() {
                    Log.d(TAG, "init")
                }

                override fun onSuccess(msg: JsonObject) {
                    Log.d(TAG, "sucesso")
                }

                override fun onFailed(error: String) {
                    Log.e(TAG, error)
                }

            })
    }

}