package com.mcontigo.authandroid.viewmodel

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.facebook.AccessToken
import com.google.gson.JsonObject
import com.mcontigo.androidauthmodule.body.FacebookAuthBody
import com.mcontigo.androidauthmodule.body.UserSignInBody
import com.mcontigo.androidauthmodule.callback.JsonObjectResponseCallback
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.androidauthmodule.util.TypeAuth
import com.mcontigo.authandroid.configuration.ConfigurationLibAuth

class MainViewModel(application: Application) : AndroidViewModel(application), LifecycleObserver {

    val TAG = "MainViewModel"
    val email = ObservableField("")
    val password = ObservableField("")
    val successLogin = MutableLiveData<Boolean>()
    var errorMsg = ""
    val logged = ObservableBoolean(false)
    val authUtil = AuthUtil(getApplication(), ConfigurationLibAuth.BASE_URL)

    val authBiometric = ObservableBoolean(false)


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        if (authUtil.getTokenAccess() != null) {

            if (authUtil.getTypeLogin() == TypeAuth.BIOMETRIC) {
                biometric()
            } else {
                Log.d(TAG, authUtil.getTokenAccess().toString())
                getUser()
            }
        } else if (isLoggedInFacebook() && authUtil.getTypeLogin() == TypeAuth.FACEBOOK)
            authFacebook(AccessToken.getCurrentAccessToken())
    }


    fun signInEmail() {
        signInEmailAndPassword(UserSignInBody(email.get() ?: "", password.get() ?: ""))
    }


    private fun signInEmailAndPassword(user: UserSignInBody) {

        val hashUser = hashMapOf(
            Pair("email", user.email),
            Pair("password", user.password)
        )

        authUtil.userEmailAuth(ConfigurationLibAuth.getHeaderDefault(), user, object :
            JsonObjectResponseCallback {
            override fun onBegin() {
                Log.d(TAG, "init validation")
            }

            override fun onSuccess(userResponse: JsonObject) {
                Log.d(TAG, "$userResponse")
                successLogin.postValue(true)
                logged.set(true)
            }

            override fun onFailed(error: String) {
                Log.e(TAG, error)
            }
        })


    }

    fun authFacebook(accessToken: AccessToken) {


        val facebookAuthBody = FacebookAuthBody(
            accessToken.token,
            ""
        )


        authUtil.facebookAuth(ConfigurationLibAuth.getHeaderDefault(), facebookAuthBody, object :
            JsonObjectResponseCallback {
            override fun onBegin() {
                Log.d(TAG, "init validation")
            }

            override fun onSuccess(userResponse: JsonObject) {
                Log.d(TAG, "$userResponse")
                successLogin.postValue(true)
                logged.set(true)
            }

            override fun onFailed(error: String) {
                Log.e(TAG, error)
            }


        })

        Log.d(TAG, accessToken.toString())

    }


    fun getUser() {
        authUtil.getUser(ConfigurationLibAuth.getHeaderWithAuthorizationToken(authUtil.getTokenAccess() ?: ""), object :
            JsonObjectResponseCallback {
            override fun onBegin() {
                Log.d(TAG, "init validation")
            }

            override fun onSuccess(user: JsonObject) {
                successLogin.postValue(true)
                logged.set(true)
                Log.d(TAG, "ic_user logged: $user")
            }

            override fun onFailed(error: String) {
                errorMsg = error
                successLogin.postValue(false)
                Log.e(TAG, error)
                password.set("")
                logged.set(false)
            }

        })

    }


    fun logout() {
        authUtil.logout()
        logged.set(false)
    }

    fun biometric() {
       authBiometric.set(true)
    }


    private fun isLoggedInFacebook() =
        AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired


}