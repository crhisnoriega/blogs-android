package com.mcontigo.authandroid.viewmodel

import android.app.Application
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.mcontigo.androidauthmodule.body.UserSignUpBody
import com.mcontigo.androidauthmodule.callback.JsonObjectResponseCallback
import com.mcontigo.androidauthmodule.model.UserResponse
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.authandroid.configuration.ConfigurationLibAuth

class RegisterViewModel(application: Application) : AndroidViewModel(application) {
    val TAG = "RegisterViewModel"

    val displayName = ObservableField("")
    val email = ObservableField("")
    val password = ObservableField("")
    val passwordConfirmation = ObservableField("")


    val success = MutableLiveData<Boolean>()

    val authUtil = AuthUtil(getApplication(), ConfigurationLibAuth.BASE_URL)

    var errorMsg = ""


    fun signUpEmailAndPassword() {

        val user = UserSignUpBody(
            email.get() ?: "",
            email.get() ?: "",
            password.get() ?: ""
        )


        if (passwordConfirmation.get() != password.get()) {
            errorMsg = "password and password confirmation not equals"
            success.postValue(false)
            return
        }


        authUtil.userEmailRegister(ConfigurationLibAuth.getHeaderDefault(), user, object :
            JsonObjectResponseCallback {
            override fun onBegin() {
                Log.d(TAG, "init validation")
            }

            override fun onSuccess(userResponse: JsonObject) {
                Log.d(TAG, "$userResponse")
                success.postValue(true)
            }

            override fun onFailed(error: String) {
                errorMsg = error.toString()
                success.postValue(false)
                Log.e(TAG, "$error")
                password.set("")
                passwordConfirmation.set("")
            }
        })

    }
}