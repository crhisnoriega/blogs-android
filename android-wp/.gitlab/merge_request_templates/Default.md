# Description

Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context. List any dependencies that are required for this change.

Fixes # (issue)

## Type of change

Please delete options that are not relevant.

- [ ] Bug fix (non-breaking change which fixes an issue)
- [ ] New feature (non-breaking change which adds functionality)
- [ ] Breaking change (fix or feature that would cause existing functionality to not work as expected)
- [ ] This change requires a documentation update

# How Has This Been Tested?

Please describe the tests that you ran to verify your changes. Provide instructions so we can reproduce. Please also list any relevant details for your test configuration

- [ ] Test A
- [ ] Test B

**Test Configuration**:
* Firmware version:
* Hardware:
* Toolchain:
* SDK:

# Checklist:

- [ ] My code follows the style guidelines of this project
- [ ] My feature or fix has been implemented accordingly the task scope
- [ ] I have tested against different browsers, screen resolutions, and deactivated JS (if front-end project)
- [ ] I have compared the implemented screens against the approved design (if front-end project)
- [ ] I have performed a self-review of my own code
- [ ] I have used the Site Review feature to test the application (if activated)
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] I have updated the documentation accordingly
- [ ] My changes generate no new warnings neither on browser console nor CLI console
- [ ] I have added tests that prove my fix is effective or that my feature works
- [ ] New and existing unit tests pass locally with my changes
- [ ] All new and existing tests passed
- [ ] Any dependent changes have been merged and published in downstream modules
- [ ] THE PRODUCT OWNER HAS CLICKED ON 'STOP REVIEW' BUTTON BEFORE MERGE THIS MR
