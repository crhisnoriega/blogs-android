package com.mcontigo.androidwpmodule.callbacks

import com.google.gson.JsonArray
import com.google.gson.JsonObject

interface JsonArrayCallBack{
    fun onBegin()
    fun onSuccess(result: JsonArray)
    fun onError(error: String)
}