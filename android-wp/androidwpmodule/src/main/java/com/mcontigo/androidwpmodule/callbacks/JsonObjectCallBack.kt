package com.mcontigo.androidwpmodule.callbacks

import com.google.gson.JsonObject

interface JsonObjectCallBack{
    fun onBegin()
    fun onSuccess(result: JsonObject)
    fun onError(error: String)
}