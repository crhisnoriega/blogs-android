package com.mcontigo.androidwpmodule.communication


import com.mcontigo.androidwpmodule.dao.post.Author
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface AuthorEndPoints{
    @GET("v1/users/{id}")
    suspend fun getAuthorById(@Path("id") id: Int): Response<Author>

}