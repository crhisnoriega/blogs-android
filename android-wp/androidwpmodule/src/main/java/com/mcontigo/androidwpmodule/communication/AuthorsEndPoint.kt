package com.mcontigo.androidwpmodule.communication

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

internal interface AuthorsEndPoint {
    @GET("v1/authors/")
    suspend fun getAuthors(): Response<JsonArray>

    @GET("v1/authors/{id}")
    suspend fun getAuthor(@Path("id") id: Int): Response<JsonObject>
}