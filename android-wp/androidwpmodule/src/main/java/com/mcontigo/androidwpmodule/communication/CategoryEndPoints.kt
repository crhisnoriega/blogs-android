package com.mcontigo.androidwpmodule.communication

import com.mcontigo.androidwpmodule.dao.category.Category
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

internal interface CategoryEndPoints {
    @GET("v2/categories/")
    suspend fun getCategories(): Response<List<Category>>

    @GET("v2/categories/{id}")
    suspend fun getCategory(@Path("id") id: Int): Response<Category>
}