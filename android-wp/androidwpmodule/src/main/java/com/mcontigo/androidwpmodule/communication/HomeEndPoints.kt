package com.mcontigo.androidwpmodule.communication

import com.google.gson.JsonObject
import retrofit2.Response
import retrofit2.http.GET

interface HomeEndPoints{
    @GET("v1/home")
    suspend fun getHome() : Response<JsonObject>
}