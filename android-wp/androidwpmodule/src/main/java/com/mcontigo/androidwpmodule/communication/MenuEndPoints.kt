package com.mcontigo.androidwpmodule.communication

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mcontigo.androidwpmodule.dao.menu.Item
import com.mcontigo.androidwpmodule.dao.menu.Menu
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface MenuEndPoints {
    @GET("v1/menus/")
    suspend fun getMenus() : Response<List<Menu>>

    @GET("v1/menus/{location}")
    suspend fun getLocationMenu(@Path("location") location : String) : Response<List<Item>>


    @GET("v1/sidebars/single-sidebar")
    suspend fun getSideBarInformations() : Response<JsonObject>

}