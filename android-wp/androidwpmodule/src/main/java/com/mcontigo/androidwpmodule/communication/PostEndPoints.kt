package com.mcontigo.androidwpmodule.communication

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mcontigo.androidwpmodule.dao.PostContent
import com.mcontigo.androidwpmodule.dao.TagSuggestion
import com.mcontigo.androidwpmodule.dao.post.Post
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PostEndPoints {
    @GET("mc/v1/posts/")
    suspend fun getPostsSearch(
        @Query("search") search: String?, @Query("page") page: Int, @Query("size") size: Int, @Query(
            "tags"
        ) tags: Int?,@Query(
            "orderby"
        ) orderBy: String?,
        @Query(
            "order"
        ) order: String? = "asc"
    ): Response<List<Post>>

    @GET("wp/v2/tags/")
    suspend fun getTagsSuggestions(
        @Query("page") page: Int, @Query("page_size") size: Int, @Query("orderby") orderBy: String, @Query(
            "order"
        ) order: String
    ): Response<List<TagSuggestion>>

    @GET("mc/v1/posts/{id}")
    suspend fun getPost(@Path("id") id: Int): Response<Post>

    @GET("mc/v1/posts/")
    suspend fun getPostsByCategory(
        @Query("categories") idCategory: Int? = null,
        @Query("page") page: Int,
        @Query("per_page") size: Int
    ): Response<JsonArray>

    @GET("mc/v1/posts/")
    suspend fun getPostsByAuthor(
        @Query("author") idAuthor: Int? = null,
        @Query("page") page: Int,
        @Query("per_page") size: Int
    ): Response<JsonArray>


    @GET("mc/v1/posts/")
    suspend fun getReviewedPostsByAuthor(
        @Query("reviewed_by") reviewedBy: Int? = null,
        @Query("page") page: Int,
        @Query("per_page") size: Int
    ): Response<JsonArray>

    @GET("mc/v1/posts/{slug}")
    suspend fun getPostBySlug(@Path("slug") slug: String): Response<Post>
}
