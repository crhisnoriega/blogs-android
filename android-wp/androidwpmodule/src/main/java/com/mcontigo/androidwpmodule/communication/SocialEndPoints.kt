package com.mcontigo.androidwpmodule.communication

import com.mcontigo.androidwpmodule.dao.Social
import retrofit2.Response
import retrofit2.http.GET

interface SocialEndPoints {
    @GET("v1/menus/social_links")
    suspend fun getSocials(): Response<List<Social>>
}