package com.mcontigo.androidwpmodule.communication

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.mcontigo.androidwpmodule.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

internal class WordPressService(urlApp: String) {

    val url = "$urlApp"///wp-json/mc/"


    companion object {

        val TAG = "WordPressService"
        private fun provideRetrofit(baseUrl: String): Retrofit {

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY


            val okHttpClient = if (BuildConfig.DEBUG) OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build()
            else
                OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build()


            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }


    internal fun categories() =
        provideRetrofit("$url/wp-json/wp/").create(CategoryEndPoints::class.java)

    internal fun posts() = provideRetrofit("$url/wp-json/").create(PostEndPoints::class.java)
    internal fun menus() = provideRetrofit("$url/wp-json/mc/").create(MenuEndPoints::class.java)
    internal fun home() = provideRetrofit("$url/wp-json/mc/").create(HomeEndPoints::class.java)
    internal fun social() = provideRetrofit("$url/wp-json/mc/").create(SocialEndPoints::class.java)
    internal fun author() = provideRetrofit("$url/wp-json/mc/").create(AuthorEndPoints::class.java)
}