package com.mcontigo.androidwpmodule.dao

import com.google.gson.annotations.SerializedName


data class FeatureMedia(
    @SerializedName("large")
    val large: String?,
    @SerializedName("medium")
    val medium: String?,
    @SerializedName("medium_large")
    val mediumLarge: String?,
    @SerializedName("thumbnail")
    val thumbnail: String?
)