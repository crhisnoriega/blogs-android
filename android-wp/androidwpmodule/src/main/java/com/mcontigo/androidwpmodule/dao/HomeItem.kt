package com.mcontigo.androidwpmodule.dao

import com.google.gson.annotations.SerializedName


data class HomeItem(
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("id")
    val id: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("url")
    val url: String? = "",
    @SerializedName("widgets")
    val widgets: List<WidgetPost?>? = listOf()
)

data class Widget(
    @SerializedName("content")
    val content: List<PostContentHome>? = null,
    @SerializedName("id")
    val id: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("options")
    val options: OptionsPost? = null,
    @SerializedName("type")
    val type: String? = ""
)


data class Options(
    @SerializedName("content_type")
    val contentType: String? = null,
    @SerializedName("offset")
    val offset: Int?= null,
    @SerializedName("order")
    val order: String?= null,
    @SerializedName("post_style")
    val postStyle: String?= null,
    @SerializedName("post_type")
    val postType: List<String?>?= null,
    @SerializedName("postcount")
    val postcount: Int?= null,
    @SerializedName("posts_per_page")
    val postsPerPage: Int?= null,
    @SerializedName("title")
    val title: String?= null,
    @SerializedName("widget_class")
    val widgetClass: String?= null,
    @SerializedName("widget_type")
    val widgetType: String?= null
)

data class PostContentHome(
    @SerializedName("categories")
    val category: List<CategoryPostContent>? = null,
    @SerializedName("excerpt")
    val excerpt: String? = null,
    @SerializedName("featured_media")
    val featuredMedia: FeaturedMediaPost? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("link")
    val link: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("slot_name")
    val slotName: String? = null,
    @SerializedName("is_stick")
    val isStick: String? = null,
    @SerializedName("slot_div_name")
    val slotDivName: String? = null,
    @SerializedName("slug")
    val slug: String? = null,
    @SerializedName("permalink")
    val permalink: String? = null,
    @SerializedName("headline")
    val headline: String? = null
)


data class FeaturedMedia(
    @SerializedName("large")
    val large: String? = "",
    @SerializedName("medium")
    val medium: String? = "",
    @SerializedName("medium_large")
    val mediumLarge: String? = "",
    @SerializedName("thumbnail")
    val thumbnail: String? = ""
)

data class CategoryPostContent(
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("slug")
    val slug: String? = "",
    @SerializedName("url")
    val url: String? = "",
    @SerializedName("permalink")
    val permalink: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("link")
    val link: String? = null
)