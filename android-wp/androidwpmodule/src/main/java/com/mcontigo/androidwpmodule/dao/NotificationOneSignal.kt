package com.mcontigo.androidwpmodule.dao
import com.google.gson.annotations.SerializedName



data class NotificationOnePushAction(
    @SerializedName("action")
    var action: Action? = Action(),
    @SerializedName("notification")
    var notification: Notification? = Notification()
)

data class Action(
    @SerializedName("type")
    var type: Int? = 0
)

data class Notification(
    @SerializedName("androidNotificationId")
    var androidNotificationId: Int? = 0,
    @SerializedName("displayType")
    var displayType: Int? = 0,
    @SerializedName("isAppInFocus")
    var isAppInFocus: Boolean? = false,
    @SerializedName("payload")
    var payload: Payload? = Payload(),
    @SerializedName("shown")
    var shown: Boolean? = false
)

data class Payload(
    @SerializedName("additionalData")
    var additionalData: AdditionalData? = AdditionalData(),
    @SerializedName("bigPicture")
    var bigPicture: String? = "",
    @SerializedName("body")
    var body: String? = "",
    @SerializedName("fromProjectNumber")
    var fromProjectNumber: String? = "",
    @SerializedName("lockScreenVisibility")
    var lockScreenVisibility: Int? = 0,
    @SerializedName("notificationID")
    var notificationID: String? = "",
    @SerializedName("priority")
    var priority: Int? = 0,
    @SerializedName("rawPayload")
    var rawPayload: String? = "",
    @SerializedName("title")
    var title: String? = ""
)

data class AdditionalData(
    @SerializedName("deepLinking")
    var deepLinking: String? = "",
    @SerializedName("multimessageId")
    var multimessageId: String? = "",
    @SerializedName("segmentDescription")
    var segmentDescription: String? = "",
    @SerializedName("strategy")
    var strategy: String? = ""
)