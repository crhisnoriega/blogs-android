package com.mcontigo.androidwpmodule.dao

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.annotations.SerializedName


data class PostContent(
    @SerializedName("author")
    var author: Author? = null,
    @SerializedName("bibliography")
    var bibliography: String? = null,
    @SerializedName("categories")
    var categories: List<Category?>? = null,
    @SerializedName("content")
    var content: String? = null,
    @SerializedName("excerpt")
    var excerpt: String? = null,
    @SerializedName("featured_media")
    var featuredMedia: FeaturedMediaPost? = null,
    @SerializedName("headline")
    var headline: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("link")
    var link: String? = null,
    @SerializedName("metas")
    var metas: JsonObject? = null,
    @SerializedName("modified")
    var modified: String? = null,
    @SerializedName("options")
    var options: OptionsPost? = null,
    @SerializedName("published")
    var published: String? = null,
    @SerializedName("related_links")
    var relatedLinks: List<RelatedLink?>? = null,
    @SerializedName("reviewed")
    var reviewed: String? = null,
    @SerializedName("reviewed_by")
    var reviewedBy: ReviewedBy? = null,
    @SerializedName("sidebars")
    var sidebars: JsonArray? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("sticky")
    var sticky: Boolean? = null,
    @SerializedName("tags")
    var tags: List<Tag?>? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("url")
    var url: String? = null,
    var type: String? = null
)

data class ReviewedBy(
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("gender")
    var gender: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("picture")
    var picture: String? = null,
    @SerializedName("profession")
    var profession: String? = null,
    @SerializedName("social_profiles")
    var socialProfiles: SocialProfiles? = null,
    @SerializedName("type")
    var type: String? = null
)

data class SocialProfiles(
    @SerializedName("facebook")
    var facebook: String? = null,
    @SerializedName("instagram")
    var instagram: String? = null,
    @SerializedName("linkedin")
    var linkedin: String? = null,
    @SerializedName("twitter")
    var twitter: String? = null
)

data class Metas(
    @SerializedName("article:modified_time")
    var articleModifiedTime: String? = null,
    @SerializedName("article:published_time")
    var articlePublishedTime: String? = null,
    @SerializedName("article:publisher")
    var articlePublisher: String? = null,
    @SerializedName("article:section")
    var articleSection: String? = null,
    @SerializedName("article:tag")
    var articleTag: List<String>? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("og:description")
    var ogDescription: String? = null,
    @SerializedName("og:image")
    var ogImage: String? = null,
    @SerializedName("og:image:secure_url")
    var ogImageSecureUrl: String? = null,
    @SerializedName("og:locale")
    var ogLocale: String? = null,
    @SerializedName("og:site_name")
    var ogSiteName: String? = null,
    @SerializedName("og:title")
    var ogTitle: String? = null,
    @SerializedName("og:type")
    var ogType: String? = null,
    @SerializedName("og:url")
    var ogUrl: String? = null,
    @SerializedName("robots")
    var robots: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("twitter:card")
    var twitterCard: String? = null,
    @SerializedName("twitter:creator")
    var twitterCreator: String? = null,
    @SerializedName("twitter:description")
    var twitterDescription: String? = null,
    @SerializedName("twitter:site")
    var twitterSite: String? = null,
    @SerializedName("twitter:title")
    var twitterTitle: String? = null
)

data class RelatedLink(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("link")
    var link: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("url")
    var url: String? = null
)

data class OptionsPost(
    @SerializedName("ads")
    var ads: String? = null,
    @SerializedName("author")
    var author: String? = null,
    @SerializedName("comments")
    var comments: String? = null,
    @SerializedName("date")
    var date: String? = null,

    var postStyle: String? = null,
    var contentType: String? = null
)

data class Tag(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("link")
    var link: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("url")
    var url: String? = null
)

data class FeaturedMediaPost(
    @SerializedName("big-size")
    var bigSize: String? = null,
    @SerializedName("big-size_mobile")
    var bigSizeMobile: String? = null,
    @SerializedName("large")
    var large: String? = null,
    @SerializedName("medium")
    var medium: String? = null,
    @SerializedName("medium_large")
    var mediumLarge: String? = null,
    @SerializedName("mid-size")
    var midSize: String? = null,
    @SerializedName("mid-size_mobile")
    var midSizeMobile: String? = null,
    @SerializedName("thumbnail")
    var thumbnail: String? = null
)

data class Category(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("link")
    var link: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("url")
    var url: String? = null
)

data class Author(
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("gender")
    var gender: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("picture")
    var picture: String? = null,
    @SerializedName("profession")
    var profession: String? = null,
    @SerializedName("social_profiles")
    var socialProfiles: SocialProfiles? = null,
    @SerializedName("type")
    var type: String? = null
)

data class Sidebars(
    @SerializedName("bottom")
    var bottom: Bottom? = null,
    @SerializedName("right")
    var right: Right? = null
)

data class Bottom(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("url")
    var url: String? = null,
    @SerializedName("widgets")
    var widgets: List<WidgetPost?>? = null
)

data class WidgetPost(
    @SerializedName("content")
    var content: List<Content?>? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("options")
    var options: OptionsX? = null,
    @SerializedName("type")
    var type: String? = null
)

data class Content(
    @SerializedName("categories")
    var categories: List<Category?>? = null,
    @SerializedName("excerpt")
    var excerpt: String? = null,
    @SerializedName("featured_media")
    var featuredMedia: FeaturedMediaPost? = null,
    @SerializedName("headline")
    var headline: String? = null,
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("link")
    var link: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("url")
    var url: String? = null
)

data class OptionsX(
    @SerializedName("category")
    var category: Category? = null,
    @SerializedName("content_type")
    var contentType: String? = null,
    @SerializedName("offset")
    var offset: Int? = null,
    @SerializedName("order")
    var order: String? = null,
    @SerializedName("post_style")
    var postStyle: String? = null,
    @SerializedName("post_type")
    var postType: List<String?>? = null,
    @SerializedName("postcount")
    var postcount: Int? = null,
    @SerializedName("posts_per_page")
    var postsPerPage: Int? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("widget_class")
    var widgetClass: String? = null,
    @SerializedName("widget_type")
    var widgetType: String? = null
)

data class Right(
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("url")
    var url: String? = null,
    @SerializedName("widgets")
    var widgets: List<WidgetPost?>? = null
)