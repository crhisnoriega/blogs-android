package com.mcontigo.androidwpmodule.dao

import com.google.gson.annotations.SerializedName


data class Social(
    @SerializedName("children")
    val children: Any? = null,
    @SerializedName("classes")
    val classes: List<String?>? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("ID")
    val iD: Int? = null,
    @SerializedName("item_id")
    val itemId: Any? = null,
    @SerializedName("link")
    val link: String? = null,
    @SerializedName("name")
    val name: Any? = null,
    @SerializedName("order")
    val order: Int? = null,
    @SerializedName("slug")
    val slug: Any? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("type")
    val type: String? = null
)