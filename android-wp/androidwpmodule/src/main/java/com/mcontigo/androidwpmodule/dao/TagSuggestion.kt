package com.mcontigo.androidwpmodule.dao
import com.google.gson.annotations.SerializedName


data class TagSuggestion(
    @SerializedName("_links")
    val links: LinksTag? = LinksTag(),
    @SerializedName("count")
    val count: Int? = 0,
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("link")
    val link: String? = "",
    @SerializedName("meta")
    val meta: List<Any?>? = listOf(),
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("slug")
    val slug: String? = "",
    @SerializedName("taxonomy")
    val taxonomy: String? = ""
)

data class LinksTag(
    @SerializedName("about")
    val about: List<AboutTag?>? = listOf(),
    @SerializedName("collection")
    val collection: List<CollectionTag?>? = listOf(),
    @SerializedName("curies")
    val curies: List<CuryTag?>? = listOf(),
    @SerializedName("self")
    val self: List<SelfTag?>? = listOf(),
    @SerializedName("wp:post_type")
    val wpPostType: List<WpPostTypeTag?>? = listOf()
)

data class SelfTag(
    @SerializedName("href")
    val href: String? = ""
)

data class WpPostTypeTag(
    @SerializedName("href")
    val href: String? = ""
)

data class AboutTag(
    @SerializedName("href")
    val href: String? = ""
)

data class CuryTag(
    @SerializedName("href")
    val href: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("templated")
    val templated: Boolean? = false
)

data class CollectionTag(
    @SerializedName("href")
    val href: String? = ""
)