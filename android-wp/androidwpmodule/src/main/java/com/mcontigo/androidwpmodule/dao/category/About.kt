package com.mcontigo.androidwpmodule.dao.category


data class About(
    val href: String
)