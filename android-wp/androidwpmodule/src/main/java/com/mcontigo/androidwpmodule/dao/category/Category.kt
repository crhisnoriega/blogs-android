package com.mcontigo.androidwpmodule.dao.category

data class Category(
    val _links: Links? = null,
    val count: Int? = null,
    val description: String = "",
    val id: Int? = null,
    val link: String = "",
    val meta: List<Any>? = null,
    val name: String = "",
    val parent: Int? = null,
    val slug: String = "",
    val taxonomy: String = ""
)







