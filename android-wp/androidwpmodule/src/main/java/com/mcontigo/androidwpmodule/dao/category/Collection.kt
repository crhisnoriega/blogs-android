package com.mcontigo.androidwpmodule.dao.category
data class Collection(
    val href: String
)