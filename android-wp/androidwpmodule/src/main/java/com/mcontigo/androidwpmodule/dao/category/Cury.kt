package com.mcontigo.androidwpmodule.dao.category

data class Cury(
    val href: String,
    val name: String,
    val templated: Boolean
)