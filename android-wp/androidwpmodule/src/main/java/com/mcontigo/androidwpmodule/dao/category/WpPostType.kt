package com.mcontigo.androidwpmodule.dao.category

data class WpPostType(
    val href: String
)