package com.mcontigo.androidwpmodule.dao.menu
import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("ID")
    val ID: Int? = null,
    @SerializedName("children")
    val children: List<Item>?= null,
    @SerializedName("classes")
    val classes: Any?= null,
    @SerializedName("description")
    val description: String?= null,
    @SerializedName("item_id")
    val itemId: Int?= null,
    @SerializedName("link")
    val link: String?= null,
    @SerializedName("name")
    val name: String?= null,
    @SerializedName("order")
    val order: Int?= null,
    @SerializedName("slug")
    val slug: String?= null,
    @SerializedName("title")
    val title: String?= null,
    @SerializedName("type")
    val type: String?= null
)