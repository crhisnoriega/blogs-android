package com.mcontigo.androidwpmodule.dao.menu

data class Menu(
    val description: String? = "",
    val id: String? = "",
    val url: String? = ""
)