package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ArticleTag(
    @SerializedName("content")
    var content: String? = ""
) : Parcelable