package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Author(
    @SerializedName("description")
    var description: String? = "",
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("slug")
    var slug: String? = "",
    @SerializedName("link")
    var link: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("picture")
    var picture: String? = "",
    @SerializedName("social_profiles")
    var socialProfiles: SocialProfiles? = SocialProfiles(),
    @SerializedName("type")
    var type: String? = "",
    @SerializedName("url")
    var url: String? = "",
    @SerializedName("posts")
    var posts: PostPublishedInfo? = null,
    @SerializedName("address")
    var address: String? = "",
    @SerializedName("profession")
    var profession: String? = "",
    @SerializedName("gender")
    var gender: String? = ""


    ) : Parcelable{
    companion object{
        const val TYPE_AUTHOR = "author"
        const val TYPE_PROFESSIONAL = "professional"
    }
}


@Parcelize
data class PostPublishedInfo(
    @SerializedName("published")
    var published: Long? = null,
    @SerializedName("reviewed")
    var reviewed: Long? = null,
    @SerializedName("total")
    var total: Long? = null
) : Parcelable