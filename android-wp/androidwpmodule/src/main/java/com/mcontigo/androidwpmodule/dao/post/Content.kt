package com.mcontigo.androidwpmodule.dao.post


import com.google.gson.annotations.SerializedName

data class Content(
    @SerializedName("categories")
    var categories: List<Category?>? = listOf(),
    @SerializedName("excerpt")
    var excerpt: String? = "",
    @SerializedName("featured_media")
    var featuredMedia: FeaturedMedia? = FeaturedMedia(),
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("link")
    var link: String? = "",
    @SerializedName("slug")
    var slug: String? = "",
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("url")
    var url: String? = "",
    @SerializedName("is_stick")
    var isStick: String? = "",
    @SerializedName("slot_div_name")
    var slotDivName: String? = "",
    @SerializedName("slot_name")
    var slotName: String? = ""
)
