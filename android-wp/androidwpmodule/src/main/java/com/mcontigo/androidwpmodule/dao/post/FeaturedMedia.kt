package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class FeaturedMedia(
    @SerializedName("big-size")
    var bigSize: String? = "",
    @SerializedName("big-size_mobile")
    var bigSizeMobile: String? = "",
    @SerializedName("large")
    var large: String? = "",
    @SerializedName("medium")
    var medium: String? = "",
    @SerializedName("medium_large")
    var mediumLarge: String? = "",
    @SerializedName("mid-size")
    var midSize: String? = "",
    @SerializedName("mid-size_mobile")
    var midSizeMobile: String? = "",
    @SerializedName("thumbnail")
    var thumbnail: String? = ""
) : Parcelable