package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Metas(
    @SerializedName("article:modified_time")
    var articleModifiedTime: String? = "",
    @SerializedName("article:published_time")
    var articlePublishedTime: String? = "",
    @SerializedName("article:publisher")
    var articlePublisher: String? = "",
    @SerializedName("article:section")
    var articleSection: String? = "",
    @SerializedName("article:tag")
    var articleTag: List<ArticleTag?>? = listOf(),
    @SerializedName("canonical")
    var canonical: String? = "",
    @SerializedName("description")
    var description: String? = "",
    @SerializedName("og:description")
    var ogDescription: String? = "",
    @SerializedName("og:image")
    var ogImage: String? = "",
    @SerializedName("og:image:secure_url")
    var ogImageSecureUrl: String? = "",
    @SerializedName("og:locale")
    var ogLocale: String? = "",
    @SerializedName("og:site_name")
    var ogSiteName: String? = "",
    @SerializedName("og:title")
    var ogTitle: String? = "",
    @SerializedName("og:type")
    var ogType: String? = "",
    @SerializedName("og:updated_time")
    var ogUpdatedTime: String? = "",
    @SerializedName("og:url")
    var ogUrl: String? = "",
    @SerializedName("robots")
    var robots: String? = "",
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("twitter:card")
    var twitterCard: String? = "",
    @SerializedName("twitter:creator")
    var twitterCreator: String? = "",
    @SerializedName("twitter:description")
    var twitterDescription: String? = "",
    @SerializedName("twitter:image")
    var twitterImage: String? = "",
    @SerializedName("twitter:site")
    var twitterSite: String? = "",
    @SerializedName("twitter:title")
    var twitterTitle: String? = ""
) : Parcelable