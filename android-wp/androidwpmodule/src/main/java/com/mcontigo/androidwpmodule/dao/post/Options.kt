package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Options(
    @SerializedName("is_stick")
    var isStick: String? = "",
    @SerializedName("slot_type")
    var slotType: String? = "",
    @SerializedName("category")
    var category: String? = "",
    @SerializedName("content_type")
    var contentType: String? = "",
    @SerializedName("layout")
    var layout: String? = "",
    @SerializedName("offset")
    var offset: Int? = 0,
    @SerializedName("order")
    var order: String? = "",
    @SerializedName("post_style")
    var postStyle: String? = "",
    @SerializedName("post_type")
    var postType: List<String?>? = listOf(),
    @SerializedName("posts_per_page")
    var postsPerPage: Int? = 0,
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("widget_class")
    var widgetClass: String? = "",
    @SerializedName("widget_type")
    var widgetType: String? = "",
    @SerializedName("ads")
    var ads: String? = "",
    @SerializedName("author")
    var author: String? = "",
    @SerializedName("comments")
    var comments: String? = "",
    @SerializedName("date")
    var date: String? = "",
    @SerializedName("is_sponsored")
    var isSponsored : Boolean? = false,
    @SerializedName("sponsor")
    var sponsor : String? = "",
    @SerializedName("verification_status")
    var verificationStatus : String? = ""
) : Parcelable