package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class Post(
    @SerializedName("author")
    var author: Author? = Author(),
    @SerializedName("bibliography")
    var bibliography: String? = "",
    @SerializedName("categories")
    var categories: List<Category?>? = listOf(),
    @SerializedName("content")
    var content: String? = "",
    @SerializedName("excerpt")
    var excerpt: String? = "",
    @SerializedName("featured_media")
    var featuredMedia: FeaturedMedia? = null,
    @SerializedName("headline")
    var headline: String? = "",
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("link")
    var link: String? = "",
    @SerializedName("metas")
    var metas: Metas? = Metas(),
    @SerializedName("modified")
    var modified: String? = "",
    @SerializedName("options")
    var options: Options? = Options(),
    @SerializedName("published")
    var published: String? = "",
    @SerializedName("related_links")
    var relatedLinks: List<RelatedLink?>? = listOf(),
    @SerializedName("reviewed")
    var reviewed: String? = null,
    @SerializedName("reviewed_by")
    var reviewedBy: Author? = null,
    @SerializedName("sidebars")
     var sidebars: @RawValue Any? = null,
    @SerializedName("slug")
    var slug: String? = "",
    @SerializedName("sticky")
    var sticky: Boolean? = false,
    @SerializedName("tags")
    var tags: List<Tag?>? = listOf(),
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("url")
    var url: String? = "",
    @SerializedName("permalink")
    var permalink: String? = ""
) : Parcelable