package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class RelatedLink(
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("link")
    var link: String? = "",
    @SerializedName("slug")
    var slug: String? = "",
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("url")
    var url: String? = ""
) : Parcelable