package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class SocialProfiles(
    @SerializedName("facebook")
    var facebook: String? = "",
    @SerializedName("linkedin")
    var linkedin: String? = "",
    @SerializedName("twitter")
    var twitter: String? = "",
    @SerializedName("instagram")
    var instagram: String? = ""
) : Parcelable