package com.mcontigo.androidwpmodule.dao.post


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Tag(
    @SerializedName("id")
    var id: Int? = 0,
    @SerializedName("link")
    var link: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("slug")
    var slug: String? = "",
    @SerializedName("url")
    var url: String? = ""
) : Parcelable