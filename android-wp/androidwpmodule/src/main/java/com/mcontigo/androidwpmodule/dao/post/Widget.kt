package com.mcontigo.androidwpmodule.dao.post


import com.google.gson.annotations.SerializedName

data class Widget(
    @SerializedName("content")
    var content: List<Content?>? = null,
    @SerializedName("id")
    var id: String? = "",
    @SerializedName("options")
    var options: Options? = Options(),
    @SerializedName("type")
    var type: String? = ""
)