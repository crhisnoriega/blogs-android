package com.mcontigo.androidwpmodule.repository

import com.mcontigo.androidwpmodule.communication.AuthorEndPoints
import com.mcontigo.androidwpmodule.communication.WordPressService
import com.mcontigo.androidwpmodule.dao.post.Author

class AuthorRepository(urlApi: String) : BaseRepositoryApi() {

    private val authorServiceApi: AuthorEndPoints = WordPressService(urlApi).author()

    suspend fun getAuthor(id: Int): Author? {
        return safeApiCall(
            call = { authorServiceApi.getAuthorById(id) },
            error = "Error load author by id"
        )
    }

}