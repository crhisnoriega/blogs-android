package com.mcontigo.androidwpmodule.repository

import com.mcontigo.androidwpmodule.communication.CategoryEndPoints
import com.mcontigo.androidwpmodule.communication.WordPressService
import com.mcontigo.androidwpmodule.dao.category.Category

class CategoriesRepository(urlApi : String) : BaseRepositoryApi(){

    private val categoriesServiceApi : CategoryEndPoints = WordPressService(urlApi).categories()


    suspend fun getCategories(): List<Category>? {
        return safeApiCall(
            call = { categoriesServiceApi.getCategories() },
            error = "Error load categories"
        )
    }

    suspend fun getCategoryById(id : Int): Category? {
        return safeApiCall(
            call = { categoriesServiceApi.getCategory(id) },
            error = "Error load category $id"
        )
    }

}