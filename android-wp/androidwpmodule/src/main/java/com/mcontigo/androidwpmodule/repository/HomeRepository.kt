package com.mcontigo.androidwpmodule.repository

import com.google.gson.JsonObject
import com.mcontigo.androidwpmodule.communication.HomeEndPoints
import com.mcontigo.androidwpmodule.communication.WordPressService
import com.mcontigo.androidwpmodule.dao.Options
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.util.JsonUtil

class HomeRepository(urlApi : String) : BaseRepositoryApi(){

    private val homeServiceApi : HomeEndPoints = WordPressService(urlApi).home()


    suspend fun getHome(): List<Pair<Options, List<PostContentHome>>>? {
        return getHomeApi()?.let { JsonUtil.getHome(it) }
    }


    private suspend fun getHomeApi(): JsonObject? {
        return safeApiCall(
            call = { homeServiceApi.getHome() },
            error = "Error load home"
        )
    }

}