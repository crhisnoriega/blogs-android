package com.mcontigo.androidwpmodule.repository

import com.mcontigo.androidwpmodule.communication.MenuEndPoints
import com.mcontigo.androidwpmodule.communication.WordPressService
import com.mcontigo.androidwpmodule.dao.menu.Item
import com.mcontigo.androidwpmodule.dao.menu.Menu

class MenuRepository(urlApi : String) : BaseRepositoryApi(){

    private val menuServiceEndPoint : MenuEndPoints = WordPressService(urlApi).menus()

    suspend fun getMenus(): List<Menu>? {
        return safeApiCall(
            call = { menuServiceEndPoint.getMenus() },
            error = "Error load menus"
        )
    }

    suspend fun getItemMenu(location : String): List<Item>? {
        return safeApiCall(
            call = { menuServiceEndPoint.getLocationMenu(location) },
            error = "Error load itens menu"
        )
    }



}