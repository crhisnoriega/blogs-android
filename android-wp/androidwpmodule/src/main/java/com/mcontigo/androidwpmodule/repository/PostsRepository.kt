package com.mcontigo.androidwpmodule.repository

import com.google.gson.JsonArray
import com.mcontigo.androidwpmodule.communication.PostEndPoints
import com.mcontigo.androidwpmodule.communication.WordPressService
import com.mcontigo.androidwpmodule.dao.PostContent
import com.mcontigo.androidwpmodule.dao.TagSuggestion
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.util.JsonUtil

class PostsRepository(urlApi: String) : BaseRepositoryApi() {

    private val postServiceApi: PostEndPoints = WordPressService(urlApi).posts()

    suspend fun getPostById(id: Int): Post? {
        return safeApiCall(
            call = { postServiceApi.getPost(id) },
            error = "Error load post by id"
        )
    }


    suspend fun getPostBySlug(slug: String): Post? {
        return safeApiCall(
            call = { postServiceApi.getPostBySlug(slug) },
            error = "Error load post by slug"
        )
    }


    suspend fun getTagsSuggestions(
        page: Int,
        size: Int,
        orderBy: String,
        order: String
    ): List<TagSuggestion>? {
        return safeApiCall(
            call = { postServiceApi.getTagsSuggestions(page, size, orderBy, order) },
            error = "Error load tags"
        )
    }


    suspend fun getPostSearch(
        searchTerm: String?,
        page: Int,
        size: Int,
        tagId: Int?,
        orderBy: String? = null
    ): List<Post>? {
        return safeApiCall(
            call = { postServiceApi.getPostsSearch(searchTerm, page, size, tagId, orderBy) },
            error = "Error load search posts"
        )
    }

    suspend fun getPostsByCategoryPaging(
        idCategory: Int,
        page: Int,
        size: Int,
        firstLoad: Boolean = false
    ): List<Post>? {
        return getPostsByCategoryPagingApi(idCategory, page, size)?.let {
            JsonUtil.getPostsContent(
                it,
                firstLoad
            )
        }
    }

    suspend fun getPostsByAuthor(
        authorId: Int,
        reviewed: Boolean = false,
        page: Int,
        size: Int,
        firstLoad: Boolean = false
    ): List<Post>? {
        return if (reviewed) {
            getPostsByReviewedAuthorPagingApi(authorId, page, size)?.let {
                JsonUtil.getPostsContent(
                    it,
                    firstLoad
                )
            }
        } else
            getPostsByAuthorPagingApi(authorId, page, size)?.let {
                JsonUtil.getPostsContent(
                    it,
                    firstLoad
                )
            }
    }


    private suspend fun getPostsByCategoryPagingApi(
        idCategory: Int,
        page: Int,
        size: Int
    ): JsonArray? {
        return safeApiCall(
            call = { postServiceApi.getPostsByCategory(idCategory, page, size) },
            error = "Error load posts by category"
        )
    }


    private suspend fun getPostsByAuthorPagingApi(
        authorId: Int,
        page: Int,
        size: Int
    ): JsonArray? {
        return safeApiCall(
            call = { postServiceApi.getPostsByAuthor(authorId, page, size) },
            error = "Error load posts by author"
        )
    }


    private suspend fun getPostsByReviewedAuthorPagingApi(
        authorId: Int,
        page: Int,
        size: Int
    ): JsonArray? {
        return safeApiCall(
            call = { postServiceApi.getReviewedPostsByAuthor(authorId, page, size) },
            error = "Error load posts by reviewed author"
        )
    }

}