package com.mcontigo.androidwpmodule.repository

import com.mcontigo.androidwpmodule.communication.SocialEndPoints
import com.mcontigo.androidwpmodule.communication.WordPressService
import com.mcontigo.androidwpmodule.dao.Social

class SocialRepository(urlApi : String) : BaseRepositoryApi(){

    private val socialServiceEndPoint : SocialEndPoints = WordPressService(urlApi).social()

     suspend fun getSocialsNetwork(): List<Social>? {
        return safeApiCall(
            call = { socialServiceEndPoint.getSocials() },
            error = "Error load menus"
        )
    }



}