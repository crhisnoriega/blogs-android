package com.mcontigo.androidwpmodule.util

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.mcontigo.androidwpmodule.dao.Options
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.dao.post.Post

object JsonUtil {
    private val TAG = this@JsonUtil.javaClass.simpleName

    val WIDGET_TYPE_GRID = "mc-widget-grid"
    val WIDGET_TYPE_FEATURED_POST = "mc-widget-featured-posts"
    val WIDGET_TYPE_ADS = "mc-dynamic-ads"
    val WIDGET_RECOMENDED_POSTS = "mc-widget-recommend-content"

    val POSITION_ADS_IN_POSTS_PER_CATEGORY = 4
    val POSITION_ADS_IN_POSTS_PER_CATEGORY_FIRST = 2


    val POSITION_HOME_AD_FIRST = 0
    val POSITION_HOME_AD_SECOND = 2
    val POSITION_HOME_AD_THIRD = 5
    val POSITION_HOME_AD_FOURTH = 7


    val CONTENT_ADS = "ads"
    val CONTENT_ADS_FIRST = "first_ad"


    fun getHome(jsonObject: JsonObject): List<Pair<Options, List<PostContentHome>>>? {
        try {
            val list = mutableListOf<Pair<Options, List<PostContentHome>>>()

            val jsonResultsSidebars = jsonObject.asJsonObject["sidebars"].asJsonArray

            jsonResultsSidebars.forEach { jsonResult ->
                try {
                    val widgets = jsonResult.asJsonObject["widgets"].asJsonArray
                    widgets.forEach { widget ->
                        val options: Options
                        if (widget.asJsonObject["type"].asString == WIDGET_TYPE_GRID || widget.asJsonObject["type"].asString == WIDGET_TYPE_FEATURED_POST) {
                            val contents = widget.asJsonObject["content"].asJsonArray
                            val listContent = mutableListOf<PostContentHome>()
                            contents.forEach {
                                listContent.add(
                                    Gson().fromJson(
                                        it.asJsonObject,
                                        PostContentHome::class.java
                                    )
                                )
                            }
                            options = Gson().fromJson(
                                widget.asJsonObject["options"].asJsonObject,
                                Options::class.java
                            )
                            list.add(Pair(options, listContent))
                        }
                    }
                } catch (e: Exception) {
                    Log.e(TAG, e.message)

                }
            }

            val listAdd = mutableListOf<Pair<Options, List<PostContentHome>>>()
            list.forEach {

                if (list.indexOf(it) == POSITION_HOME_AD_SECOND || list.indexOf(
                        it
                    ) == POSITION_HOME_AD_THIRD || list.indexOf(it) == POSITION_HOME_AD_FOURTH
                ) {
                    listAdd.add(
                        Pair(
                            Options(
                                postStyle = "8",
                                contentType = CONTENT_ADS
                            ), listOf(PostContentHome())
                        )
                    )
                } else if (list.indexOf(it) == POSITION_HOME_AD_FIRST) {
                    listAdd.add(
                        Pair(
                            Options(
                                postStyle = "8",
                                contentType = CONTENT_ADS_FIRST
                            ), listOf(PostContentHome())
                        )
                    )
                }
                listAdd.add(it)
            }


            return listAdd
        } catch (e: Exception) {
            Log.d(TAG, e.toString())
        }
        return null
    }


    fun getRecomendedArticles(jsonResuls: JsonObject): List<Pair<Options, List<PostContentHome>>>? {
        try {
            val list = mutableListOf<Pair<Options, List<PostContentHome>>>()

            val jsonWidgetsRecommended = jsonResuls["bottom"].asJsonObject

            try {
                val widgets = jsonWidgetsRecommended.asJsonObject["widgets"].asJsonArray
                widgets.forEach { widget ->
                    val options: Options
                    if (widget.asJsonObject["type"].asString == WIDGET_RECOMENDED_POSTS) {
                        val contents = widget.asJsonObject["content"].asJsonArray
                        val listContent = mutableListOf<PostContentHome>()
                        contents.forEach {
                            listContent.add(
                                Gson().fromJson(
                                    it.asJsonObject,
                                    PostContentHome::class.java
                                )
                            )
                        }
                        options = Gson().fromJson(
                            widget.asJsonObject["options"].asJsonObject,
                            Options::class.java
                        )
                        list.add(Pair(options, listContent))
                    }
                }
            } catch (e: Exception) {
                Log.e(TAG, e.message)
                return null
            }
            return list
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        return null
    }



    fun getPostsContent(
        jsonResuls: JsonArray,
        firstLoading: Boolean = false
    ): List<Post>? {
        try {
            val list = mutableListOf<Post>()
            jsonResuls.forEach {
                list.add(Gson().fromJson(it.asJsonObject, Post::class.java))
            }
            val listAdd = mutableListOf<Post>()
            list.forEach {
                listAdd.add(it)
                if (list.indexOf(it) == if (!firstLoading) POSITION_ADS_IN_POSTS_PER_CATEGORY else POSITION_ADS_IN_POSTS_PER_CATEGORY_FIRST) {
                    listAdd.add(Post(title = CONTENT_ADS))
                }
            }

            return listAdd
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        return null
    }


}