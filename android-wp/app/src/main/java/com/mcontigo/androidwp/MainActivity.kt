package com.mcontigo.androidwp

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders


class MainActivity : AppCompatActivity() {


    private val _viewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val TAG = this@MainActivity.javaClass.simpleName



        _viewModel.home.observe(this, Observer {
            // Log.d("Home", it.toString())

            it.forEach {
                Log.d("Home", it.first.toString())

                it.second.forEach {
                    Log.d("Home", it.toString())
                }

            }
        })


    }


}

