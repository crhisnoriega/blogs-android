package com.mcontigo.androidwp

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcontigo.androidwpmodule.dao.Options
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.repository.HomeRepository
import com.mcontigo.androidwpmodule.repository.PostsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel : ViewModel(){

    private val urlApi = "https://dev.mejorconsalud.com/"

    private val _home = MutableLiveData<List<Pair<Options, List<PostContentHome>>>>()
    val home : LiveData<List<Pair<Options, List<PostContentHome>>>> = _home
    private val homeRepository = HomeRepository(urlApi)
    private val postsRepository = PostsRepository(urlApi)


    init {

      viewModelScope.launch {
         val home = homeRepository.getHome()

          home?.also {
              _home.postValue(it)
          } ?: run {

          }

      }





    }




}