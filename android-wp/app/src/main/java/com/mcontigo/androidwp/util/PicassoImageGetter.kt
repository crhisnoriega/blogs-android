package com.mcontigo.androidwp.util

import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.widget.TextView
import com.squareup.picasso.Picasso


class PicassoImageGetter(val textView: TextView, val resources: Resources, val pablo: Picasso) : Html.ImageGetter {

    override fun getDrawable(source: String?): Drawable {
        val bitmap = pablo.load(source).get()
        val drawable = BitmapDrawable(resources, bitmap)
        val result = BitmapDrawablePlaceHolder(drawable)
        result.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        return result
    }
}

class BitmapDrawablePlaceHolder(var drawable: Drawable) : BitmapDrawable() {
    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        drawable.draw(canvas)
    }
}

