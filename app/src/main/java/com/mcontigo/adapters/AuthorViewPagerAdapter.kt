package com.mcontigo.adapters

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.mcontigo.androidwpmodule.dao.post.Author
import com.mcontigo.view.fragments.*

@SuppressLint("WrongConstant")
class AuthorViewPagerAdapter(
    fragmentManager: FragmentManager,
    val numberOfTab: Int,
    val title: List<String>,
    val author: Author
) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    val TAG = this@AuthorViewPagerAdapter.javaClass.simpleName

    override fun getItem(position: Int): Fragment {
        val fragment: Fragment
        when (position) {
            0 -> fragment = AuthorAboutFragment.newInstance(author.description ?: "")
            1 -> fragment = if(author.posts?.published != 0L) AuthorPostsFragment.newInstance(author) else AuthorReviewedPostsFragment.newInstance(author)
            else -> fragment = AuthorReviewedPostsFragment.newInstance(author)

        }
        return fragment
    }

    override fun getCount(): Int = numberOfTab

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }

}