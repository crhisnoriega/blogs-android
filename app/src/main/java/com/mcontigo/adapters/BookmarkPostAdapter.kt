package com.mcontigo.adapters

import android.view.ViewGroup
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.ItemBookmarkLeftRecycleviewBinding
import com.mcontigo.databinding.ItemBookmarkRecycleviewBinding
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.viewholders.BookmarkLeftViewHolder
import com.mcontigo.viewholders.BookmarkViewHolder

class BookmarkPostAdapter(
    private var listPosts: List<Post>,
    private val listener: PostContentRvInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val RIGHT = 0
    private val LEFT = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val mContext = parent.context
        when (viewType){
            RIGHT -> {
                val binding : ItemBookmarkRecycleviewBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_bookmark_recycleview,
                    parent,
                    false
                )
                return BookmarkViewHolder(binding, mContext, listener)
            }
            else -> {
                val binding : ItemBookmarkLeftRecycleviewBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_bookmark_left_recycleview,
                    parent,
                    false
                )
                return BookmarkLeftViewHolder(binding, mContext, listener)
            }
        }
    }

    override fun getItemCount(): Int {
        return listPosts.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item: Post = listPosts[position]
        val category = item.categories?.first()?.name
        if(holder is BookmarkViewHolder){
            holder.bind(item, category)
        }else if(holder is BookmarkLeftViewHolder){
            holder.bind(item, category)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return position%2
    }

    fun setListPost(lt: List<Post>){
        listPosts = lt
    }
}