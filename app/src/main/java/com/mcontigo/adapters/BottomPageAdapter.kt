package com.mcontigo.adapters

import android.annotation.SuppressLint
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.view.fragments.BookmarkFragment
import com.mcontigo.view.fragments.HomeFragment
import com.mcontigo.view.fragments.SearchFragment
import com.mcontigo.view.fragments.SettingsFragment

@SuppressLint("WrongConstant")
class BottomPageAdapter(fm: Fragment) :
    FragmentStateAdapter(fm) {

    override fun getItemCount(): Int {
        if(BookmarkUtil.isEnabled()){
            return 4
        }
        return 3
    }

    override fun createFragment(position: Int): Fragment {
        if(BookmarkUtil.isEnabled()){
            return when (position) {
                0 -> SearchFragment()
                1 -> HomeFragment()
                2 -> BookmarkFragment()
                else -> SettingsFragment()
            }
        }
        return when (position) {
            0 -> SearchFragment()
            1 -> HomeFragment()
            else -> SettingsFragment()
        }
    }

    val TAG = this@BottomPageAdapter.javaClass.simpleName

}

