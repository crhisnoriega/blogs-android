package com.mcontigo.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.ads.mediation.admob.AdMobAdapter
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.*
import com.mcontigo.interfaces.ItemPostRvInterface
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.viewholders.*

class HomeGridPostAdapter(
    private val listPosts: List<PostContentHome>?,
    private val listener: ItemPostRvInterface,
    private val postType: Int = 1,
    private val contentType: String = CONTENT_TYPE_CATEGORY,
    private val widgetType: Int = WIDGET_DEFAULT
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        val WIDGET_DEFAULT = 0
        val WIDGET_SLIDER = 2

        val CONTENT_TYPE_CATEGORY = "posts_by_category"
        val CONTENT_TYPE_FEATURED_POSTS = "featured_posts"
        val CONTENT_TYPE_RECOMMENDS = "recommends"
        val CONTENT_ADS = "ads"
        val CONTENT_ADS_FIRST = "first_ad"

        val POST_STYLE_WITH_SPOTLIGHT_INITAL_LEFT = 1
        val POST_STYLE_WITH_SPOTLIGHT_INITAL_RIGHT = 2
        val POST_STYLE_WITH_SPOTLIGHT_INITAL_LEFT_DESCRIPTION = 3
        val POST_STYLE_WITH_SPOTLIGHT_INITAL_RIGHT_DESCRIPTION = 4
        val POST_STYLE_WITHOUT_SPOTLIGHT_RIGHT = 5
        val POST_STYLE_WITH_SPOTLIGHT_AND_HORIZONTAL_SCROLL = 6
        val POST_STYLE_WITHOUT_SPOTLIGHT_LEFT = 7
        val UNIFIED_NATIVE_AD_VIEW_TYPE = 8
    }

    private val ITEM_SPOTLIGHT = 1
    private val ITEM_LEFT = 2
    private val ITEM_RIGHT = 3
    private val ITEM_LIST_HORIZONTAL = 4
    private val ITEM_LIST_ONLY_HORIZONTAL = 5
    private val ITEM_LIST_HORIZONTAL_STYLE_6 = 6


    private lateinit var binding: ItemPostRecycleviewBinding
    private lateinit var bindingSpotlightBinding: ItemPostRecyclerviewSpotlightBinding
    private lateinit var bindingImgRight: ItemPostImgRightRecycleviewBinding
    private lateinit var bindingSlider: HomeGridSliderBinding
    private lateinit var bindingOnlyHorizontal: HomeOnlySliderBinding
    private lateinit var bindingAd: CardBannerExpressAdContainerBinding
    private lateinit var bindingAdMultiplesSize: CardMultplesSizeBannerExpressAdContainerBinding


    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context

        val prefs = context.getSharedPreferences("consentAds", 0)
        val nonPersonalizedAds = prefs.getBoolean("nonPersonalizedAds", false)

        val adRequestExtra = Bundle()
        if(nonPersonalizedAds){
            adRequestExtra.putString("npa", "1")
        }

        if (contentType == CONTENT_TYPE_CATEGORY && widgetType == WIDGET_DEFAULT) {
            when (viewType) {
                ITEM_SPOTLIGHT -> {
                    bindingSpotlightBinding = DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_post_recyclerview_spotlight,
                        parent,
                        false
                    )

                    return ItemPostSpotlightViewHolder(bindingSpotlightBinding, context, listener)
                }
                ITEM_LEFT -> {
                    binding = DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_post_recycleview,
                        parent,
                        false
                    )

                    return ItensPostDetailsViewHolder(binding, context, listener)
                }
                ITEM_LIST_HORIZONTAL -> {
                    bindingSlider = DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.home_grid_slider,
                        parent,
                        false
                    )

                    return HomeSliderViewHolder(bindingSlider, context, listener)
                }

                ITEM_RIGHT -> bindingImgRight = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post_img_right_recycleview,
                    parent,
                    false
                )
                else -> {
                    bindingImgRight = DataBindingUtil.inflate(
                        LayoutInflater.from(parent.context),
                        R.layout.item_post_img_right_recycleview,
                        parent,
                        false
                    )
                }
            }
        } else if (contentType == CONTENT_TYPE_FEATURED_POSTS || contentType == CONTENT_TYPE_RECOMMENDS || widgetType == WIDGET_SLIDER) {
            bindingOnlyHorizontal = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.home_only_slider,
                parent,
                false
            )

            return HomeOnlySliderViewHolder(bindingOnlyHorizontal, context, listener)
        } else if (contentType == CONTENT_ADS_FIRST) {
            bindingAd = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.card_banner_express_ad_container,
                parent,
                false
            )
            return UnifiedBannerAdViewHolder(bindingAd,
                PublisherAdRequest.Builder()
                .addNetworkExtrasBundle(AdMobAdapter::class.java, adRequestExtra)
                .build())
        } else if (contentType == CONTENT_ADS) {
            bindingAdMultiplesSize = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.card_multples_size_banner_express_ad_container,
                parent,
                false
            )
            return UnifiedNativeBannerMultiplesSizesAdViewHolder(bindingAdMultiplesSize,
                PublisherAdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter::class.java, adRequestExtra)
                    .setContentUrl(ConfigurationLib.baseUrl)
                    .build())
        }
        return ItensPostImgRightDetailsViewHolder(bindingImgRight, context, listener)

    }

    override fun getItemViewType(position: Int): Int {

        if (contentType == CONTENT_TYPE_CATEGORY && widgetType == WIDGET_DEFAULT) {
            when (postType) {
                POST_STYLE_WITH_SPOTLIGHT_INITAL_LEFT, POST_STYLE_WITH_SPOTLIGHT_INITAL_LEFT_DESCRIPTION -> {
                    return if (position == 0) ITEM_SPOTLIGHT else if (position > 0 && position % 2 == 0) ITEM_RIGHT else ITEM_LEFT
                }
                POST_STYLE_WITH_SPOTLIGHT_INITAL_RIGHT, POST_STYLE_WITH_SPOTLIGHT_INITAL_RIGHT_DESCRIPTION -> {
                    return if (position == 0) ITEM_SPOTLIGHT else if (position > 0 && position % 2 == 0) ITEM_LEFT else ITEM_RIGHT
                }
                POST_STYLE_WITHOUT_SPOTLIGHT_RIGHT -> {
                    return if (position % 2 == 0) ITEM_RIGHT else ITEM_LEFT
                }
                POST_STYLE_WITHOUT_SPOTLIGHT_LEFT -> {
                    return if (position % 2 == 0) ITEM_LEFT else ITEM_RIGHT
                }
                POST_STYLE_WITH_SPOTLIGHT_AND_HORIZONTAL_SCROLL -> {
                    return ITEM_LIST_HORIZONTAL
                }
            }
        } else if (contentType == CONTENT_TYPE_FEATURED_POSTS || contentType == CONTENT_TYPE_RECOMMENDS || widgetType == WIDGET_SLIDER) {
            return ITEM_LIST_ONLY_HORIZONTAL
        } else if (contentType == CONTENT_ADS || contentType == CONTENT_ADS_FIRST) {
            return UNIFIED_NATIVE_AD_VIEW_TYPE
        }
        return if (position == 0) ITEM_SPOTLIGHT else if (position > 0 && position % 2 != 0) ITEM_LEFT else ITEM_RIGHT
    }

    override fun getItemCount(): Int {
        if (postType == POST_STYLE_WITH_SPOTLIGHT_AND_HORIZONTAL_SCROLL
            || contentType == CONTENT_TYPE_FEATURED_POSTS
            || contentType == CONTENT_TYPE_RECOMMENDS
            || widgetType == WIDGET_SLIDER
        ) return 1
        return listPosts?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (contentType == CONTENT_ADS || contentType == CONTENT_ADS_FIRST) {

        } else if (contentType == CONTENT_TYPE_CATEGORY && widgetType == WIDGET_DEFAULT) {
            when (postType) {
                POST_STYLE_WITH_SPOTLIGHT_INITAL_LEFT, POST_STYLE_WITH_SPOTLIGHT_INITAL_LEFT_DESCRIPTION -> {
                    if (position == 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItemPostSpotlightViewHolder =
                                holder as ItemPostSpotlightViewHolder
                            holderVH.bind(it, postType == 1)
                        }
                    } else if (position > 0 && position % 2 == 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostImgRightDetailsViewHolder =
                                holder as ItensPostImgRightDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }
                    } else {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostDetailsViewHolder =
                                holder as ItensPostDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }
                    }

                }
                POST_STYLE_WITH_SPOTLIGHT_INITAL_RIGHT, POST_STYLE_WITH_SPOTLIGHT_INITAL_RIGHT_DESCRIPTION -> {
                    if (position == 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItemPostSpotlightViewHolder =
                                holder as ItemPostSpotlightViewHolder
                            holderVH.bind(it, postType == 2)
                        }
                    } else if (position > 0 && position % 2 == 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostDetailsViewHolder =
                                holder as ItensPostDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }
                    } else {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostImgRightDetailsViewHolder =
                                holder as ItensPostImgRightDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }
                    }
                }
                POST_STYLE_WITHOUT_SPOTLIGHT_RIGHT -> {
                    if (position % 2 == 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostImgRightDetailsViewHolder =
                                holder as ItensPostImgRightDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }
                    } else {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostDetailsViewHolder =
                                holder as ItensPostDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }

                    }
                }
                POST_STYLE_WITHOUT_SPOTLIGHT_LEFT -> {
                    if (position % 2 != 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostImgRightDetailsViewHolder =
                                holder as ItensPostImgRightDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }
                    } else {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostDetailsViewHolder =
                                holder as ItensPostDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }

                    }
                }
                POST_STYLE_WITH_SPOTLIGHT_AND_HORIZONTAL_SCROLL -> {
                    val holderVH: HomeSliderViewHolder = holder as HomeSliderViewHolder
                    listPosts?.let { holderVH.bind(it) }
                }
                else -> {
                    if (position == 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItemPostSpotlightViewHolder =
                                holder as ItemPostSpotlightViewHolder
                            holderVH.bind(it)
                        }
                    } else if (position > 0 && position % 2 == 0) {
                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostImgRightDetailsViewHolder =
                                holder as ItensPostImgRightDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }
                    } else {

                        listPosts?.get(position)?.let {
                            val holderVH: ItensPostDetailsViewHolder =
                                holder as ItensPostDetailsViewHolder
                            holderVH.bind(it)
                            if (position == itemCount - 1) holderVH.itemBinding.divider.visibility =
                                View.GONE
                        }

                    }
                }

            }
        } else if (contentType == CONTENT_TYPE_FEATURED_POSTS || contentType == CONTENT_TYPE_RECOMMENDS || widgetType == WIDGET_SLIDER) {
            val holderVH: HomeOnlySliderViewHolder = holder as HomeOnlySliderViewHolder
            listPosts?.let { holderVH.bind(it) }
        }

    }
}