package com.mcontigo.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.Options
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.HomeGridSectionLayoutBinding
import com.mcontigo.interfaces.ItemPostRvInterface
import com.mcontigo.viewholders.HomeSectionViewHolder
import com.mcontigo.viewholders.ItemSlideDescriptionViewHolder


class HomeSectionsAdapter(
    private val listPosts: List<Pair<Options, List<PostContentHome>>>?,
    private val listener: ItemPostRvInterface,
    private val showingSwipeDescription: Boolean = true,
    val postStyleDefault: Int = HomeGridPostAdapter.POST_STYLE_WITH_SPOTLIGHT_AND_HORIZONTAL_SCROLL

) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private lateinit var binding: HomeGridSectionLayoutBinding
    private lateinit var context: Context


    private val ITEM_DESCRIPTION_SLIDE = 1
    private val HOME_SECTION = 2

    private var offsetItem = 1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context

        if (!showingSwipeDescription) offsetItem = 0

        when (viewType) {
            HOME_SECTION -> {
                binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.home_grid_section_layout,
                    parent,
                    false
                )

                return HomeSectionViewHolder(binding, context, listener, postStyleDefault)
            }
        }

        return ItemSlideDescriptionViewHolder.create(parent)

    }

    override fun getItemViewType(position: Int): Int {
        return if (position > 0 || !showingSwipeDescription)
            HOME_SECTION
        else
            ITEM_DESCRIPTION_SLIDE

    }

    override fun getItemCount(): Int {
        return if (!showingSwipeDescription) listPosts?.size!! else listPosts?.size!! + 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (position == 1 && showingSwipeDescription) {
            val holderVH = holder as HomeSectionViewHolder
            holderVH.itemBinding.tvHeaderSection.visibility = View.GONE
            holderVH.itemBinding.divider.visibility = View.GONE
            listPosts?.get(0)?.let { holderVH.bind(it) }
        } else if (position == 2) {
            val holderVH = holder as HomeSectionViewHolder
            holderVH.itemBinding.tvHeaderSection.visibility = View.GONE
            holderVH.itemBinding.divider.visibility = View.GONE
            listPosts?.get(1)?.let { holderVH.bind(it) }
        } else if (position > 2 || !showingSwipeDescription) {
            val holderVH = holder as HomeSectionViewHolder
            holderVH.itemBinding.divider.visibility = View.VISIBLE
            if (listPosts?.get(position - offsetItem)?.first?.contentType == HomeGridPostAdapter.CONTENT_ADS) {
                holderVH.itemBinding.tvHeaderSection.visibility = View.GONE
            } else {
                holderVH.itemBinding.tvHeaderSection.visibility = View.VISIBLE
            }

            listPosts?.get(position - offsetItem)?.let { holderVH.bind(it) }


        }

    }


}


