package com.mcontigo.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.ItemPostRecyclerviewSliderBinding
import com.mcontigo.interfaces.ItemPostRvInterface
import com.mcontigo.viewholders.ItemPostSliderViewHolder

class ItemPostSliderAdapter(
    private val listPosts: List<PostContentHome>?,
    private val listener: ItemPostRvInterface
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    private lateinit var binding: ItemPostRecyclerviewSliderBinding
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context


        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_post_recyclerview_slider,
            parent,
            false
        )

        return ItemPostSliderViewHolder(binding, context, listener)

    }

    override fun getItemCount(): Int {
        Log.d("HomeGrid", "${listPosts?.size}")
        return listPosts?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        listPosts?.get(position)?.let {
            val holderVH: ItemPostSliderViewHolder = holder as ItemPostSliderViewHolder
            holderVH.bind(it)
        }
    }
}