package com.mcontigo.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.mcontigo.BuildConfig
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.util.JsonUtil
import com.mcontigo.databinding.CardMultplesSizeBannerExpressAdContainerBinding
import com.mcontigo.databinding.ItemPostImgRightRecycleviewBinding
import com.mcontigo.databinding.ItemPostRecyclerviewSpotlightBinding
import com.mcontigo.databinding.ItemPostRecycleviewBinding
import com.mcontigo.enums.State
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.viewholders.ItemPostSpotlightPerCategoryViewHolder
import com.mcontigo.viewholders.ListFooterViewHolder
import com.mcontigo.viewholders.PostContentDetailsViewHolder
import com.mcontigo.viewholders.UnifiedNativeBannerMultiplesSizesAdViewHolder


class ItensPostAdapter(
    private val listener: PostContentRvInterface,
    val categoryName: String = "",
    private val retry: () -> Unit
) :
    PagedListAdapter<Post, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    private var adRequest: PublisherAdRequest? = null

    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2
    private val DATA_VIEW_TYPE_RIGHT = 3
    private val DATA_VIEW_TYPE_SPOTLIGHT = 4
    private val ADS = 5


    private var state = State.LOADING

    private lateinit var binding: ItemPostRecycleviewBinding
    private lateinit var bindingImgRight: ItemPostImgRightRecycleviewBinding
    private lateinit var bindingSpotlight: ItemPostRecyclerviewSpotlightBinding
    private lateinit var bindingAd: CardMultplesSizeBannerExpressAdContainerBinding
    private lateinit var context: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return when (viewType) {
            DATA_VIEW_TYPE_SPOTLIGHT -> {
                context = parent.context

                bindingSpotlight = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post_recyclerview_spotlight,
                    parent,
                    false
                )
                ItemPostSpotlightPerCategoryViewHolder(bindingSpotlight, context, listener)
            }
            DATA_VIEW_TYPE -> {
                context = parent.context

                binding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post_recycleview,
                    parent,
                    false
                )
                PostContentDetailsViewHolder(binding, context, listener)
            }
            DATA_VIEW_TYPE_RIGHT -> {
                context = parent.context

                bindingImgRight = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post_img_right_recycleview,
                    parent,
                    false
                )
                PostContentDetailsViewHolder(bindingImgRight, context, listener)
            }

            ADS -> {
                bindingAd = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.card_multples_size_banner_express_ad_container,
                    parent,
                    false
                )
                return UnifiedNativeBannerMultiplesSizesAdViewHolder(
                    bindingAd,
                    if (adRequest != null) adRequest!! else PublisherAdRequest
                        .Builder()
                        .setContentUrl(ConfigurationLib.baseUrl + categoryName)
                        .addCustomTargeting("domain", ConfigurationLib.baseUrl)
                        .addCustomTargeting("type", "box")
                        .addCustomTargeting("env", "mobile")
                        .addCustomTargeting(
                            "vertical",
                            if (BuildConfig.FLAVOR == "mcs") "health (mcs)" else " psychology (lmem)"
                        )
                        .addCustomTargeting("position", (itemCount + 1).toString())
                        .build()
                )
            }

            else -> ListFooterViewHolder.create(retry, parent)
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == ADS) {

        } else if (getItemViewType(position) == DATA_VIEW_TYPE || getItemViewType(position) == DATA_VIEW_TYPE_RIGHT) {
            val itemPost = getItem(position)
            itemPost?.let {
                (holder as PostContentDetailsViewHolder).bind(
                    it,
                    if (it.categories?.isEmpty() == false) it.categories?.get(0)?.name
                        ?: "" else categoryName
                )
            }
        } else if (getItemViewType(position) == DATA_VIEW_TYPE_SPOTLIGHT) {
            val itemPost = getItem(position)
            itemPost?.let {
                (holder as ItemPostSpotlightPerCategoryViewHolder).bind(
                    it,
                    if (it.categories?.isEmpty() == false) it.categories?.get(0)?.name
                        ?: "" else categoryName
                )
            }
        } else (holder as ListFooterViewHolder).bind(state)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) {
            when {
                currentList?.get(position)?.title == JsonUtil.CONTENT_ADS -> {

                    adRequest = PublisherAdRequest
                        .Builder()
                        .setContentUrl(ConfigurationLib.baseUrl + categoryName)
                        .addCustomTargeting("domain", ConfigurationLib.baseUrl)
                        .addCustomTargeting("type", "box")
                        .addCustomTargeting("env", "mobile")
                        .addCustomTargeting(
                            "vertical",
                            if (BuildConfig.FLAVOR == "mcs") "health (mcs)" else " psychology (lmem)"
                        )
                        .addCustomTargeting("position", position.toString())
                        .build()

                    ADS
                }
                position == 0 -> DATA_VIEW_TYPE_SPOTLIGHT
                position % 2 == 0 -> DATA_VIEW_TYPE
                else -> DATA_VIEW_TYPE_RIGHT
            }
        } else FOOTER_VIEW_TYPE
    }


    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<Post> =
            object : DiffUtil.ItemCallback<Post>() {

                override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                    return oldItem.id?.equals(newItem.id) ?: false
                }

                override fun areContentsTheSame(
                    oldItem: Post,
                    newItem: Post
                ): Boolean {
                    return oldItem == newItem
                }
            }
    }

    override fun getItemCount(): Int {
        return (super.getItemCount()) + if (hasFooter()) 1 else 0
    }


    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }


}