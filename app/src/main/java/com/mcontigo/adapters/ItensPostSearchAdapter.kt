package com.mcontigo.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.ItemPostRecyclerviewSpotlightBinding
import com.mcontigo.enums.State
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.viewholders.ItemPostSearchViewHolder
import com.mcontigo.viewholders.ListFooterViewHolder


class ItensPostSearchAdapter(private val listener: PostContentRvInterface,private val retry: () -> Unit) :
    PagedListAdapter<Post, RecyclerView.ViewHolder>(DIFF_CALLBACK) {


    private val DATA_VIEW_TYPE = 1
    private val FOOTER_VIEW_TYPE = 2

    private var state = State.LOADING

    private lateinit var binding: ItemPostRecyclerviewSpotlightBinding
    private lateinit var context: Context


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context

        return if (viewType == DATA_VIEW_TYPE) {
            context = parent.context

            binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_post_recyclerview_spotlight,
                parent,
                false
            )
            ItemPostSearchViewHolder(binding, context, listener)
        } else ListFooterViewHolder.create(retry, parent)
    }



    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == DATA_VIEW_TYPE) {
            val itemPost = getItem(position)
            itemPost?.let { (holder as ItemPostSearchViewHolder).bind(it) }
        } else (holder as ListFooterViewHolder).bind(state)
    }


    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount()) DATA_VIEW_TYPE else FOOTER_VIEW_TYPE
    }


    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<Post> = object : DiffUtil.ItemCallback<Post>() {

            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.id?.equals(newItem.id) ?: false
            }

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem == newItem
            }
        }
    }


    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasFooter()) 1 else 0
    }

    private fun hasFooter(): Boolean {
        return super.getItemCount() != 0 && (state == State.LOADING || state == State.ERROR)
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }


}