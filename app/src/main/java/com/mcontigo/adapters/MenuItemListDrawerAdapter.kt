package com.mcontigo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.menu.Item
import com.mcontigo.databinding.MenuItemListItemBinding

class MenuItemListDrawerAdapter(val listItem: List<Item>, val clickListener: (Int) -> Unit) :
    RecyclerView.Adapter<MenuItemViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuItemViewHolder {
        val binding = DataBindingUtil.inflate<MenuItemListItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.menu_item_list_item,
            parent,
            false
        )
        return MenuItemViewHolder(binding)
    }

    override fun getItemCount(): Int = listItem.size

    override fun onBindViewHolder(holder: MenuItemViewHolder, position: Int) {
        holder.itemBinding.tvMenuTitle.text = listItem[position].name


        if(position == listItem.size - 1) holder.itemBinding.divider.visibility = View.GONE

        holder.itemBinding.itemMenu.setOnClickListener {
            clickListener((position + 1))
        }
    }



}

class MenuItemViewHolder(val itemBinding: MenuItemListItemBinding) : RecyclerView.ViewHolder(itemBinding.root)
