package com.mcontigo.adapters

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.mcontigo.androidwpmodule.dao.menu.Item
import com.mcontigo.view.fragments.HomeGridFragment
import com.mcontigo.view.fragments.ItensPostFragment

@SuppressLint("WrongConstant")
class PageAdapter(fragmentManager: FragmentManager, val endPointCategories: List<Item>) :
    FragmentPagerAdapter(fragmentManager ,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    val TAG = this@PageAdapter.javaClass.simpleName

    companion object {
        val CATEGORY_SLUG = "categoryId"
        val CATEGORY_NAME = "categoryName"
    }

    override fun getItem(position: Int): Fragment {
        val bundle = Bundle()
        var fragment: Fragment
        if (position == 0) {
            fragment = HomeGridFragment()
        } else {
            Log.d(TAG, "Category -> " +  endPointCategories[position].itemId.toString())
            bundle.putInt(CATEGORY_SLUG, endPointCategories[position].itemId ?: 0)
            bundle.putString(CATEGORY_NAME, endPointCategories[position].name)
            fragment = ItensPostFragment()
            fragment.arguments = bundle
        }
        return fragment
    }

    override fun getCount(): Int = endPointCategories.size

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return endPointCategories[position].name
    }


}