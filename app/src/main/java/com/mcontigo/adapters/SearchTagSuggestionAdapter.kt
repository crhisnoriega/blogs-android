package com.mcontigo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.TagSuggestion
import com.mcontigo.databinding.ItemCategorySearchRecycleviewBinding
import com.mcontigo.interfaces.ItemCategorySuggetionInterface
import com.mcontigo.viewholders.SearchTagSuggestionViewHolder


class SearchTagSuggestionAdapter(private val listener: ItemCategorySuggetionInterface) :
    PagedListAdapter<TagSuggestion, SearchTagSuggestionViewHolder>(DIFF_CALLBACK) {


    private lateinit var binding: ItemCategorySearchRecycleviewBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchTagSuggestionViewHolder {
            binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_category_search_recycleview,
                parent,
                false
            )
            return SearchTagSuggestionViewHolder(binding)
    }


    override fun onBindViewHolder(holder: SearchTagSuggestionViewHolder, position: Int) {
        if (position < 6) {
            val tagSuggestion = getItem(position)
            tagSuggestion?.let { holder.bind(it, listener) }
        }else{
            holder.itemBinding.root.visibility = View.GONE
        }
    }


    companion object {
        private val DIFF_CALLBACK: DiffUtil.ItemCallback<TagSuggestion> =
            object : DiffUtil.ItemCallback<TagSuggestion>() {


                override fun areItemsTheSame(oldItem: TagSuggestion, newItem: TagSuggestion): Boolean {
                    return oldItem.id?.equals(newItem.id)!!
                }

                override fun areContentsTheSame(oldItem: TagSuggestion, newItem: TagSuggestion): Boolean {
                    return oldItem == newItem
                }
            }
    }
}