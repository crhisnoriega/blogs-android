package com.mcontigo.app

import android.app.Application
import android.content.Context
import android.util.Log
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.mcontigo.BuildConfig
import com.mcontigo.R
import com.mcontigo.androidauthmodule.callback.UserCallBack
import com.mcontigo.androidauthmodule.model.User
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.androidauthmodule.util.TypeAuth
import com.mcontigo.androidwpmodule.util.CacheDirUtil
import com.mcontigo.utils.*
import com.mcontigo.utils.ConfigurationLib.baseUrl
import com.onesignal.OneSignal
import com.taboola.android.PublisherInfo
import com.taboola.android.Taboola
import io.sentry.Sentry
import io.sentry.android.AndroidSentryClientFactory
import java.util.*

import com.zeugmasolutions.localehelper.LocaleHelperApplicationDelegate

import android.content.res.Configuration 

class BlogAppApplication : Application() {

    private val TAG = this@BlogAppApplication.javaClass.simpleName
    private val localeAppDelegate = LocaleHelperApplicationDelegate()


    companion object {
        lateinit var instance: Context private set
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(localeAppDelegate.attachBaseContext(base))
    }
    
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localeAppDelegate.onConfigurationChanged(this)
    } 

    override fun onCreate() {
        super.onCreate()
        instance = this

        val authUtil = AuthUtil(this, ConfigurationLib.authUrl)

        if(authUtil.getTypeLogin() === TypeAuth.GOOGLE){
            val account = GoogleSignIn.getLastSignedInAccount(this)
            account?.also {
                LoginSocialNetworksUtil.googleAuth(authUtil, account){
                    UserUtil.userAuthToken = it?.token ?: ""
                    UserUtil.user.postValue(it?.user)
                }
            }
        }
        else if (isLoggedInFacebook() && authUtil.getTypeLogin() == TypeAuth.FACEBOOK)
            LoginSocialNetworksUtil.authFacebook(
                authUtil, AccessToken.getCurrentAccessToken()
            ) {
                UserUtil.userAuthToken = it?.token ?: ""
                UserUtil.user.postValue(it?.user)
            }
        else if (!authUtil.getTokenAccess().isNullOrEmpty()) {
            UserUtil.userAuthToken = authUtil.getTokenAccess() ?: ""
        }




        authUtil.getUser(
            ConfigurationLib.getHeaderWithAuthorizationToken(
                authUtil.getTokenAccess() ?: ""
            ), object : UserCallBack {
                override fun onBegin() {

                }

                override fun onSuccess(user: User) {
                    UserUtil.user.postValue(user)
                }

                override fun onFailed(error: String) {

                }


            })

        ClearCacheUtil.deleteCache(this)
        CacheDirUtil.cacheDir = this.cacheDir
        baseUrl = getString(R.string.url)

        Log.d("Language", Locale.getDefault().language)

        if (ConfigurationLib.ONE_SIGNAL_LOCALE_FILTER.contains(Locale.getDefault().language)) {

            // OneSignal Initialization
            OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(PostInAppMessageClickHandler())
                .init()


            OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)


            Log.d(
                TAG,
                "player id -> ${OneSignal.getPermissionSubscriptionState().subscriptionStatus.userId}"
            )
        }


        val publisherInfo = PublisherInfo(getString(R.string.taboola_key))
        Taboola.init(publisherInfo)
        if (BuildConfig.DEBUG) {
            Taboola.verifyIntegration(false)
        }



        Sentry.init(
            getString(R.string.sentry_url_key),
            AndroidSentryClientFactory(this)
        );
    }

    private fun isLoggedInFacebook() =
        AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired

}