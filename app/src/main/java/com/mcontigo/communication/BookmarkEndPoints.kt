package com.mcontigo.communication

import com.mcontigo.androidwpmodule.dao.Content
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.dao.bookmark.IdReference
import retrofit2.Response
import retrofit2.http.*

interface BookmarkEndPoints{
    @GET("bookmark/ids")
    suspend fun findIds(@HeaderMap headerMap: Map<String, String>): Response<List<IdReference>>

    @GET("bookmark/posts")
    suspend fun find(@HeaderMap headerMap: Map<String, String>, @QueryMap params:  Map<String, String>): Response<List<Post>>

    @GET("bookmark/posts/{postId}")
    suspend fun find(@HeaderMap headerMap: Map<String, String>, @Path("postId") id: Int): Response<Post>

    @POST("bookmark/posts")
    suspend fun create(@HeaderMap headerMap: Map<String, String>, @Body content:Post): Response<String>

    @POST("bookmark/posts")
    suspend fun create(@HeaderMap headerMap: Map<String, String>, @Body content: PostContentHome): Response<String>

    @DELETE("bookmark/posts/{postId}")
    suspend fun remove(@HeaderMap headerMap: Map<String, String>, @Path("postId") id: Int?): Response<String>
}