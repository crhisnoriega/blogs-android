package com.mcontigo.dao.bookmark

import com.google.gson.annotations.SerializedName

data class IdReference (
    @SerializedName("id")
    val id: Int
)