package com.mcontigo.enums

enum class BookmarkState { LOADING, DONE, ON, OFF, ERROR, SIGNOUT }