package com.mcontigo.enums

enum class State { DONE, LOADING, ERROR }