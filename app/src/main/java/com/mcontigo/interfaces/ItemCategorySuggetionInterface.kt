package com.mcontigo.interfaces

interface ItemCategorySuggetionInterface {
    fun onSelectSuggestion(title : String, idTag : Int?)
}