package com.mcontigo.interfaces

import com.mcontigo.androidwpmodule.dao.PostContentHome

interface ItemPostRvInterface {
    fun onItemClick(itemPost: PostContentHome)
}