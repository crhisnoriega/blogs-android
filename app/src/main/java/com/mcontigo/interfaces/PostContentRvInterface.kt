package com.mcontigo.interfaces

import com.mcontigo.androidwpmodule.dao.post.Post

interface PostContentRvInterface {
    fun onItemClick(postContent: Post)
}