package com.mcontigo.paging.datasources

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.enums.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostSearchDataSource(
    private val postsRepository: PostsRepository,
    private val coroutineScope: CoroutineScope,
    private val search: String? = null,
    private val tagId: Int? = null
) : PageKeyedDataSource<Int, Post>() {

    private val TAG = this@PostSearchDataSource.javaClass.simpleName
    var state: MutableLiveData<State> = MutableLiveData()
    var retry: (() -> Unit?)? = null
        private set


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Post>
    ) {
        coroutineScope.launch(Dispatchers.IO) {

            updateState(State.LOADING)

            val postsSearched =
                postsRepository.getPostSearch(search, 1, params.requestedLoadSize, tagId)

            postsSearched?.also {
                updateState(State.DONE)
                callback.onResult(it, null, 2)
            } ?: run {
                updateState(State.ERROR)
                setRetry { loadInitial(params, callback) }
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Post>) {


        coroutineScope.launch(Dispatchers.IO) {

            updateState(State.LOADING)

            val postsSearched = postsRepository.getPostSearch(
                search,
                params.key,
                params.requestedLoadSize,
                tagId
            )

            postsSearched?.also {
                updateState(State.DONE)
                callback.onResult(it, params.key + 1)
            } ?: run {
                updateState(State.ERROR)
                setRetry { loadAfter(params, callback) }
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Post>) {}

    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    private fun setRetry(retry: () -> Unit) {
        this.retry = if (this.retry == null) null else retry
    }


}