package com.mcontigo.paging.datasources

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.mcontigo.androidwpmodule.dao.TagSuggestion
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.enums.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TagSuggestionDataSource(
    private val postsRepository: PostsRepository,
    private val coroutineScope: CoroutineScope
) : PageKeyedDataSource<Int, TagSuggestion>() {

    private val TAG = this@TagSuggestionDataSource.javaClass.simpleName
    private val ORDER_BY = "count"
    private val ORDER = "desc"

    var state: MutableLiveData<State> = MutableLiveData()
    var retry: (() -> Unit?)? = null
        private set


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, TagSuggestion>
    ) {


        coroutineScope.launch(Dispatchers.IO) {

            updateState(State.LOADING)

            val listTags =
                postsRepository.getTagsSuggestions(1, params.requestedLoadSize, ORDER_BY, ORDER)


            listTags?.also {
                updateState(State.DONE)
                callback.onResult(it, null, 2)
            } ?: run {
                updateState(State.ERROR)
                setRetry { loadInitial(params, callback) }
            }


        }

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, TagSuggestion>) {
//        wordPressUtil.getTagsSuggestions(
//            params.key,
//            params.requestedLoadSize,
//            ORDER_BY,
//            ORDER,
//            object : JsonArrayCallBack {
//                override fun onBegin() {
//                    updateState(State.LOADING)
//                }
//
//                override fun onSuccess(result: JsonArray) {
//
//                    compositeDisposable.add(
//                        JsonUtil.getTagSuggestion(result)
//                            .subscribeOn(Schedulers.io())
//                            .subscribe(
//                                {
//                                    updateState(State.DONE)
//                                    callback.onResult(it, params.key + 1)
//                                },
//                                {
//                                    updateState(State.ERROR)
//                                    setRetry(Action { loadAfter(params, callback) })
//                                }
//                            )
//                    )
//                }
//
//                override fun onError(error: String) {
//                    updateState(State.ERROR)
//                    setRetry(Action { loadAfter(params, callback) })
//                }
//
//            })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, TagSuggestion>) {}


    private fun updateState(state: State) {
        this.state.postValue(state)
    }

    private fun setRetry(retry: () -> Unit) {
        this.retry = if (this.retry == null) null else retry
    }


}