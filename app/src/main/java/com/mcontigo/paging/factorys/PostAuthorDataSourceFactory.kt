package com.mcontigo.paging.factorys

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.paging.datasources.PostAuthorDataSorce
import kotlinx.coroutines.CoroutineScope

class PostAuthorDataSourceFactory(
    private val postsRepository: PostsRepository,
    private val coroutineScope: CoroutineScope,
    private val authorId: Int,
    private val reviewed : Boolean = false
) : DataSource.Factory<Int, Post>() {
    val postDataSourceLiveData = MutableLiveData<PostAuthorDataSorce>()
    override fun create(): DataSource<Int, Post> {
        val postDataSorce =
            PostAuthorDataSorce(
                postsRepository,
                coroutineScope,
                authorId,
                reviewed
            )
        postDataSourceLiveData.postValue(postDataSorce)
        return postDataSorce
    }
}