package com.mcontigo.paging.factorys

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.paging.datasources.PostDataSorce
import kotlinx.coroutines.CoroutineScope

class PostDataSourceFactory(
    private val postsRepository: PostsRepository,
    private val couroutineScope: CoroutineScope,
    private val categoryId: Int
) : DataSource.Factory<Int, Post>() {
    val postDataSourceLiveData = MutableLiveData<PostDataSorce>()
    override fun create(): DataSource<Int, Post> {
        val postDataSorce =
            PostDataSorce(
                postsRepository,
                couroutineScope,
                categoryId
            )
        postDataSourceLiveData.postValue(postDataSorce)
        return postDataSorce
    }
}