package com.mcontigo.paging.factorys

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.paging.datasources.PostSearchDataSource
import kotlinx.coroutines.CoroutineScope

class PostSearchSourceFactory(
    private val postsRepository: PostsRepository,
    private val coroutineScope: CoroutineScope,
    private val search: String? = null,
    private val tagId: Int? = null
) : DataSource.Factory<Int, Post>() {
    val postSearchDataSourceLiveData = MutableLiveData<PostSearchDataSource>()
    override fun create(): DataSource<Int, Post> {
        val postSearchDataSource =
            PostSearchDataSource(
                postsRepository,
                coroutineScope,
                search,
                tagId
            )
        postSearchDataSourceLiveData.postValue(postSearchDataSource)
        return postSearchDataSource
    }
}