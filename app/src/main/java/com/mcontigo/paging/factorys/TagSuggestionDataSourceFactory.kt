package com.mcontigo.paging.factorys

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.mcontigo.androidwpmodule.dao.TagSuggestion
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.paging.datasources.TagSuggestionDataSource
import kotlinx.coroutines.CoroutineScope

class TagSuggestionDataSourceFactory(
    private val postsRepository: PostsRepository,
    private val coroutineScope: CoroutineScope
) : DataSource.Factory<Int, TagSuggestion>() {

    val tagSuggestionDataSourceLiveData = MutableLiveData<TagSuggestionDataSource>()

    override fun create(): DataSource<Int, TagSuggestion> {
        val tagSuggestionDataSource = TagSuggestionDataSource(
            postsRepository,
            coroutineScope
        )
        tagSuggestionDataSourceLiveData.postValue(tagSuggestionDataSource)
        return tagSuggestionDataSource
    }
}