package com.mcontigo.utils

import android.content.Context
import androidx.fragment.app.FragmentManager
import com.mcontigo.BuildConfig
import com.mcontigo.R
import com.mcontigo.view.dialogs.BottomRateDialog


object AppRater {

    private var appPName = BuildConfig.APPLICATION_ID// Package Name
    private var appTitle = ""

    private val DAYS_UNTIL_PROMPT = 3//Min number of days
    private val LAUNCHES_UNTIL_PROMPT = 3//Min number of launches

    fun appLaunched(mContext: Context, fragmentManager: FragmentManager) {

        appTitle = mContext.getString(R.string.app_name)// App Name

        val prefs = mContext.getSharedPreferences("apprater", 0)
        if (prefs.getBoolean("dontshowagain", false)) {
            return
        }

        val editor = prefs.edit()

        // Increment launch counter
        val launchCount = prefs.getLong("launch_count", 0) + 1
        editor.putLong("launch_count", launchCount)

        // Get date of first launch
        var dateFirstLaunch = prefs.getLong("date_firstlaunch", 0)
        if (dateFirstLaunch == 0L) {
            dateFirstLaunch = System.currentTimeMillis()
            editor.putLong("date_firstlaunch", dateFirstLaunch)
        }

        // Wait at least n days before opening
        if (launchCount >= LAUNCHES_UNTIL_PROMPT) {
            if (System.currentTimeMillis() >= dateFirstLaunch + DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000) {
                BottomRateDialog.newInstance().show(fragmentManager, "RATE")
            }
        }

        editor.apply()
    }

}