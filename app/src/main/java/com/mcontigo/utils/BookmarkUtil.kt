package com.mcontigo.utils

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mcontigo.BuildConfig
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.communication.RetrofitProvider
import com.mcontigo.view.dialogs.DialogBookmarkTutorial
import com.mcontigo.view.dialogs.DialogConnectToPay
import com.mcontigo.view.dialogs.CustomSnackbarDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

object BookmarkUtil {
    private const val IS_TUTORIAL_BOOKMARK_DONE = "isTutorialBookmarkDone"
    private val retrofitProvider: RetrofitProvider = RetrofitProvider(ConfigurationLib.authUrl)
    private var enableBookmark: Boolean? = null
    private var listIds: MutableLiveData<List<Int>> = MutableLiveData(arrayListOf())

    fun shouldUpdateBookmarkList(){
        fetchIds()
    }

    @Throws(IllegalArgumentException::class)
    fun toggleBookmark(context: Context, post: Any){
        var postId :Int? = null
        if(UserUtil.user.value == null){
            displayConnectPopup(context)
        }else if(post is Post){
            postId = post.id
        }else if(post is PostContentHome){
            postId = post.id
        }else{
            throw IllegalArgumentException("Post most be a Post or PostContentHome")
        }
        if(!isBookmarked(postId)){
            addBookmark(context, post)
        }else{
            removeBookmark(context, post)
        }
    }

    @Throws(IllegalArgumentException::class)
    private fun addBookmark(context: Context, post: Any){
        if(UserUtil.user.value == null){
            displayConnectPopup(context)
        }else{
            CoroutineScope(Dispatchers.IO).launch {
                var response : Response<String>
                var postId: Int?
                if(post is Post){
                    response = retrofitProvider.bookmark().create(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), post)
                    postId = post.id
                }else if(post is PostContentHome){
                    response = retrofitProvider.bookmark().create(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), post)
                    postId = post.id
                }else{
                    throw IllegalArgumentException("Post most be a Post or PostContentHome")
                }
                Log.d("TAG", response.isSuccessful.toString())
                shouldUpdateBookmarkList()
                if(response.code() == 201) {
                    forceUpdateId(true,postId)
                    if (!isTutorialBookmarkDone(context)) {
                        withContext(Dispatchers.Main.immediate) {
                            displayFirstBookmarkPopup(context)
                        }
                        setTutorialBookmarkDone(context)
                    } else {
                        withContext(Dispatchers.Main.immediate) {
                            CustomSnackbarDialog.newDialogBookmarkAdd(context).show()
                        }
                    }
                }else if(response.code() == 409){
                    withContext(Dispatchers.Main.immediate) {
                        CustomSnackbarDialog.newDialogBookmarkDuplicated(context).show()
                    }
                }else{
                    withContext(Dispatchers.Main.immediate){
                        var snackbarError = CustomSnackbarDialog.newDialogBookmarkError(context)
                        CustomSnackbarDialog.setErrorCodeToDialog(snackbarError, response.code().toString()).show()
                    }
                }
            }
        }
    }

    @Throws(IllegalArgumentException::class)
    private fun removeBookmark(context: Context, post: Any){
        if(UserUtil.user.value == null){
            displayConnectPopup(context)
        }else{
            var postId: Int?
            CoroutineScope(Dispatchers.IO).launch {
                var response : Response<String>
                if(post is Post){
                    response = retrofitProvider.bookmark().remove(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), post.id)
                    postId = post.id
                }else if(post is PostContentHome){
                    response = retrofitProvider.bookmark().remove(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), post.id)
                    postId = post.id
                }else{
                    throw IllegalArgumentException("Post most be a Post or PostContentHome")
                }
                shouldUpdateBookmarkList()
                if(response.isSuccessful) {
                    forceUpdateId(false,postId)
                    withContext(Dispatchers.Main.immediate) {
                        CustomSnackbarDialog.newDialogBookmarkRemoved(context, Runnable { addBookmark(context, post) }).show()
                    }
                }else{
                    withContext(Dispatchers.Main.immediate){
                        var snackbarError = CustomSnackbarDialog.newDialogBookmarkError(context)
                        CustomSnackbarDialog.setErrorCodeToDialog(snackbarError, response.code().toString()).show()
                    }
                }
            }
        }
    }

    private fun isBookmarked(postId: Int?): Boolean{
        var marked = false
        listIds.value?.also { marked = it.contains(postId) }
        return marked;
    }

    fun isEnabled(): Boolean {
        if(enableBookmark == null){
            enableBookmark = (ConfigurationLib.BOOKMARK_LOCALE_FILTER.contains(Locale.getDefault().language)
                    && ConfigurationLib.BOOKMARK_FLAVOR_FILTER.contains(BuildConfig.FLAVOR))
        }
        return enableBookmark!!
    }

    fun getPostListIds(): MutableLiveData<List<Int>> = listIds

    private fun forceUpdateId(isAdding:Boolean, id: Int?){
        if(isAdding){
            val list = listIds.value as ArrayList
            id?.let {
                list.add(it)
                listIds.postValue(list)
            }
        }else{
            val list = listIds.value as ArrayList
            id?.let {
                list.remove(it)
                listIds.postValue(list)
            }
        }
    }

    private fun fetchIds(){
        CoroutineScope(Dispatchers.IO).launch {
            var response = retrofitProvider.bookmark().findIds(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken))
            if(response.isSuccessful) {
                listIds.postValue(response.body()?.map { it -> it.id })
            }
        }
    }

    private fun displayConnectPopup(context: Context){
        val activity =  context as AppCompatActivity
        DialogConnectToPay.newInstance().show(activity.supportFragmentManager, DialogConnectToPay.DIALOG_TAG)
    }

    private fun displayFirstBookmarkPopup(context: Context){
        val activity =  context as AppCompatActivity
        DialogBookmarkTutorial.newInstance().show(activity.supportFragmentManager, DialogBookmarkTutorial.DIALOG_TAG)
    }

    private fun isTutorialBookmarkDone(context: Context): Boolean{
        return SharedPreferencesUtil(context).getPreference(IS_TUTORIAL_BOOKMARK_DONE)
    }

    private fun setTutorialBookmarkDone(context: Context){
        SharedPreferencesUtil(context).savePreference(IS_TUTORIAL_BOOKMARK_DONE, true)
    }



}