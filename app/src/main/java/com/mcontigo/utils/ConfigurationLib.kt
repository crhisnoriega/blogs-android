package com.mcontigo.utils

import com.mcontigo.BuildConfig

object ConfigurationLib{
    var baseUrl = "https://mejorconsalud.com/"
    var authUrl = "https://api.beta.lamenteesmaravillosa.com/"

    var endPointHome = "v1/home"
    var endPointPostsCategory = "mc/v1/posts/"

    val SECRET_KEY_MEJOR_KEY = "x-mejor-key" //"unycos"
    val SECRET_KEY_MEJOR_VALUE = "courses" //"unycos"
    val SECRET_KEY_APP_KEY = "x-mejor-app"
    val SECRET_KEY_APP_VALUE = "mobile_courses"
    val APP_VERSION_KEY = "x-app-version"
    val APP_VERSION_VALUE = BuildConfig.VERSION_NAME

    val ONE_SIGNAL_LOCALE_FILTER : List<String> = listOf("es")

    val LOGIN_LOCALE_FILTER : List<String> = listOf("es")
    val LOGIN_FLAVOR_FILTER : List<String> = listOf("lmem")

    val BOOKMARK_LOCALE_FILTER : List<String> = listOf("es")
    val BOOKMARK_FLAVOR_FILTER : List<String> = listOf("lmem")

    fun getHeaderWithAuthorizationToken(token: String) = hashMapOf(
        Pair(SECRET_KEY_MEJOR_KEY, SECRET_KEY_MEJOR_VALUE),
        Pair("Authorization", "Mejor $token"),
        Pair(SECRET_KEY_APP_KEY, SECRET_KEY_APP_VALUE),
        Pair(APP_VERSION_KEY, APP_VERSION_VALUE)
    )

    fun getHeaderDefault() = hashMapOf(
        Pair(SECRET_KEY_MEJOR_KEY, SECRET_KEY_MEJOR_VALUE),
        Pair(SECRET_KEY_APP_KEY, SECRET_KEY_APP_VALUE),
        Pair(APP_VERSION_KEY, APP_VERSION_VALUE)
    )
}