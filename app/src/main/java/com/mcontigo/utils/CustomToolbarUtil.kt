package com.mcontigo.utils

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData

@SuppressLint("StaticFieldLeak")
object CustomToolbarUtil {


    val observerExpandedToolbar  = MutableLiveData<Boolean>()
    val observerOpenDrawer  = MutableLiveData<Boolean>()


}