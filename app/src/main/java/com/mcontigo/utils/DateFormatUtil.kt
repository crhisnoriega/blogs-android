package com.mcontigo.utils

import android.util.Log
import java.text.DateFormat
import java.text.SimpleDateFormat

object DateFormatUtil {

    private val TAG = this@DateFormatUtil.javaClass.simpleName

    fun formatDateToString(dateApi: String): String {
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val dateFormated = simpleDateFormat.parse(dateApi)
        var dateFormatedString = dateApi
        try {
            dateFormatedString = DateFormat.getDateInstance(DateFormat.LONG).format(dateFormated)
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        return dateFormatedString
    }
}