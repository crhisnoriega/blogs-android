package com.mcontigo.utils

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import android.widget.TextView

object HtmlUtil {

    fun fromHtmlWithImageGetter(context: Context, tv: TextView, htmlText: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(
                htmlText,
                Html.FROM_HTML_MODE_LEGACY,
                MyImageGetter(context, tv),
                null
            )
        } else {
            Html.fromHtml(htmlText, MyImageGetter(context, tv), null)
        }
    }


    fun fromHtml(htmlText: String) : Spanned{
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(
                htmlText,
                Html.FROM_HTML_MODE_LEGACY
            )
        } else {
            Html.fromHtml(htmlText)
        }
    }

}