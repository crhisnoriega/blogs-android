package com.mcontigo.utils

import android.util.Log
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.mcontigo.R
import com.mcontigo.androidauthmodule.body.FacebookAuthBody
import com.mcontigo.androidauthmodule.callback.AuthCallBack
import com.mcontigo.androidauthmodule.model.UserResponse
import com.mcontigo.androidauthmodule.util.AuthUtil

class LoginSocialNetworksUtil {

    companion object {

        private val TAG = this@Companion.javaClass.simpleName

        fun authFacebook(authUtil: AuthUtil, accessToken: AccessToken, isSuccess : (UserResponse?) -> Unit) {

            val facebookAuthBody = FacebookAuthBody(
                accessToken.token,
                ""
            )

            authUtil.facebookAuth(ConfigurationLib.getHeaderDefault(), facebookAuthBody, object :
                AuthCallBack {
                override fun onBegin() {

                }

                override fun onSuccess(userResponse: UserResponse) {
                    isSuccess(userResponse)
                }

                override fun onFailed(error: String) {
                    Log.d(TAG, accessToken.toString())
                    isSuccess(null)
                }


            })


        }

        fun googleAuth(authUtil: AuthUtil,googleSignInAccount : GoogleSignInAccount,isSuccess : (UserResponse?) -> Unit){
            googleSignInAccount?.idToken?.also {
                authUtil.googleAuth(ConfigurationLib.getHeaderDefault(), it, object : AuthCallBack{
                    override fun onSuccess(userResponse: UserResponse) {
                        isSuccess(userResponse)
                    }

                    override fun onFailed(error: String) {
                        isSuccess(null)
                    }

                    override fun onBegin() {
                    }

                })
            }

        }
    }
}