package com.mcontigo.utils

import androidx.lifecycle.MutableLiveData
import com.mcontigo.androidwpmodule.dao.menu.Item

object MenuItensUtil{
    var listItens = MutableLiveData<List<Item>>()
    var itemSelected = MutableLiveData<Int>()
    var bottomNavigationSelected = MutableLiveData<Int>()
    var bottomPageSelected : Int = -1

    var bottomPageForceSelector = MutableLiveData<Int>()

    var facebookLink = ""
    var twitterLink = ""
    var pinterestLink = ""
    var instagramLink = ""
}