package com.mcontigo.utils

import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable


class MyDrawableWrapper(var drawable: Drawable?) : BitmapDrawable() {
    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        drawable?.draw(canvas)
    }
}