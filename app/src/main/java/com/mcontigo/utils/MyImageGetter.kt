package com.mcontigo.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.Html
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition

class MyImageGetter(val context: Context, val tvText: TextView) : Html.ImageGetter {
    override fun getDrawable(source: String?): Drawable {
        val myDrawableWrapper = MyDrawableWrapper(null)
        Glide.with(context)
            .asBitmap()
            .load(source)
            .fitCenter()
            .into(BitmapTarget(context, myDrawableWrapper, tvText))
        return myDrawableWrapper
    }
}

class BitmapTarget(val context: Context, val myDrawable: MyDrawableWrapper, val tv: TextView) : SimpleTarget<Bitmap>() {
    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
        val drawable = BitmapDrawable(context.resources, resource)

        val width = drawable.intrinsicWidth
        val height = drawable.intrinsicHeight

        val caculatedHeight = if(width > tv.width) height else height * 2

        drawable.setBounds(0, 0, tv.width, caculatedHeight)
        myDrawable.setBounds(0, 0, tv.width, caculatedHeight)
        myDrawable.drawable = drawable
        tv.text = tv.text
        tv.invalidate()
    }
}

