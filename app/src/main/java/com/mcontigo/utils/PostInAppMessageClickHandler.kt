package com.mcontigo.utils

import android.content.Intent
import androidx.core.net.toUri
import com.google.gson.Gson
import com.mcontigo.androidwpmodule.dao.NotificationOnePushAction
import com.mcontigo.app.BlogAppApplication
import com.mcontigo.view.MainHostActivity
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal


class PostInAppMessageClickHandler : OneSignal.NotificationOpenedHandler {
    override fun notificationOpened(result: OSNotificationOpenResult?) {
        val data = Gson().fromJson(result?.toJSONObject()?.toString(), NotificationOnePushAction::class.java)

        if (data != null) {
            val notificationAdditionalData = data.notification?.payload?.additionalData
            notificationAdditionalData?.deepLinking?.replace("?utm_source=marfeelpush", "")?.also {

                val intent = Intent(BlogAppApplication.instance, MainHostActivity::class.java)
                intent.data = it.toUri()
                intent.apply {
                    addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                }

                BlogAppApplication.instance.startActivity(intent)
            }
        }
    }
}