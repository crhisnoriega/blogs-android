package com.mcontigo.utils

data class ResponseStateUI <T>(
    val stateUI : StateUI = StateUI.DONE,
    val valueState : T? = null,
    val messageResId : Int? = null,
    val message : String? = null
)