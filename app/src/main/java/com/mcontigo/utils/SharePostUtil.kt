package com.mcontigo.utils

import android.content.Context
import android.content.Intent
import com.mcontigo.R


object SharePostUtil{
    fun sharePost(context: Context, linkPost : String){
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name))
            var shareMessage = "\n${context.getString(R.string.sharePostMsg)}\n\n $linkPost\n" +
                    "\n"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            context.startActivity(Intent.createChooser(shareIntent, "${context.getString(R.string.sharePost)} $linkPost"))
        } catch (e: Exception) {
            //e.toString();
        }

    }
}