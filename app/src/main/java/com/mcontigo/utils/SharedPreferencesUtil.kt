package com.mcontigo.utils

import android.content.Context


class SharedPreferencesUtil(context: Context){

    companion object {
        const val APP_PREF_KEY = "app_blog"

        const val THEME = "theme"
    }

    var sharedPreferences = context.getSharedPreferences(APP_PREF_KEY, Context.MODE_PRIVATE)

    fun savePreference(key : String, value : Boolean){
        val editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getPreference(key : String, valueDefault : Boolean = false):Boolean{
        return sharedPreferences.getBoolean(key, valueDefault)
    }

}