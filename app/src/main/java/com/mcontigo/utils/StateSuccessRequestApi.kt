package com.mcontigo.utils

data class StateSuccessRequestApi(
    val requestStatus: Int,
    val obj: Any? = null
) {
    companion object {
        val SUCCESS_REQUEST = 0
        val ERROR_REQUEST = 1
    }
}