package com.mcontigo.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.webkit.URLUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.mcontigo.androidauthmodule.model.User
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import java.io.File
import java.net.URL
import java.net.HttpURLConnection

object UserUtil {
    var userAuthToken: String = ""
    var user: MutableLiveData<User?> = MutableLiveData(null)


    fun loadPicture(url: String, context: Context): Bitmap? {

        var bitmap: Bitmap? = null

        if (URLUtil.isValidUrl(url)) {
            val file = Glide.with(context).asFile().load(url)

            var loaded = false;
            file.addListener(object : RequestListener<File> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<File>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: File?,
                    model: Any?,
                    target: Target<File>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource?.also {
                        loaded = true
                        bitmap =
                            BitmapFactory.decodeByteArray(
                                it.readBytes(),
                                0,
                                it.readBytes().size
                            )

                    }
                    return true
                }

            })

            file.submit()
            while(!loaded){
                Thread.sleep(100);
            }
            
            
         
        } else {
            val file = Base64.decode(url, Base64.DEFAULT)
            bitmap = BitmapFactory.decodeByteArray(file, 0, file.size)
        }
        return bitmap
    }
}