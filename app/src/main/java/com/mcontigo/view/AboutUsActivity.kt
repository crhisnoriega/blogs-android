package com.mcontigo.view


import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.mcontigo.R
import com.mcontigo.databinding.ActivityAboutUsBinding


class AboutUsActivity : BaseActivity() {

    lateinit var binding: ActivityAboutUsBinding
    val title : String = ""
    val url: String = ""


    companion object{
        val TITLE = "title"
        val URL = "url"

        fun open(context: Context, title: String ,url : String){
            val intent = Intent(context, AboutUsActivity::class.java)
            intent.putExtra(TITLE,title)
            intent.putExtra(URL,url)
            context.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_about_us)
        setSupportActionBar(binding.toolbar.toolbarCustom)


        intent.extras?.also {
            val title = it.getString(TITLE)
            val linkUrl = it.getString(URL)
            binding.toolbar.toolbarTitle.text = title
            binding.webView.loadUrl(linkUrl)


            binding.webView.settings.javaScriptEnabled = true
            binding.webView.webViewClient = object : WebViewClient() {


                override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.webView.visibility = View.GONE
                }


                override fun onPageFinished(view: WebView?, url: String?) {
                    binding.progressBar.visibility = View.GONE
                    binding.webView.visibility = View.VISIBLE
                }

                override fun onLoadResource(view: WebView, url: String) {

                    try {
                        binding.webView.loadUrl(
                            "javascript:(function() { " +
                                    "var head = document.getElementsByTagName('header')[0];"
                                    + "head.parentNode.removeChild(head);" +
                                    "})()"
                        )

                        binding.webView.loadUrl(
                            ("javascript:(function() { " +
                                    "var head = document.getElementsByTagName('footer')[0];"
                                    + "head.parentNode.removeChild(head);" +
                                    "})()")
                        )

                        binding.webView.loadUrl(
                            ("javascript:(function() { " +
                                    "var nav = document.getElementsByTagName('nav')[0];"
                                    + "nav.parentNode.removeChild(nav);" +
                                    "})()")
                        )
                    } catch (e: Exception) {
                        finish()
                        e.printStackTrace()
                    }
                }
            }
        } ?: run {
            finish()
        }

        binding.toolbar.imgButtonBack.visibility = View.VISIBLE
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.toolbarLogo.visibility = View.GONE
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.imgButtonBack.setOnClickListener {
            finish()
        }


    }

}
