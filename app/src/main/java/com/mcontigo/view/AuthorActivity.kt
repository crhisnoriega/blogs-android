package com.mcontigo.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import com.mcontigo.R
import com.mcontigo.adapters.AuthorViewPagerAdapter
import com.mcontigo.androidwpmodule.dao.post.Author
import com.mcontigo.databinding.ActivityAuthorBinding
import com.mcontigo.viewmodel.AuthorViewModel

class AuthorActivity : BaseActivity() {


    private val _binding: ActivityAuthorBinding by lazy {
        DataBindingUtil.setContentView<ActivityAuthorBinding>(this, R.layout.activity_author)
    }
    private val _viewModel by lazy {
        ViewModelProviders.of(this).get(AuthorViewModel::class.java)
    }

    private var authorExtra: Author? = null


    companion object {
        const val AUTHOR_ID = "authorId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        _viewModel.author.observe(this, Observer {
            configureViewPager(it)

            if (it.address?.isNotEmpty() == true) {
                _binding.tvLocalization.text = it.address
                _binding.tvLocalization.visibility = View.VISIBLE
                _binding.imgLocalization.visibility = View.VISIBLE
            }

            it.posts?.also {
                _binding.tvArticlesPublished.text = String.format(
                    getString(R.string.lbl_articles_published_author),
                    it.published.toString()
                )

                _binding.tvArticlesPublished.visibility = View.VISIBLE
                _binding.imgArticlesPublished.visibility = View.VISIBLE
            }


        })

        _binding.imgButtonBack.setOnClickListener {
            finish()
        }

        intent.extras?.also {
            authorExtra = it.getParcelable(AUTHOR_ID)

            _binding.tvAuthorName.text = authorExtra?.name

            _viewModel.getAuthor(authorExtra?.id!!)

            authorExtra?.picture?.also {

                val urlPicture =
                    if (!it.contains(getString(R.string.url))) "${getString(R.string.url)}$it" else it

                Glide.with(this)
                    .load(urlPicture)
                    .error(R.drawable.no_profile)
                    .centerCrop()
                    .into(_binding.imgAuthor)
            }

            authorExtra?.socialProfiles?.facebook?.also { facebookLink ->
                if (facebookLink.isNotEmpty())
                    _binding.imgFacebook.visibility = View.VISIBLE
                _binding.imgFacebook.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(facebookLink)
                    startActivity(intent)
                }

            }

            authorExtra?.socialProfiles?.twitter?.also { twitterLink ->
                if (twitterLink.isNotEmpty())
                    _binding.imgTwitter.visibility = View.VISIBLE
                _binding.imgTwitter.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(twitterLink)
                    startActivity(intent)
                }

            }

            authorExtra?.socialProfiles?.instagram?.also { instagramLink ->
                if (instagramLink.isNotEmpty())
                    _binding.imgInstagram.visibility = View.VISIBLE
                _binding.imgInstagram.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(instagramLink)
                    startActivity(intent)
                }

            }

            authorExtra?.socialProfiles?.linkedin?.also { linkedinLink ->
                if (linkedinLink.isNotEmpty())
                    _binding.imgLinkedIn.visibility = View.VISIBLE
                _binding.imgLinkedIn.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(linkedinLink)
                    startActivity(intent)
                }

            }

            _binding.tvLocalization.visibility = View.GONE
            _binding.tvArticlesPublished.visibility = View.GONE
            _binding.imgLocalization.visibility = View.GONE
            _binding.imgArticlesPublished.visibility = View.GONE


        }

    }

    private fun configureViewPager(author: Author) {
        _binding.tabLayout.setupWithViewPager(_binding.pager)

        _binding.pager.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(_binding.tabLayout)
        )

        val isProfessional = author.type?.contentEquals(Author.TYPE_PROFESSIONAL) ?: false
        val quantityPost = author.posts?.published ?: 0
        val quantityRevisedPosts = author.posts?.reviewed ?: 0

        val listTitle: List<String>
        if (isProfessional) {
            listTitle = if (quantityPost > 0 && quantityRevisedPosts > 0) {
                listOf(
                    getString(R.string.lbl_author_about),
                    getString(R.string.lbl_author_articles),
                    getString(R.string.lbl_author_revised_articles)
                )
            }else if(quantityRevisedPosts == 0L){
                listOf(
                    getString(R.string.lbl_author_about),
                    getString(R.string.lbl_author_articles)
                )
            } else {
                listOf(
                    getString(R.string.lbl_author_about),
                    getString(R.string.lbl_author_revised_articles)
                )
            }
        } else {
            listTitle = if (quantityPost > 0) {
                listOf(
                    getString(R.string.lbl_author_about),
                    getString(R.string.lbl_author_articles)
                )
            } else {
                listOf(
                    getString(R.string.lbl_author_about)
                )
            }
        }


        _binding.pager.adapter = AuthorViewPagerAdapter(
            supportFragmentManager,
            listTitle.count(),
            listTitle,
            author
        )

        _binding.tabLayout.visibility = View.VISIBLE
        _binding.pager.visibility = View.VISIBLE
        _binding.progressBar.visibility = View.GONE

    }
}
