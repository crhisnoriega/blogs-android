package com.mcontigo.view

import android.content.Intent
import android.content.ComponentName 
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mcontigo.R
import com.mcontigo.utils.MenuItensUtil
import com.mcontigo.viewmodel.SettingsViewModel
import com.mcontigo.view.dialogs.DialogChangeLocaleFragment
import android.view.View
import com.mcontigo.databinding.ActivityChangeLocaleBinding
import com.mcontigo.databinding.CustomButtonFrameLayoutLocaleOptionBinding

import androidx.databinding.DataBindingUtil
import com.mcontigo.BuildConfig

import java.util.Locale
import android.app.AlarmManager
import android.app.PendingIntent 
import android.content.Context
import android.content.res.Configuration 
import android.content.pm.PackageManager 

import com.zeugmasolutions.localehelper.LocaleHelper 
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegateImpl
import com.zeugmasolutions.localehelper.LocaleHelperApplicationDelegate

import android.graphics.drawable.Drawable

class ChangeLocaleActivity : BaseActivity() {

     private val localeDelegate = LocaleHelperActivityDelegateImpl()
     
    lateinit var binding: ActivityChangeLocaleBinding

    lateinit var viewModel: SettingsViewModel
    

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_locale)
        setSupportActionBar(binding.toolbar.toolbarCustom)
        
        binding.toolbar.toolbarTitle.text = "App Version"
        binding.toolbar.imgButtonBack.visibility = View.VISIBLE
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.toolbarLogo.visibility = View.GONE
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.imgButtonBack.setOnClickListener {
            finish()
        }

        viewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        
        setButton(binding.buttonEnLocale, getResources().getString(R.string.lang_english), "en", "US")
        setButton(binding.buttonPtLocale, getResources().getString(R.string.lang_portuguese), "pt", "BR")
        setButton(binding.buttonFrLocale, getResources().getString(R.string.lang_french), "fr", "FR")
        setButton(binding.buttonItLocale, getResources().getString(R.string.lang_italian), "it", "IT")
        setButton(binding.buttonDeLocale, getResources().getString(R.string.lang_deustch), "de", "DE")
        setButton(binding.buttonEsLocale, getResources().getString(R.string.lang_spanish), "es", "ES")
        
    }
    
    private fun setButton(button: CustomButtonFrameLayoutLocaleOptionBinding, title: String, locale: String, country: String){
        var text = retrieveString(this, R.string.url, locale, country);
        Log.i("crhisn","text: " + text)
        button.title.setText(text)
        button.name.text = title
        if(LocaleHelper.getLocale(this).language.equals(locale))
            button.imgArrow.visibility = View.VISIBLE
        else
            button.imgArrow.visibility = View.GONE
        
        
        button.root.setOnClickListener {
            DialogChangeLocaleFragment.newInstance(
                retrieveDrawable(this, R.drawable.logo, locale, country),
                "CHANGE VERSION",
                "You are changing the default app version to",
                buttonLeftAction = {
                    // finish()
                    setLabel(locale)
                    localeDelegate.setLocale(this,  Locale(locale, country))
                    restartApp()
                },
                buttonRightAction = {
                    
                }
            ).show(supportFragmentManager, DialogChangeLocaleFragment.DIALOG_TAG)
            
            
        }
    }
    
    private fun retrieveString(context: Context, resourceId: Int, locale: String, country: String) : String{
        var config = Configuration(context.getResources().getConfiguration());
        config.setLocale(Locale(locale, country));
        return context.createConfigurationContext(config).getText(resourceId).toString();
    }
    
     private fun retrieveDrawable(context: Context, resourceId: Int, locale: String, country: String) : Drawable{
        var config = Configuration(context.getResources().getConfiguration());
        config.setLocale(Locale(locale, country));
        return context.createConfigurationContext(config).getResources().getDrawable(resourceId);
    }
    
    
    private fun showConfirm() {
        
    }
    
   
    var lls = arrayOf<String>("en","es") 
    
    private fun setLabel(language: String) {
        for (value in lls) {
            val action = if (value == language) {
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED
            } else {
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED
            }
            Log.i("crhisn", "setting: " + action)
            packageManager.setComponentEnabledSetting(
                ComponentName(BuildConfig.APPLICATION_ID, "${BuildConfig.APPLICATION_ID}.${value}"),
                action, PackageManager.DONT_KILL_APP
            )
        }
    }
    
    private fun restartApp() {
        Handler().postDelayed({
            runOnUiThread {
                val intent = Intent(baseContext, SplashActivity::class.java)
                startActivity(intent)
                Runtime.getRuntime().exit(0);
            }    
        }, 100)
           
    }
}

