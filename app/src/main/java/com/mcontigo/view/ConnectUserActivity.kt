package com.mcontigo.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.ActivityConnectUserBinding
import com.mcontigo.utils.StateUI
import com.mcontigo.view.dialogs.DialogForgotPasswordFragment
import com.mcontigo.view.dialogs.DialogProgressFragment
import com.mcontigo.view.dialogs.DialogRegisterUserFragment
import com.mcontigo.viewmodel.ConnectUserViewModel

val RC_SIGN_IN = 905

class ConnectUserActivity : BaseActivity() {

    private var dialogProgressFragment: DialogProgressFragment? = null
    lateinit var callbackManager: CallbackManager
    lateinit var googleSignIn: GoogleSignInClient

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(ConnectUserViewModel::class.java)
    }

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityConnectUserBinding>(
            this,
            R.layout.activity_connect_user
        )
    }

    companion object {
        fun open(activity: Activity) {
            val intent = Intent(activity, ConnectUserActivity::class.java)
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
        }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.toolbar.imgButtonBack.setOnClickListener {
            finish()
        }
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.imgButtonBack.visibility = View.VISIBLE
        binding.toolbar.toolbarTitle.text = getString(R.string.lbl_connect)
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.toolbarLogo.visibility = View.INVISIBLE


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        binding.btnGoogle.setOnClickListener {
            val signInIntent = googleSignIn.signInIntent;
            startActivityForResult(signInIntent, com.mcontigo.view.dialogs.RC_SIGN_IN)
        }

        binding.btnForgotPassword.setOnClickListener {
            ForgotPasswordActivity.open(this)
        }

        googleSignIn = GoogleSignIn.getClient(this, gso)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        callbackManager = CallbackManager.Factory.create()


        binding.btnFacebook.setReadPermissions("email")

        binding.btnFacebook.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    viewModel.authFacebook(loginResult.accessToken)
                }

                override fun onCancel() {

                }

                override fun onError(exception: FacebookException) {
                    Log.e(DialogRegisterUserFragment.DIALOG_TAG, exception.toString())
                }
            })

        binding.btnConnectWithEmail.setOnClickListener {
            viewModel.signInEmailAndPassword()
        }

        binding.btnRegister.setOnClickListener {
            finish()
            RegisterUserActivity.open(this)
        }



        viewModel.stateUI.observe(this, Observer {

            val singleLiveEventIfNoHandled = it.getContentIfNotHandled()
            dialogProgressFragment?.dismiss()
            when (singleLiveEventIfNoHandled?.stateUI) {

                StateUI.SUCCESS -> {
                    finish()
                }
                StateUI.LOADING -> {
                    dialogProgressFragment =
                        DialogProgressFragment.newInstance(getString(singleLiveEventIfNoHandled.messageResId!!))
                    dialogProgressFragment?.show(
                        supportFragmentManager,
                        DialogProgressFragment.DIALOG_TAG
                    )
                }
                StateUI.FAIL -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.message?.let { it }
                            ?: run { getString(R.string.lbl_user_fail_register) },
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            }
        })


    }
}
