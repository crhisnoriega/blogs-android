package com.mcontigo.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.ActivityForgotPasswordBinding
import com.mcontigo.utils.StateUI
import com.mcontigo.view.dialogs.DialogProgressFragment
import com.mcontigo.viewmodel.ForgotPasswordViewModel

class ForgotPasswordActivity : BaseActivity() {

    private var dialogProgressFragment: DialogProgressFragment? = null

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(ForgotPasswordViewModel::class.java)
    }

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityForgotPasswordBinding>(
            this,
            R.layout.activity_forgot_password
        )
    }

    companion object {
        fun open(context: Context) {
            val intent = Intent(context, ForgotPasswordActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.toolbar.imgButtonBack.setOnClickListener { finish() }
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.imgButtonBack.visibility = View.VISIBLE
        binding.toolbar.toolbarTitle.text = getString(R.string.lbl_forgot_password)
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.toolbarLogo.visibility = View.INVISIBLE

        viewModel.email.observe(this, Observer {
            binding.btnRecovery.isEnabled =
                android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches()
        })

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.stateUI.observe(this, Observer {

            val singleLiveEventIfNoHandled = it.getContentIfNotHandled()
            dialogProgressFragment?.dismiss()
            when (singleLiveEventIfNoHandled?.stateUI) {

                StateUI.SUCCESS -> {
                    binding.groupInput.visibility = View.GONE
                    binding.groupMsgRecovery.visibility = View.VISIBLE
                }
                StateUI.LOADING -> {
                    dialogProgressFragment =
                        DialogProgressFragment.newInstance(getString(singleLiveEventIfNoHandled.messageResId!!))
                    dialogProgressFragment?.show(
                        supportFragmentManager,
                        DialogProgressFragment.DIALOG_TAG
                    )
                }
                StateUI.FAIL -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.message?.let { it }
                            ?: run { getString(R.string.ChangePasswordErrorMessage) },
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            }
        })


    }
}
