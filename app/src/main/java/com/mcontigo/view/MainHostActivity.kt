package com.mcontigo.view

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.ads.consent.*
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.material.navigation.NavigationView
import com.mcontigo.BuildConfig
import com.mcontigo.R
import com.mcontigo.adapters.MenuItemListDrawerAdapter
import com.mcontigo.androidwpmodule.dao.menu.Item
import com.mcontigo.databinding.ActivityMainHostBinding
import com.mcontigo.utils.*
import com.mcontigo.view.dialogs.DialogConnectUserFragment
import com.mcontigo.view.dialogs.DialogRegisterUserFragment
import com.mcontigo.utils.AppRater
import com.mcontigo.utils.CustomToolbarUtil
import com.mcontigo.utils.MenuItensUtil
import com.mcontigo.viewmodel.LoadCagoriesItensMenuViewModel
import java.net.MalformedURLException
import java.net.URL
import com.google.ads.mediation.admob.AdMobAdapter
import com.mcontigo.view.dialogs.CustomSnackbarDialog
import java.util.*


const val SEARCH_SCREEN = 0
const val HOME_SCREEN = 1
const val BOOKMARK_SCREEN = 2
const val SETTINGS_SCREEN = 3
const val TOTAL_PAGES = 4


class MainHostActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {


    lateinit var binding: ActivityMainHostBinding
    lateinit var viewModel: LoadCagoriesItensMenuViewModel

    private var listItens: MutableList<Item>? = null
    private var consentForm: ConsentForm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_host)
        viewModel = ViewModelProviders.of(this).get(LoadCagoriesItensMenuViewModel::class.java)

        BookmarkUtil.shouldUpdateBookmarkList()

		lifecycle.addObserver(viewModel)
        
        var data: Uri? = intent?.data
        if (data != null) {
            PostDetailsActivity.openPostActivity(this, data.toString())
            data = null
        }

        val consentInformation = ConsentInformation.getInstance(this)
        val publisherIds = arrayOf("7120678")

//        ConsentInformation.getInstance(this).addTestDevice("431EE52342B7CA8989E10F907C482892")
//        ConsentInformation.getInstance(this).setDebugGeography(DebugGeography.DEBUG_GEOGRAPHY_EEA)

        val mContext = this
        consentInformation.requestConsentInfoUpdate(
            publisherIds,
            object : ConsentInfoUpdateListener {
                override fun onConsentInfoUpdated(consentStatus: ConsentStatus) {
                    if (ConsentInformation.getInstance(mContext).isRequestLocationInEeaOrUnknown) {
                        val prefs = mContext.getSharedPreferences("consentAds", 0)
                        val editor = prefs.edit()
                        editor.putBoolean("LocationInEeaOrUnknown", true)
                        editor.apply()
                    }

                    when (consentStatus) {
                        ConsentStatus.PERSONALIZED -> initializeAds(false)
                        ConsentStatus.NON_PERSONALIZED -> initializeAds(true)
                        ConsentStatus.UNKNOWN -> runOnUiThread(Runnable { loadConsentForm() })
                    }
                }

                override fun onFailedToUpdateConsentInfo(errorDescription: String) {
                    Log.d("consentInformation", "onFailedToUpdateConsentInfo: $errorDescription")
                    initializeAds(true)
                }
            })

        setupNavigation()

        listItens = mutableListOf(
            Item(
                slug = "today",
                title = getString(R.string.category_toolbar_today_category),
                name = getString(R.string.category_toolbar_today_category)
            )
        )

        viewModel.getItensMenu().observe(this, Observer {
            listItens!!.addAll(it)
            MenuItensUtil.listItens.postValue(listItens)
            Log.d("Reload menu", "reloaded ${it.size}")
            setSideMenu(listItens)
        })


        binding.headerNavigation.imgClose.setOnClickListener {
            binding.drawerLayout.closeDrawers()
        }


        viewModel.stateUI.observe(this, Observer {
            it.getContentIfNotHandled()?.also { responseStateUI ->
                when (responseStateUI.stateUI) {
                    StateUI.SUCCESS -> {
                        responseStateUI.messageResId?.also {
                            var snack = CustomSnackbarDialog.newDialogConfirmEmailSuccess(this)
                            snack.setAction("Close") {
                                snack.dismiss()
                            }
                            snack.show()
                        }
                    }
                }
            }
        })


        UserUtil.user.observe(this, Observer {
            it?.also {
                binding.headerNavigation.groupAuth.visibility = View.GONE
                binding.headerNavigation.groupUserAuthenticated.visibility = View.VISIBLE
                binding.headerNavigation.tvNameUser.text = it.displayName
                binding.headerNavigation.tvEmail.text = it.email

                if (it.verified == false) {
                    CustomSnackbarDialog.newDialogConfirmEmail(this, action = {
                        viewModel.sendAuthenticationEmail()
                    }).show()
                }

                it.picture?.also {

                    if (it.isNotEmpty()) {
                        binding.headerNavigation.frameUserBackground.imgUserAvatar.visibility =
                            View.INVISIBLE
                        binding.headerNavigation.frameUserBackground.frameUserBackground.visibility =
                            View.INVISIBLE
                        binding.headerNavigation.frameUserBackground.userAvarPhoto.visibility =
                            View.VISIBLE
                        binding.headerNavigation.frameUserBackground.userAvarPhoto.setImageBitmap(
                            UserUtil.loadPicture(it, application)
                        )
                    }
                }

                binding.headerNavigation.frameUserBackground.root.visibility = View.VISIBLE

            } ?: run {
                binding.headerNavigation.groupUserAuthenticated.visibility = View.GONE
                binding.headerNavigation.frameUserBackground.root.visibility = View.VISIBLE
                binding.headerNavigation.frameUserBackground.imgUserAvatar.visibility =
                    View.VISIBLE
                binding.headerNavigation.frameUserBackground.frameUserBackground.visibility =
                    View.VISIBLE
                binding.headerNavigation.frameUserBackground.userAvarPhoto.visibility =
                    View.INVISIBLE
                verifyHeaderVisibilityPerFlavor()
            }
        })


        verifyHeaderVisibilityPerFlavor()


        binding.headerNavigation.imgUserSettings.setOnClickListener {
            MenuItensUtil.bottomNavigationSelected.postValue(SETTINGS_SCREEN)
            binding.drawerLayout.closeDrawers()
        }


        binding.headerNavigation.btnRegister.setOnClickListener {
            DialogRegisterUserFragment.newInstance()
                .show(supportFragmentManager, DialogRegisterUserFragment.DIALOG_TAG)
            binding.drawerLayout.closeDrawers()
        }

        binding.headerNavigation.btnConnect.setOnClickListener {
            DialogConnectUserFragment.newInstance()
                .show(supportFragmentManager, DialogConnectUserFragment.DIALOG_TAG)
            binding.drawerLayout.closeDrawers()
        }


        binding.headerNavigation.imgLogo.setOnClickListener {
            MenuItensUtil.itemSelected.postValue(0)
            binding.drawerLayout.closeDrawers()
        }

        binding.footerNavigation.imgLogo.setOnClickListener {
            MenuItensUtil.itemSelected.postValue(0)
            binding.drawerLayout.closeDrawers()
        }

        AppRater.appLaunched(this, supportFragmentManager)
    }

    private fun initializeAds(nonPersonalizedAds: Boolean) {
        MobileAds.initialize(this, getString(R.string.admob_app_id))

        val prefs = this.getSharedPreferences("consentAds", 0)
        val editor = prefs.edit()
        editor.putBoolean("nonPersonalizedAds", nonPersonalizedAds)
        editor.apply()

        val extras = Bundle()
        if (nonPersonalizedAds) {
            extras.putString("npa", "1")
        }

        val adRequest = AdRequest.Builder()
            .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
            .addNetworkExtrasBundle(AdMobAdapter::class.java, extras)
            .build()

        val android_id = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        var data: Uri? = intent?.data
        if (data != null) {
            PostDetailsActivity.openPostActivity(this, data.toString())
        }

    }

    private fun setSideMenu(itensMenu: List<Item>?) {
        val list = mutableListOf<Item>()
        itensMenu?.forEach { item ->
            if (itensMenu?.indexOf(item) > 0) {
                list.add(item)
            }
        }
        val adapter = MenuItemListDrawerAdapter(list) { indexItem: Int ->
            menuNavigationIndexClicked(indexItem)
        }
        binding.rvMenuItens.layoutManager = LinearLayoutManager(this)
        binding.rvMenuItens.adapter = adapter
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeNavigationDrawer()
        } else {
            if (MenuItensUtil.bottomPageSelected != R.id.homeFragment) {
                MenuItensUtil.itemSelected.postValue(MenuItensUtil.itemSelected.value)
            } else
                super.onBackPressed()

        }
    }

    @SuppressLint("WrongConstant")
    private fun setupNavigation() {
        binding.navigationView.setNavigationItemSelectedListener(this@MainHostActivity)
        CustomToolbarUtil.observerOpenDrawer.observe(this, Observer {
            if (it) {
                binding.drawerLayout.openDrawer(Gravity.START)
            }

        })
    }

    fun menuNavigationIndexClicked(index: Int) {
        closeNavigationDrawer()
        MenuItensUtil.itemSelected.postValue(index)
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        menuItem.isChecked = true
        closeNavigationDrawer()
        MenuItensUtil.itemSelected.postValue(menuItem.groupId)
        return true
    }


    fun closeNavigationDrawer() {
        binding.drawerLayout.closeDrawers()
        CustomToolbarUtil.observerOpenDrawer.postValue(false)
    }


    fun verifyHeaderVisibilityPerFlavor() {


        if (BuildConfig.FLAVOR.equals("mcs") || Locale.getDefault().language != "es") {
            binding.headerNavigation.groupUserAuthenticated.visibility = View.GONE
            binding.headerNavigation.groupAuth.visibility = View.GONE
            binding.headerNavigation.imgLogo.visibility = View.VISIBLE
            binding.headerNavigation.frameUserBackground.root.visibility = View.GONE
        } else {
            binding.headerNavigation.groupAuth.visibility = View.VISIBLE
            binding.headerNavigation.imgLogo.visibility = View.GONE
        }
    }

    private fun loadConsentForm() {
        val privacyUrl: URL
        try {
            privacyUrl = URL(getString(R.string.url) + getString(R.string.urlTerm))
            consentForm = ConsentForm.Builder(this, privacyUrl)
                .withListener(object : ConsentFormListener() {

                    override fun onConsentFormLoaded() {
                        // Consent form loaded successfully.
                        Log.d("loadConsentForm", "onConsentFormLoaded")
                        runOnUiThread(Runnable { showConsentForm() })
                    }

                    override fun onConsentFormOpened() {
                        // Consent form was displayed.
                        Log.d("loadConsentForm", "onConsentFormOpened")
                    }

                    override fun onConsentFormClosed(
                        consentStatus: ConsentStatus,
                        userPrefersAdFree: Boolean
                    ) {
                        // Consent form was closed.
                        Log.d("loadConsentForm", "onConsentFormClosed")
                        when (consentStatus) {
                            ConsentStatus.PERSONALIZED -> initializeAds(false)
                            ConsentStatus.NON_PERSONALIZED -> initializeAds(true)
                            else -> initializeAds(true)
                        }
                    }

                    override fun onConsentFormError(errorDescription: String) {
                        // Consent form error.
                        Log.d("loadConsentForm", "onConsentFormError")
                        initializeAds(false)
                    }
                })
                .withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build()
            consentForm?.load()
        } catch (e: MalformedURLException) {

        }
    }

    private fun showConsentForm() {
        consentForm?.show()
    }
}
