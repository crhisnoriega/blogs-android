package com.mcontigo.view

import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BaseTarget
import com.bumptech.glide.request.target.SizeReadyCallback
import com.bumptech.glide.request.transition.Transition
import com.google.ads.mediation.admob.AdMobAdapter
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.google.android.gms.ads.doubleclick.PublisherAdView
import com.google.android.material.appbar.AppBarLayout
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.mcontigo.BuildConfig
import com.mcontigo.R
import com.mcontigo.adapters.HomeGridPostAdapter
import com.mcontigo.adapters.HomeSectionsAdapter
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.dao.post.Author
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.PostDetailsActivityBinding
import com.mcontigo.enums.BookmarkState
import com.mcontigo.interfaces.ItemPostRvInterface
import com.mcontigo.utils.*
import com.mcontigo.view.dialogs.BibliographyBottomModalFragment
import com.mcontigo.view.dialogs.DialogConnectToPay
import com.mcontigo.view.dialogs.DialogInformationFragment
import com.mcontigo.view.dialogs.CustomSnackbarDialog
import com.mcontigo.viewmodel.PostDetailsViewModel
import com.taboola.android.utils.SdkDetailsHelper
import io.mcontigo.DivUtil
import io.mcontigo.richtextlib.spans.RemoteBitmapSpan
import io.mcontigo.richtextlib.spans.UrlBitmapDownloader
import io.mcontigo.richtextlib.ui.RichContentView
import io.mcontigo.richtextlib.v2.RichTextV2
import kotlinx.android.synthetic.main.card_multples_size_banner_express_ad_container.view.*


private const val TIME_DELAY_CREATE_DYNAMIC_VIEWS = 500L

class PostDetailsActivity : BaseActivity(), ItemPostRvInterface {

    private val TAG = this@PostDetailsActivity.javaClass.simpleName
    private lateinit var viewModel: PostDetailsViewModel
    private lateinit var binding: PostDetailsActivityBinding
    private lateinit var adapterItens: HomeSectionsAdapter
    private lateinit var inflater: LayoutInflater
    private lateinit var adRequest: PublisherAdRequest
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var loadingUrl = false
    private var data: String? = null

    companion object {
        const val ON = "on"
        const val OFF = "off"
        const val MAX_LINES_DESCRIPTION_AUTHOR = 10
        const val MARGIN_TOP_TEXT_CONTENT = 64
        const val MARGIN_BOTTOM_TEXT_CONTENT = 64
        const val MARGIN_LEFT_TEXT_CONTENT = 64
        const val MARGIN_RIGHT_TEXT_CONTENT = 64

        const val FAST_CHECKED = "fact_checked"
        const val EVIDENCE_BASED = "evidence_based"

        const val ARTICLE_URL = "urlArticle"
        const val IS_URL = "isUrl"
        const val POST_ID = "postId"
        const val LINK_IMG_THUMB = "linkImgThumbnail"
        const val CATEGORY_NAME = "categoryName"

        fun openPostActivity(context: Context, url: String) {
            val intent = Intent(context, PostDetailsActivity::class.java).apply {
                putExtra(ARTICLE_URL, url)
                putExtra(IS_URL, true)
                putExtra(LINK_IMG_THUMB, " ")
                putExtra(CATEGORY_NAME, " ")
                addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            context.startActivity(intent)
        }


        fun openPostActivity(
            context: Context,
            postID: Int,
            linkImgThumbnail: String? = "",
            categoryName: String = ""
        ) {
            val intent = Intent(context, PostDetailsActivity::class.java).apply {
                putExtra(POST_ID, postID)
                putExtra(LINK_IMG_THUMB, linkImgThumbnail)
                putExtra(CATEGORY_NAME, TextUtils.htmlEncode(categoryName))
                putExtra(IS_URL, false)
            }
            context.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.post_details_activity)
        viewModel = ViewModelProviders.of(this).get(PostDetailsViewModel::class.java)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        binding.nestedScrollView.visibility = View.GONE

        inflater = LayoutInflater.from(this)

        Log.d("data", data.toString())

        intent.extras?.also {

            data = it.getString(ARTICLE_URL)

            if (data != null) {
                loadingUrl = true
                viewModel.url = data.toString()
            } else {
                viewModel.url = it.getString(ARTICLE_URL)
                loadingUrl = it.getBoolean(IS_URL)
                viewModel.postId = PostDetailsActivityArgs.fromBundle(it).postId
                viewModel.thumbnailLink = PostDetailsActivityArgs.fromBundle(it).linkImgThumbnail
                viewModel.categoryName = PostDetailsActivityArgs.fromBundle(it).categoryName
            }

            PostsDetailInstanceUtil.listPostDetailsOpenend.add(this@PostDetailsActivity)

        } ?: run {
            finish()
        }


        binding.lifecycleOwner = this

        binding.viewModel = viewModel

        setupNavigation()

        if (!loadingUrl)
            viewModel.postId?.also { viewModel.getPostDetails(it) } ?: run {
                finish()
            } else {
            viewModel.url?.also { viewModel.getPostDetails(it) } ?: run { finish() }
        }

        viewModel.getRecomendedPost().observe(this, Observer {
            binding.rvItensPost.recycledViewPool.clear()
            adapterItens = HomeSectionsAdapter(
                it,
                this@PostDetailsActivity,
                false,
                HomeGridPostAdapter.POST_STYLE_WITHOUT_SPOTLIGHT_LEFT
            )
            binding.rvItensPost.adapter = adapterItens
            binding.rvItensPost.visibility = View.VISIBLE
            binding.rvItensPost.itemAnimator = null
        })

        val prefs = this.getSharedPreferences("consentAds", 0)
        val nonPersonalizedAds = prefs.getBoolean("nonPersonalizedAds", false)

        val adRequestExtra = Bundle()
        if(nonPersonalizedAds){
            adRequestExtra.putString("npa", "1")
        }

        viewModel.getPostLiveData().observe(this, Observer { post ->

            adRequest = PublisherAdRequest.Builder().setContentUrl(post.link)
                .addNetworkExtrasBundle(AdMobAdapter::class.java, adRequestExtra)
                .addCustomTargeting("postID", post.id.toString())
                .addCustomTargeting("authorId", post.author?.id.toString())
                .addCustomTargeting("author", post.author?.slug)
                .addCustomTargeting("tags", post.tags?.map { it?.slug.toString() })
                .addCustomTargeting("url", post.slug)
                .build()


            viewModel.categoryName?.let {
                binding.tvCategory.text = it
            }

            loadImg()

            val bundle = Bundle()
            bundle.putString("id", "${post.id}")
            bundle.putString("link", "${post.link}")
            bundle.putString("title", "${post.title}")
            firebaseAnalytics.logEvent("open_article", bundle)

            binding.tvAbout.text = post.title?.let { HtmlUtil.fromHtml(it) }
            binding.tvAuthorPost.text = post.author?.name

            if (post.author?.name.isNullOrBlank() && post.options?.author != ON) binding.icUserPublish.visibility =
                View.INVISIBLE


            if (post.options?.isSponsored == true) {
                binding.chipSponsored.visibility = View.VISIBLE
                binding.chipSponsored.setOnClickListener {
                    DialogInformationFragment.newInstance(
                        getString(R.string.lbl_title_dialog_sponsor),
                        String.format(
                            getString(R.string.lbl_msg_sponsor),
                            getString(R.string.app_name),
                            post.options?.sponsor ?: ""
                        ),
                        R.drawable.ic_sponsor
                    ).show(supportFragmentManager, DialogInformationFragment.DIALOG_TAG)
                }
            }

            when (post.options?.verificationStatus) {
                FAST_CHECKED -> {


                    val states = arrayOf(
                        intArrayOf(-android.R.attr.state_enabled), // disabled
                        intArrayOf(-android.R.attr.state_enabled), // disabled
                        intArrayOf(-android.R.attr.state_checked), // unchecked
                        intArrayOf(-android.R.attr.state_pressed)  // unpressed
                    )


                    val backgroundColorGreen =
                        ContextCompat.getColor(this, R.color.chip_background_fast_checked)

                    val colors = intArrayOf(
                        backgroundColorGreen,
                        backgroundColorGreen,
                        backgroundColorGreen,
                        backgroundColorGreen
                    )

                    val colorsStateList = ColorStateList(states, colors)
                    binding.chipFastChecked.chipBackgroundColor = colorsStateList


                    binding.chipFastChecked.text = getString(R.string.lbl_article_fast_checked)
                    binding.chipFastChecked.visibility = View.VISIBLE
                    binding.chipFastChecked.setOnClickListener {
                        DialogInformationFragment.newInstance(
                            getString(R.string.lbl_article_fast_checked),
                            getString(R.string.lbl_msg_fast_checked),
                            R.drawable.ic_fact_fast_verified
                        ).show(supportFragmentManager, DialogInformationFragment.DIALOG_TAG)
                    }
                }
                EVIDENCE_BASED -> {
                    binding.chipFastChecked.text = getString(R.string.lbl_article_evidence_based)
                    binding.chipFastChecked.visibility = View.VISIBLE
                    binding.chipFastChecked.setOnClickListener {
                        DialogInformationFragment.newInstance(
                            getString(R.string.lbl_article_evidence_based),
                            getString(R.string.lbl_msg_evidence_based),
                            R.drawable.ic_evidence_based
                        ).show(supportFragmentManager, DialogInformationFragment.DIALOG_TAG)
                    }
                }
                else -> {
                    binding.chipFastChecked.visibility = View.GONE
                }
            }



            post.author?.also { author ->
                configureAuthor(author)
                binding.authorDetails.root.visibility = View.VISIBLE
                if (author.type == Author.TYPE_PROFESSIONAL) {
                    binding.tvAuthorPost.visibility = View.GONE
                    binding.icUserPublish.visibility = View.GONE
                    binding.viewCertificationPost.visibility = View.VISIBLE
                    binding.tvCheckedInfo.text = HtmlUtil.fromHtml(
                        String.format(
                            getString(R.string.label_author_is_professional),
                            author.profession,
                            "<font color=\"black\"><b>${author.name}</b></font>"
                        )
                    )

                    binding.viewCertificationPost.setOnClickListener {
                        val intent = Intent(this, AuthorActivity::class.java)
                        intent.putExtra(AuthorActivity.AUTHOR_ID, author)
                        startActivity(intent)
                    }
                } else {
                    post?.reviewedBy?.let { professionalAuthor ->
                        binding.viewCertificationPost.visibility = View.VISIBLE
                        binding.tvCheckedInfo.text = HtmlUtil.fromHtml(
                            String.format(
                                getString(R.string.label_signed_only_name_and_date),
                                professionalAuthor.profession,
                                "<font color=\"black\"><b>${professionalAuthor.name}</b></font>",
                                post.reviewed?.let { reviewed ->
                                    "<font color=\"black\">${DateFormatUtil.formatDateToString(
                                        reviewed
                                    )}</font>"
                                }
                            )
                        )
                        binding.viewCertificationPost.setOnClickListener {
                            val intent = Intent(this, AuthorActivity::class.java)
                            intent.putExtra(AuthorActivity.AUTHOR_ID, professionalAuthor)
                            startActivity(intent)
                        }


                    } ?: run {
                        binding.viewCertificationPost.visibility = View.GONE
                    }
                }

            } ?: run {
                binding.authorDetails.root.visibility = View.GONE
            }



            post?.link?.let { link ->
                binding.btnShareAppBar.visibility = View.VISIBLE
                binding.btnShareAppBar.setOnClickListener {
                    SharePostUtil.sharePost(this@PostDetailsActivity, link)
                }
            }

            post?.bibliography?.let { bibliography ->
                binding.btnBibliography.visibility = View.VISIBLE
                binding.btnBibliography.setOnClickListener {
                    BibliographyBottomModalFragment.newInstance(bibliography)
                        .show(supportFragmentManager, BibliographyBottomModalFragment.DIALOG_TAG)
                }
            }




            post.headline?.let {
                binding.frameHeadline.visibility = View.VISIBLE
                binding.tvHeadline.text = HtmlUtil.fromHtml(it)
            } ?: run {
                binding.frameHeadline.visibility = View.GONE
            }


            val dateString = post.modified

            binding.tvDatePublish.text =
                dateString?.let { DateFormatUtil.formatDateToString(it) }


            DivUtil.postId.observe(this, Observer {
                viewModel.getPostDiv(it)
            })

            DivUtil.postData.observe(this, Observer {
                val featuredPost = Gson().fromJson(it, Post::class.java)
                viewModel.postDiv.postValue(featuredPost)
            })


            post.content?.also { rendered ->


                val renderedWithoutSpaces = rendered.replace("<figure class=\"\\&quot;content-thumb\\&quot;\">", "").replace("</figure>", "").replace("<p>\\n</p>", "")

                RichTextV2.textFromHtml(applicationContext, renderedWithoutSpaces)
                var countLines = 0
                var firstAdd = true

                loadAdsAfterBeforeRecomendedPosts(post.options?.ads ?: ON)

                renderedWithoutSpaces.substringBefore("mc-featured-container card recommended-post").lines()
                    .forEach {
                        if (countLines == 10 || (firstAdd && countLines == 0)) {
                            if (post.options?.ads == ON) {
                                firstAdd = false
                                val view =
                                    inflater.inflate(
                                        R.layout.card_multples_size_banner_express_ad_container,
                                        null
                                    )
                                val params = LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT
                                )




                                params.topMargin = MARGIN_TOP_TEXT_CONTENT
                                params.bottomMargin = MARGIN_BOTTOM_TEXT_CONTENT

                                view.layoutParams = params

                                val adView =
                                    if (BuildConfig.DEBUG) view.publisherAdViewDebug else view.publisherAdView
                                val mPublisherAdView: PublisherAdView = adView
                                adView.visibility = View.VISIBLE
                                mPublisherAdView.loadAd(adRequest)
                                binding.linearContent.addView(view)
                            }

                            countLines = 0

                        }
                        val richContentView = RichContentView(this, R.color.colorSecondary)
                        val textParam = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        )

                        if ("<img" !in it) {
                            textParam.marginStart = MARGIN_LEFT_TEXT_CONTENT
                            textParam.marginEnd = MARGIN_RIGHT_TEXT_CONTENT
                        }

                        richContentView.layoutParams = textParam

                        richContentView.setText(
                            RichTextV2.textFromHtml(
                                applicationContext,
                                it
                            )
                        )

                        richContentView.setFontFamily("roboto")
                        richContentView.setUrlBitmapDownloader(object : UrlBitmapDownloader {
                            override fun downloadImage(
                                urlBitmapSpan: RemoteBitmapSpan,
                                image: Uri
                            ) {
                                Glide.with(this@PostDetailsActivity)
                                    .load(image)
                                    .into(object : BaseTarget<Drawable>() {
                                        override fun onResourceReady(
                                            resource: Drawable,
                                            transition: Transition<in Drawable>?
                                        ) {
                                            urlBitmapSpan.updateBitmap(
                                                this@PostDetailsActivity,
                                                resource
                                            )
                                        }

                                        override fun getSize(@NonNull cb: SizeReadyCallback) {

                                            cb.onSizeReady(
                                                urlBitmapSpan.possibleSize.width(),
                                                urlBitmapSpan.possibleSize.height()
                                            )
                                        }

                                        override fun removeCallback(@NonNull cb: SizeReadyCallback) {

                                        }
                                    })

                            }

                        })

                        binding.linearContent.addView(richContentView)
                        countLines++
                    }



                Handler().postDelayed({
                    binding.nestedScrollView.visibility = View.VISIBLE
                    binding.nestedScrollView.scrollTo(0, 0)
                    binding.progressBar.visibility = View.GONE
                }, TIME_DELAY_CREATE_DYNAMIC_VIEWS)

            }
            setupBookmark()
        })


        binding.appBarLayoutPostDetails.addOnOffsetChangedListener(object :
            AppBarStateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout, state: State) {
                when (state) {
                    State.COLLAPSED -> {
                        binding.floatingActionButton.show()
                    }
                    State.EXPANDED -> {
                        binding.floatingActionButton.hide()
                    }
                }
            }

        })


        binding.tvPostContentRich.setUrlBitmapDownloader { urlBitmapSpan, image ->

            try {
                Glide.with(this@PostDetailsActivity)
                    .load(image)
                    .error(R.drawable.ic_broken_image_black_24dp)
                    .into(object : BaseTarget<Drawable>() {
                        override fun onResourceReady(
                            resource: Drawable,
                            transition: Transition<in Drawable>?
                        ) {
                            urlBitmapSpan.updateBitmap(this@PostDetailsActivity, resource)
                        }

                        override fun getSize(@NonNull cb: SizeReadyCallback) {
                            cb.onSizeReady(
                                urlBitmapSpan.possibleSize.width(),
                                urlBitmapSpan.possibleSize.height()
                            )
                        }

                        override fun removeCallback(@NonNull cb: SizeReadyCallback) {

                        }
                    })
            } catch (e: Exception) {
                urlBitmapSpan.updateBitmap(
                    this@PostDetailsActivity,
                    ContextCompat.getDrawable(
                        this@PostDetailsActivity,
                        R.drawable.ic_broken_image_black_24dp
                    )
                )
            }
        }

        viewModel.getPostDiv().observe(this, Observer { itemPost ->

            if (itemPost.categories?.isNotEmpty() == true)
                binding.tvCategoryDiv.text = itemPost.categories?.get(0)?.name

            binding.tvAboutDiv.text = itemPost.title?.let { HtmlUtil.fromHtml(it) }
            binding.tvDescriptionDiv.text = itemPost.headline?.let { HtmlUtil.fromHtml(it) }

            itemPost.featuredMedia?.large.let {
                Glide.with(binding.root)
                    .load(it)
                    .centerCrop()
                    .into(binding.ivArticle)
            }

            itemPost.link?.also { link ->
                binding.btnShareDiv.setOnClickListener {
                    SharePostUtil.sharePost(this@PostDetailsActivity, link)
                }
            } ?: run {
                binding.btnShareDiv.visibility = View.INVISIBLE
            }

            itemPost.id?.also { id ->
                binding.postDivItem.setOnClickListener {
                    val intent = Intent(this, PostDetailsActivity::class.java)
                    intent.putExtra("postId", id)
                    intent.putExtra("linkImgThumbnail", itemPost.featuredMedia?.large)

                    intent.putExtra(
                        "categoryName",
                        if (itemPost.categories?.isNotEmpty() == true)
                            TextUtils.htmlEncode(itemPost.categories?.get(0)?.name)
                        else ""
                    )
                    startActivity(intent)
                    finish()
                }


            }
            binding.postDivItem.visibility = View.VISIBLE
        })


    }

    private fun setupBookmark() {
        if (BookmarkUtil.isEnabled()) {
            //viewModel.fetchBookmark()
            val mContext = this
            UserUtil.user.observe(this, Observer {
                viewModel.fetchBookmark()
            })
            viewModel.getBookmarkState().observe(this, Observer {
                when (it) {
                    BookmarkState.LOADING -> binding.imgBtnBookmark.visibility = View.GONE
                    BookmarkState.ERROR -> {
                        CustomSnackbarDialog.newDialogBookmarkError(mContext).show()
                        binding.imgBtnBookmark.visibility = View.GONE
                    }
                    BookmarkState.ON -> {
                        binding.imgBtnBookmark.visibility = View.VISIBLE
                        binding.imgBtnBookmark.setImageDrawable(getDrawable(R.drawable.ic_bookmark))
                        binding.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                            viewModel.setBookmark(false, mContext)
                        })
                    }
                    BookmarkState.OFF -> {
                        binding.imgBtnBookmark.visibility = View.VISIBLE
                        binding.imgBtnBookmark.setImageDrawable(getDrawable(R.drawable.ic_bookmark_off))
                        binding.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                            viewModel.setBookmark(true, mContext)
                        })
                    }
                    BookmarkState.SIGNOUT -> {
                        binding.imgBtnBookmark.visibility = View.VISIBLE
                        binding.imgBtnBookmark.setImageDrawable(getDrawable(R.drawable.ic_bookmark_off))
                        binding.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                            DialogConnectToPay.newInstance()
                                .show(this.supportFragmentManager, DialogConnectToPay.DIALOG_TAG)
                        })
                    }
                }
            })
        } else {
            binding.imgBtnBookmark.visibility = View.GONE
        }
    }

    private fun loadAdsAfterBeforeRecomendedPosts(postsOn: String = ON) {
        if (postsOn == ON) {
            inflateAdList(binding.adBeforeItensPost)
            inflateAdList(binding.adAfterItensPost)

            binding.taboolaView.visibility = View.VISIBLE

            // Activate `useOnlineTemplate` for improved loading time
            val optionalExtraProperties = HashMap<String, String>();
            optionalExtraProperties.put("useOnlineTemplate", "true");
            binding.taboolaView.setExtraProperties(optionalExtraProperties)


            binding.taboolaView.getLayoutParams().height =
                SdkDetailsHelper.getDisplayHeight(binding.root.context) * 6


            //Set the scrolling behavior
            binding.taboolaView.setInterceptScroll(true) // See step 3 below


            binding.taboolaView.fetchContent()
        }
    }


    private fun inflateAdList(linearLayout: LinearLayout) {

        val view =
            inflater.inflate(
                R.layout.card_multples_size_banner_express_ad_container,
                null
            )
        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )


        params.topMargin = MARGIN_TOP_TEXT_CONTENT
        params.bottomMargin = MARGIN_BOTTOM_TEXT_CONTENT

        view.layoutParams = params

        val adView =
            if (BuildConfig.DEBUG) view.publisherAdViewDebug else view.publisherAdView
        val mPublisherAdView: PublisherAdView = adView
        adView.visibility = View.VISIBLE
        mPublisherAdView.loadAd(adRequest)


        linearLayout.addView(view)

    }

    private fun configureAuthor(author: Author) {
        binding.authorDetails.tvAuthorName.text = author.name



        binding.authorDetails.tvAuthorDescription.text =
            HtmlUtil.fromHtml(author.description ?: "").take(300)
        binding.authorDetails.tvAuthorDescription.append(" [...]")


        author.picture?.also {


            val urlPicture =
                if (!it.contains(getString(R.string.url))) "${getString(R.string.url)}$it" else it

            Glide.with(binding.root)
                .load(urlPicture)
                .centerCrop()
                .error(R.drawable.no_profile)
                .into(binding.authorDetails.imgAuthor)
        }

        author.socialProfiles?.facebook?.also { facebookLink ->
            if (facebookLink.isNotEmpty())
                binding.authorDetails.imgFacebook.visibility = View.VISIBLE
            binding.authorDetails.imgFacebook.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(facebookLink)
                startActivity(intent)
            }

        }

        author.socialProfiles?.twitter?.also { twitterLink ->
            if (twitterLink.isNotEmpty())
                binding.authorDetails.imgTwitter.visibility = View.VISIBLE
            binding.authorDetails.imgTwitter.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(twitterLink)
                startActivity(intent)
            }

        }

        author.socialProfiles?.instagram?.also { instagramLink ->
            if (instagramLink.isNotEmpty())
                binding.authorDetails.imgInstagram.visibility = View.VISIBLE
            binding.authorDetails.imgInstagram.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(instagramLink)
                startActivity(intent)
            }

        }

        author.socialProfiles?.linkedin?.also { linkedinLink ->
            if (linkedinLink.isNotEmpty())
                binding.authorDetails.imgLinkedIn.visibility = View.VISIBLE
            binding.authorDetails.imgLinkedIn.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(linkedinLink)
                startActivity(intent)
            }

        }


        binding.authorDetails.btnReadMore.setOnClickListener {
            val intent = Intent(this, AuthorActivity::class.java)
            intent.putExtra(AuthorActivity.AUTHOR_ID, author)
            startActivity(intent)
        }

        binding.authorDetails.imgAuthor.setOnClickListener {
            val intent = Intent(this, AuthorActivity::class.java)
            intent.putExtra(AuthorActivity.AUTHOR_ID, author)
            startActivity(intent)
        }

        binding.tvAuthorPost.setOnClickListener {
            val intent = Intent(this, AuthorActivity::class.java)
            intent.putExtra(AuthorActivity.AUTHOR_ID, author)
            startActivity(intent)
        }


    }

    private fun setupNavigation() {
        setSupportActionBar(binding.toolbarPostDetails)

        binding.imgButtonBack.visibility = View.VISIBLE
        binding.toolbarLogo.visibility = View.VISIBLE

        binding.toolbarLogo.setOnClickListener {
            MenuItensUtil.itemSelected.postValue(0)
            MenuItensUtil.bottomPageSelected = HOME_SCREEN
            PostsDetailInstanceUtil.listPostDetailsOpenend.forEach {
                it.finish()
            }
            PostsDetailInstanceUtil.listPostDetailsOpenend.clear()
        }

        binding.imgButtonBack.setOnClickListener {
            val postDetailToRemove =
                PostsDetailInstanceUtil.listPostDetailsOpenend.first { it == this@PostDetailsActivity }
            PostsDetailInstanceUtil.listPostDetailsOpenend.remove(postDetailToRemove)
            finish()
        }


        binding.floatingActionButton.setOnClickListener {
            val postDetailToRemove =
                PostsDetailInstanceUtil.listPostDetailsOpenend.first { it == this@PostDetailsActivity }
            PostsDetailInstanceUtil.listPostDetailsOpenend.remove(postDetailToRemove)
            finish()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }


    private fun loadImg() {
        viewModel.thumbnailLink?.let {
            Glide.with(binding.root)
                .load(it)
                .centerCrop()
                .into(binding.ivPostImg)
        } ?: run {
            binding.ivPostImg.visibility = View.GONE
        }
    }

    override fun onItemClick(itemPost: PostContentHome) {

        Log.d(TAG, itemPost.toString())

        itemPost.id?.also {
            openPostActivity(
                this,
                it,
                itemPost.featuredMedia?.large,
                TextUtils.htmlEncode(itemPost.category?.get(0)?.name)
            )
        }

    }


}
