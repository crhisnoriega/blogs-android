package com.mcontigo.view

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.ActivityRegisterUserBinding
import com.mcontigo.utils.StateUI
import com.mcontigo.view.dialogs.DialogProgressFragment
import com.mcontigo.view.dialogs.DialogRegisterUserFragment
import com.mcontigo.viewmodel.RegisterUserViewModel

class RegisterUserActivity : BaseActivity() {

    private val TAG = this@RegisterUserActivity.javaClass.simpleName
    val RC_SIGN_IN = 905

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(RegisterUserViewModel::class.java)
    }

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRegisterUserBinding>(
            this,
            R.layout.activity_register_user
        )
    }


    companion object {
        fun open(activity: Activity) {
            val intent = Intent(activity, RegisterUserActivity::class.java)
            activity.startActivity(intent)
            activity.overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
        }
    }

    lateinit var callbackManager: CallbackManager
    lateinit var googleSignIn: GoogleSignInClient
    private var dialogProgressFragment: DialogProgressFragment? = null

    override fun overridePendingTransition(enterAnim: Int, exitAnim: Int) {
        super.overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.toolbar.imgButtonBack.setOnClickListener { finish() }
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.imgButtonBack.visibility = View.VISIBLE
        binding.toolbar.toolbarTitle.text = getString(R.string.lbl_register)
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.toolbarLogo.visibility = View.INVISIBLE


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        binding.btnGoogle.setOnClickListener {
            val signInIntent = googleSignIn.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        googleSignIn = GoogleSignIn.getClient(this, gso)


        binding.toolbar.imgButtonBack.setOnClickListener {
            finish()
        }

        binding.btnHaveAccount.setOnClickListener {
            finish()
            ConnectUserActivity.open(this)
        }




        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        callbackManager = CallbackManager.Factory.create()

        binding.btnFacebook.setReadPermissions("email")

        binding.btnFacebook.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    viewModel.authFacebook(loginResult.accessToken)
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    Log.e(DialogRegisterUserFragment.DIALOG_TAG, exception.toString())
                    val snackbar = Snackbar.make(
                        binding.root,
                        getString(R.string.lbl_user_fail_register),
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            this@RegisterUserActivity,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            })

        viewModel.stateUI.observe(this, Observer {

            val singleLiveEventIfNoHandled = it.getContentIfNotHandled()

            dialogProgressFragment?.dismiss()

            when (singleLiveEventIfNoHandled?.stateUI) {

                StateUI.SUCCESS -> {
                    finish()
                }
                StateUI.LOADING -> {
                    dialogProgressFragment =
                        DialogProgressFragment.newInstance(getString(singleLiveEventIfNoHandled.messageResId!!))
                    dialogProgressFragment?.show(
                        supportFragmentManager,
                        DialogProgressFragment.DIALOG_TAG
                    )
                }
                StateUI.FAIL -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.messageResId?.let { getString(it) }
                            ?: run { getString(R.string.lbl_user_fail_register) },
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            }
        })

    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_up, R.anim.slide_down)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }


        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            account?.let {
                viewModel.googleAuth(it)
            }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e(TAG, e.toString())
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
        }

    }
}
