package com.mcontigo.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.ActivitySetNewPasswordBinding
import com.mcontigo.utils.StateUI
import com.mcontigo.view.dialogs.DialogProgressFragment
import com.mcontigo.viewmodel.ChangePasswordViewModel

class SetNewPasswordActivity : BaseActivity() {

    private var dialogProgressFragment: DialogProgressFragment? = null

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(ChangePasswordViewModel::class.java)
    }

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivitySetNewPasswordBinding>(
            this,
            R.layout.activity_set_new_password
        )
    }

    companion object {
        fun open(context: Context) {
            val intent = Intent(context, SetNewPasswordActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.viewModel = viewModel
        binding.lifecycleOwner = this


        binding.toolbar.imgButtonBack.setOnClickListener { finish() }
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.imgButtonBack.visibility = View.VISIBLE
        binding.toolbar.toolbarTitle.text = getString(R.string.lbl_set_new_password)
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.toolbarLogo.visibility = View.INVISIBLE


        viewModel.stateUI.observe(this, Observer {

            val singleLiveEventIfNoHandled = it.getContentIfNotHandled()
            dialogProgressFragment?.dismiss()
            when (singleLiveEventIfNoHandled?.stateUI) {

                StateUI.SUCCESS -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.messageResId!!,
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                    Handler().postDelayed({
                        snackbar.dismiss()
                        finish()
                    }, 1000)
                }
                StateUI.LOADING -> {
                    dialogProgressFragment =
                        DialogProgressFragment.newInstance(getString(singleLiveEventIfNoHandled.messageResId!!))
                    dialogProgressFragment?.show(
                        supportFragmentManager,
                        DialogProgressFragment.DIALOG_TAG
                    )
                }
                StateUI.FAIL -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.messageResId?.let { getString(it) }
                            ?: run { getString(R.string.lbl_user_fail_register) },
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            }
        })

    }
}
