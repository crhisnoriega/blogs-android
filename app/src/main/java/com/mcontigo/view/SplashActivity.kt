package com.mcontigo.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mcontigo.R
import com.mcontigo.utils.MenuItensUtil
import com.mcontigo.viewmodel.LoadCagoriesItensMenuViewModel


import android.content.res.Configuration 
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegateImpl
import android.content.Context

class SplashActivity : BaseActivity() {

    lateinit var viewModel: LoadCagoriesItensMenuViewModel
    private val localeDelegate = LocaleHelperActivityDelegateImpl()
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        localeDelegate.onCreate(this)
         
        viewModel = ViewModelProviders.of(this).get(LoadCagoriesItensMenuViewModel::class.java)
        lifecycle.addObserver(viewModel)

        viewModel.getItensMenu().observe(this, Observer {
            MenuItensUtil.itemSelected.value = 0
            Handler().postDelayed({
                val intent = Intent(this, MainHostActivity::class.java)
                startActivity(intent)
                finish()
            },2000)
        })
    }
}
