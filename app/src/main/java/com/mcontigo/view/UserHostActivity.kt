package com.mcontigo.mcs.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import com.mcontigo.R
import com.mcontigo.databinding.ActivityUserHostBinding


class UserHostActivity : AppCompatActivity() {

    private lateinit var _binding: ActivityUserHostBinding
    private lateinit var _navController : NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        _binding = DataBindingUtil.setContentView(this, R.layout.activity_user_host)

        val openRegister = intent.extras?.getBoolean(TYPE_OPEN)

        Navigation.findNavController(this, R.id.nav_host_fragment).navigate(
            if (openRegister == true) R.id.registerUserFragment else R.id.connectUserFragment
        )


        _navController = findNavController(R.id.nav_host_fragment)


        _binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        _binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        _binding.toolbar.imgButtonBack.visibility = View.VISIBLE

        _binding.toolbar.imgButtonBack.setOnClickListener {
            finish()
        }
        _navController.addOnDestinationChangedListener { controller, destination, arguments ->
            _binding.toolbar.toolbarTitle.text = destination.label
        }

    }


    companion object {
        val TYPE_OPEN = "type"
        fun open(context: Context, openRegister: Boolean = false) {
            val intent = Intent(context, UserHostActivity::class.java)
            intent.putExtra(TYPE_OPEN, openRegister)
            context.startActivity(intent)
        }
    }
}
