package com.mcontigo.view

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.ActivityUserProfileBinding
import com.mcontigo.utils.StateUI
import com.mcontigo.view.dialogs.DialogChangePasswordFragment
import com.mcontigo.view.dialogs.DialogProgressFragment
import com.mcontigo.view.dialogs.DialogQuestionFragment
import com.mcontigo.viewmodel.UserProfileViewModel
import java.io.ByteArrayOutputStream
import com.mcontigo.view.dialogs.CustomSnackbarDialog
import androidx.coordinatorlayout.widget.CoordinatorLayout

class UserProfileActivity : BaseActivity() {

    private val TAG = this@UserProfileActivity.javaClass.simpleName

    private val GALLERY_REQUEST_CODE = 1
    private val PERMISSION_READ_STORAGE = 2

    private var dialogProgressFragment: DialogProgressFragment? = null

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityUserProfileBinding>(
            this,
            R.layout.activity_user_profile
        )
    }
    private val viewModel by lazy {
        ViewModelProviders.of(this).get(UserProfileViewModel::class.java)
    }

    companion object {
        fun open(context: Context) {
            val intent = Intent(context, UserProfileActivity::class.java)
            context.startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.toolbar.imgButtonBack.setOnClickListener { finish() }
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.imgButtonBack.visibility = View.VISIBLE
        binding.toolbar.toolbarTitle.text = getString(R.string.lbl_user_profile)
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.toolbarLogo.visibility = View.INVISIBLE

        binding.btnChangePassword.setOnClickListener {
            SetNewPasswordActivity.open(this)
        }


        binding.btnChangeProfilePicture.setOnClickListener {
            if (verifyPermissions())
                openGalery()
        }

        viewModel.imgAvatar.observe(this, Observer {
            val bitmap = it
            binding.userAvatar.imgUserAvatar.visibility = View.INVISIBLE
            binding.userAvatar.frameUserBackground.visibility = View.INVISIBLE
            binding.userAvatar.userAvarPhoto.visibility = View.VISIBLE
            binding.userAvatar.userAvarPhoto.setImageBitmap(bitmap)
            binding.userAvatar.userAvarPhoto.buildDrawingCache(true)
        })


        binding.btnLogout.setOnClickListener {
            DialogQuestionFragment.newInstance(
                getString(R.string.lbl_signOff),
                getString(R.string.lbl_msg_question_logout),
                buttonLeftAction = {
                    viewModel.logout()
                },
                buttonRightAction = {

                }
            ).show(supportFragmentManager, DialogQuestionFragment.DIALOG_TAG)
        }

        viewModel.stateUI.observe(this, Observer {
            it.getContentIfNotHandled()?.also { responseStatusUI ->
                dialogProgressFragment?.dismiss()
                when (responseStatusUI.stateUI) {
                    StateUI.LOADING -> {
                        dialogProgressFragment =
                            DialogProgressFragment.newInstance(getString(responseStatusUI.messageResId!!))
                        dialogProgressFragment?.show(
                            supportFragmentManager,
                            DialogProgressFragment.DIALOG_TAG
                        )
                    }
                    StateUI.DONE -> {
                        
                        val snackbar = Snackbar.make(
                            binding.root,
                            "Account validated",
                            Snackbar.LENGTH_LONG
                        )
                        
                        val view = this.layoutInflater.inflate(R.layout.snackbar_email_success, null)
                        val layout = snackbar.view as Snackbar.SnackbarLayout
                        
                        snackbar.view.setBackgroundColor(
                            ContextCompat.getColor(
                                this,
                                R.color.colorSecondary
                            )
                        )
                        
                        val layoutParams = layout.layoutParams
                        layout.addView(view)
                        
                        snackbar.show()
                        // finish()
                    }
                }
            }
        })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_READ_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openGalery()
            }
        }
    }

    private fun openGalery() {
        val intent =
            Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        val mimeTypes =
            arrayOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, GALLERY_REQUEST_CODE)
    }

    private fun verifyPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            ) {

                val snackbar = Snackbar.make(
                    binding.root,
                    getString(R.string.lbl_permission_storage),
                    Snackbar.LENGTH_LONG
                )
                snackbar.view.setBackgroundColor(
                    ContextCompat.getColor(
                        this,
                        R.color.colorWhite
                    )
                )
                snackbar.show()


            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    PERMISSION_READ_STORAGE
                )
            }
        } else {
            return true
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                GALLERY_REQUEST_CODE -> {
                    data?.data?.also {
                        manageImageFromUri(it)?.also {
                            viewModel.imgAvatar.postValue(it)
                        }
                    }
                }
            }
        }
    }


    private fun manageImageFromUri(imageUri: Uri): Bitmap? {
        var bitmap: Bitmap? = null
        try {
            bitmap = if (Build.VERSION.SDK_INT < 28) {
                MediaStore.Images.Media.getBitmap(
                    this.contentResolver, imageUri
                )
            } else {
                val source = ImageDecoder.createSource(this.contentResolver, imageUri)
                ImageDecoder.decodeBitmap(source)
            }
        } catch (e: Exception) {
            Log.e(TAG, e.toString())
        }
        return bitmap
    }
}
