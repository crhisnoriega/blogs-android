package com.mcontigo.view.custom;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

public class FontImageTextView extends AppCompatTextView {

    private static final String DRAWABLE = "drawable";

    float imageScale;
    /**
     * Regex pattern that looks for embedded images of the format: [img src=imageName/]
     */
    public static final String PATTERN = "\\Q[img src=\\E([a-zA-Z0-9_]+?)\\Q/]\\E";

    public FontImageTextView(Context context) {
        this(context,null);
    }

    public FontImageTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontImageTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setText(CharSequence text, AppCompatTextView.BufferType type) {
        final Spannable spannable = getTextWithImages(getContext(), text, getLineHeight(), getCurrentTextColor());
        super.setText(spannable, AppCompatTextView.BufferType.SPANNABLE);
    }

    private Spannable getTextWithImages(Context context, CharSequence text, int lineHeight, int colour) {
        final Spannable spannable = Spannable.Factory.getInstance().newSpannable(text);
        addImages(context, spannable, lineHeight, colour);
        return spannable;
    }

    private boolean addImages(Context context, Spannable spannable, int lineHeight, int colour) {
        final Pattern refImg = Pattern.compile(PATTERN);
        boolean hasChanges = false;

        final Matcher matcher = refImg.matcher(spannable);
        while (matcher.find()) {
            boolean set = true;
            for (ImageSpan span : spannable.getSpans(matcher.start(), matcher.end(), ImageSpan.class)) {
                if (spannable.getSpanStart(span) >= matcher.start()
                        && spannable.getSpanEnd(span) <= matcher.end()) {
                    spannable.removeSpan(span);
                } else {
                    set = false;
                    break;
                }
            }
            final String resName = spannable.subSequence(matcher.start(1), matcher.end(1)).toString().trim();
            final int id = context.getResources().getIdentifier(resName, DRAWABLE, context.getPackageName());
            if (set) {
                hasChanges = true;
                spannable.setSpan(makeImageSpan(context, id, (int)(((float)lineHeight)*.7), colour),
                        matcher.start(),
                        matcher.end(),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                );
            }
        }
        return hasChanges;
    }

    /**
     * Create an ImageSpan for the given icon drawable. This also sets the image size and colour.
     * Works best with a white, square icon because of the colouring and resizing.
     *
     * @param context       The Android Context.
     * @param drawableResId A drawable resource Id.
     * @param size          The desired size (i.e. width and height) of the image icon in pixels.
     *                      Use the lineHeight of the TextView to make the image inline with the
     *                      surrounding text.
     * @param colour        The colour (careful: NOT a resource Id) to apply to the image.
     * @return An ImageSpan, aligned with the bottom of the text.
     * /
    private static CenteredImageSpan makeImageSpan(Context context, int drawableResId, int size, int colour) {
    final Drawable drawable = context.getResources().getDrawable(drawableResId);
    drawable.mutate();
    //drawable.setColorFilter(colour, PorterDuff.Mode.MULTIPLY);
    drawable.setBounds(0, 0, (int)(size*1.2f), (int)(size*1.2f));
    CenteredImageSpan imageSpan = new CenteredImageSpan(drawable, DynamicDrawableSpan.ALIGN_BOTTOM);
    return imageSpan;
    }
     */
    private ImageSpan makeImageSpan(Context context, int drawableResId, int size, int colour) {
        final Drawable drawable = context.getResources().getDrawable(drawableResId);
        drawable.mutate();
        //drawable.setColorFilter(colour, PorterDuff.Mode.MULTIPLY);
        if(imageScale>0f){
            drawable.setBounds(0, 0, (int)(size*imageScale), (int)(size*imageScale));
        }else{
            drawable.setBounds(0, 0, (int)(size*1.2f), (int)(size*1.2f));
        }
        ImageSpan imageSpan = new ImageSpan(drawable, DynamicDrawableSpan.ALIGN_BASELINE);
        return imageSpan;
    }
}