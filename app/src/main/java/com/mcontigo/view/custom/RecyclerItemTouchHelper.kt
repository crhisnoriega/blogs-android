package com.mcontigo.view.custom

import android.graphics.Canvas
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.mcontigo.R
import com.mcontigo.viewholders.BookmarkLeftViewHolder
import com.mcontigo.viewholders.BookmarkViewHolder

class RecyclerItemTouchHelper(dragDirs: Int, swipeDirs: Int, val listener: RecyclerItemTouchHelperListener, val swipeRefreshLayout: SwipeRefreshLayout?) :
    ItemTouchHelper.SimpleCallback(dragDirs, swipeDirs) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if(viewHolder!=null){
            var foregroundView: ConstraintLayout? = null
            if(viewHolder is BookmarkViewHolder){
                foregroundView = viewHolder.viewForeground
            }else if(viewHolder is BookmarkLeftViewHolder){
                foregroundView = viewHolder.viewForeground
            }
            foregroundView?.also {
                ItemTouchHelper.Callback.getDefaultUIUtil().onSelected(it)
            }
        }
        val swiping = actionState == ItemTouchHelper.ACTION_STATE_SWIPE
        swipeRefreshLayout?.isEnabled = !swiping
    }

    override fun onChildDrawOver(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder?,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        var foregroundView: ConstraintLayout? = null
        if(viewHolder is BookmarkViewHolder){
            foregroundView = viewHolder.viewForeground
        }else if(viewHolder is BookmarkLeftViewHolder){
            foregroundView = viewHolder.viewForeground
        }
        foregroundView?.also {
            ItemTouchHelper.Callback.getDefaultUIUtil().onDrawOver(c, recyclerView, it, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        var foregroundView: ConstraintLayout? = null
        if(viewHolder is BookmarkViewHolder){
            foregroundView = viewHolder.viewForeground
        }else if(viewHolder is BookmarkLeftViewHolder){
            foregroundView = viewHolder.viewForeground
        }
        foregroundView?.also {
            ItemTouchHelper.Callback.getDefaultUIUtil().clearView(it)
        }
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        var foregroundView: ConstraintLayout? = null
        if(viewHolder is BookmarkViewHolder){
            foregroundView = viewHolder.viewForeground
        }else if(viewHolder is BookmarkLeftViewHolder){
            foregroundView = viewHolder.viewForeground
        }
        foregroundView?.also {
            ItemTouchHelper.Callback.getDefaultUIUtil().onDraw(c, recyclerView, it, dX, dY, actionState, isCurrentlyActive)
        }
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        listener.onSwiped(viewHolder, direction, viewHolder.adapterPosition)
    }

    interface RecyclerItemTouchHelperListener {
        fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int)
    }
}