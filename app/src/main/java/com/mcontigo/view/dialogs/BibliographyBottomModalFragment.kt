package com.mcontigo.view.dialogs


import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcontigo.R
import com.mcontigo.databinding.FragmentBibliographyBottomModalBinding
import io.mcontigo.richtextlib.v2.RichTextV2

/**
 * A simple [Fragment] subclass.
 */
class BibliographyBottomModalFragment(private val contentBibliography : String) : BottomSheetDialogFragment() {

    private lateinit var binding : FragmentBibliographyBottomModalBinding

    private var bottomSheet: NestedScrollView? = null
    private var bottomSheetPeekHeight: Int = 0
    private var behavior: BottomSheetBehavior<View>? = null
    private var bottomDialog : Dialog? = null

    companion object {
        const val DIALOG_TAG = "bottombibliography"
        fun newInstance(contentBibliography : String): BibliographyBottomModalFragment {
            return BibliographyBottomModalFragment(contentBibliography)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bibliography_bottom_modal, container, false)
        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog =  super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById(R.id.design_bottom_sheet) as FrameLayout
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED)
        }

        bottomDialog = dialog

        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpBottomSheet()
    }

    private fun setUpBottomSheet() {
        behavior = BottomSheetBehavior.from(view?.parent as View)

        bottomSheet = binding.content

        // 86dp
        bottomSheetPeekHeight = 0


        val params = bottomSheet?.layoutParams
        params?.height = getScreenHeight()
        bottomSheet?.layoutParams = params


        behavior = BottomSheetBehavior.from(view!!.parent as View)


        val element = RichTextV2.textFromHtml(activity!!, contentBibliography)
        binding.tvPostContentRich.setText(element)


        binding.imgClose2.setOnClickListener {
            bottomDialog?.dismiss()
        }
    }

    private fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }



}
