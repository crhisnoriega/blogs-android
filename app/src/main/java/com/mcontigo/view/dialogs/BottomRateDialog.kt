package com.mcontigo.view.dialogs

import android.app.Dialog
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mcontigo.BuildConfig
import com.mcontigo.R
import com.mcontigo.databinding.DialogRateLayoutBinding

class BottomRateDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogRateLayoutBinding

    private var bottomSheet: ConstraintLayout? = null
    private var bottomSheetPeekHeight: Int = 0
    private var behavior: BottomSheetBehavior<View>? = null
    private var bottomDialog: Dialog? = null

    companion object {
        fun newInstance(): BottomRateDialog { return BottomRateDialog()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_rate_layout,
            container,
            false
        )
        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)


        dialog.setOnShowListener {
            val bottomSheet = dialog.findViewById(R.id.design_bottom_sheet) as FrameLayout
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED)
        }

        bottomDialog = dialog


        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpBottomSheet()
    }

    private fun setUpBottomSheet() {


        val prefs = context?.getSharedPreferences("apprater", 0)
//        if (prefs?.getBoolean("dontshowagain", false) == true) {
//            dialog?.dismiss()
//            return
//
//        }

        val editor = prefs?.edit()

        behavior = BottomSheetBehavior.from(view?.parent as View)

        bottomSheet = binding.content

        binding.tvTitle.text = String.format(getString(R.string.lbl_title_rate), getString(R.string.app_name))
        binding.tvMsg.text = getString(R.string.lbl_msg_rate_app)
        binding.btnRate.text = getString(R.string.lbl_rate)
        binding.btnRate.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=${BuildConfig.APPLICATION_ID}")
                )
            )
            editor?.putBoolean("dontshowagain", true)
            editor?.apply()
            dialog?.dismiss()
        }

        binding.btnLater.text = getText(R.string.lbl_remind_later)
        binding.btnLater.setOnClickListener {
            editor?.putLong("launch_count", 0)
            editor?.apply()
            dialog?.dismiss()
        }

        binding.btnNo.text = getText(R.string.lbl_no_rate)
        binding.btnNo.setOnClickListener {
            editor?.putBoolean("dontshowagain", true)
            editor?.commit()
            dialog?.dismiss()
        }
    }

    private fun getScreenHeight(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

}