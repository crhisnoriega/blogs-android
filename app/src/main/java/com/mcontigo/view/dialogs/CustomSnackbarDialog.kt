package com.mcontigo.view.dialogs

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R

object CustomSnackbarDialog {
    fun newDialogBookmarkAdd(context: Context): Snackbar {
        return customSnackBar(context, R.layout.snackbar_bookmark_added)
    }

    fun newDialogBookmarkDuplicated(context: Context): Snackbar {
        return customSnackBar(context, R.layout.snackbar_bookmark_duplicated)
    }

    fun newDialogBookmarkRemoved(context: Context, runnable: Runnable?): Snackbar {
        var snackbar = customSnackBar(context, R.layout.snackbar_bookmark_removed)
        runnable?.also {
            snackbar = setCustomAction(snackbar, R.id.tv_undo, runnable)
        }
        return snackbar
    }

    fun newDialogConfirmEmail(context: Context, action: (() -> Unit)): Snackbar{
        var snackbar = customSnackBar(context, R.layout.snackbar_email_confirm)
        return setCustomAction(snackbar, R.id.tv_action, action).setDuration(BaseTransientBottomBar.LENGTH_INDEFINITE)
    }

    fun newDialogConfirmEmailSuccess(context: Context): Snackbar{
        return customSnackBar(context, R.layout.snackbar_email_success)
    }

    fun newDialogBookmarkError(context: Context): Snackbar{
        return customSnackBar(context, R.layout.snackbar_bookmark_error)
    }

    fun setErrorCodeToDialog(snackbar: Snackbar, errorCode: String): Snackbar{
        return setCustomText(snackbar, R.id.tv_error_code, "(${errorCode})")
    }

    private fun customSnackBar(context: Context, res: Int):Snackbar {
        val activity = context as AppCompatActivity
        val inflater = activity.layoutInflater
        val view = inflater.inflate(res, null)
        val snackbar = Snackbar.make(context.findViewById(R.id.nav_host_fragment), "", Snackbar.LENGTH_LONG)
        val layout = snackbar.view as Snackbar.SnackbarLayout
        val textView = layout.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.visibility = View.INVISIBLE
        val bottomNav = context.findViewById<BottomNavigationView?>(R.id.bottomNavigationView)
        var marginBottom = 0
        bottomNav?.let{
            marginBottom = it.height
        }
        val layoutParams = layout.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.setMargins(0,0,0, marginBottom)
        layout.setPadding(0,0,0,0)
        layout.addView(view)
        return snackbar
    }
    
    public fun customSnackBar(view: View, context: Context, res: Int):Snackbar {
        val activity = context as AppCompatActivity
        val inflater = activity.layoutInflater
        val view = inflater.inflate(res, null)
        val snackbar = Snackbar.make(view, "", Snackbar.LENGTH_LONG)
        val layout = snackbar.view as Snackbar.SnackbarLayout
        val textView = layout.findViewById<TextView>(com.google.android.material.R.id.snackbar_text)
        textView.visibility = View.INVISIBLE
        val bottomNav = context.findViewById<BottomNavigationView?>(R.id.bottomNavigationView)
        var marginBottom = 0
        bottomNav?.let{
            marginBottom = it.height
        }
        val layoutParams = layout.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.setMargins(0,0,0, marginBottom)
        layout.setPadding(0,0,0,0)
        layout.addView(view)
        return snackbar
    }

    private fun setCustomText(snackbar: Snackbar, res: Int, text: String): Snackbar{
        val view = snackbar.view.findViewById<TextView>(res)
        view?.text = text;
        return snackbar
    }

    private fun setCustomAction(snackbar: Snackbar, res: Int, runnable: Runnable?): Snackbar{
        val view = snackbar.view.findViewById<TextView>(res)
        view.setOnClickListener {
            runnable?.run()
        }
        return snackbar
    }

    private fun setCustomAction(snackbar: Snackbar, res: Int, action: () -> Unit): Snackbar{
        val view = snackbar.view.findViewById<TextView>(res)
        view.setOnClickListener(View.OnClickListener { action.invoke() })
        return snackbar
    }

}