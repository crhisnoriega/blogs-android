package com.mcontigo.view.dialogs


import android.app.Activity
import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.mcontigo.R
import com.mcontigo.databinding.DialogBookmarkTutorialBinding
import com.mcontigo.databinding.FragmentInfoDialogLayoutBinding
import com.mcontigo.utils.MenuItensUtil
import com.mcontigo.view.MainHostActivity
import com.mcontigo.view.fragments.HomeViewPagerFragment

class DialogBookmarkTutorial() : DialogFragment() {

    private lateinit var binding: DialogBookmarkTutorialBinding
    private var bottomDialog: Dialog? = null

    companion object {
        const val DIALOG_TAG = "DialogBookmarkTutorial"
        fun newInstance(): DialogBookmarkTutorial {
            return DialogBookmarkTutorial()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_bookmark_tutorial,
            container,
            false
        )
        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        bottomDialog = dialog
        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpDialog()
    }

    private fun setUpDialog() {
        dialog?.window?.setLayout(getScreenWidth(), ViewGroup.LayoutParams.WRAP_CONTENT)

        binding.imgClose.setOnClickListener {
            bottomDialog?.dismiss()
        }

        binding.btnClose.setOnClickListener {
            bottomDialog?.dismiss()
        }

        binding.btnAction.setOnClickListener{
            MenuItensUtil.bottomPageForceSelector.postValue(2)
            bottomDialog?.dismiss()
        }
    }

    private fun getScreenHeigth(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }
    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }


}
