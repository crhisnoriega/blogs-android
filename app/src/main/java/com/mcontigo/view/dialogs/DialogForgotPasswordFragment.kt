package com.mcontigo.view.dialogs


import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.ForgotPasswordFragmentBinding
import com.mcontigo.utils.StateUI
import com.mcontigo.viewmodel.ForgotPasswordViewModel

class DialogForgotPasswordFragment : DialogFragment() {


    private val viewModel by lazy {
        ViewModelProviders.of(this).get(ForgotPasswordViewModel::class.java)
    }
    private lateinit var binding: ForgotPasswordFragmentBinding
    private var bottomDialog: Dialog? = null
    private var dialogProgressFragment: DialogProgressFragment? = null


    companion object {
        const val DIALOG_TAG = "dialogForgotPassword"
        fun newInstance(): DialogForgotPasswordFragment {
            return DialogForgotPasswordFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.forgot_password_fragment,
            container,
            false
        )
        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        bottomDialog = dialog
        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpDialog()
    }

    private fun setUpDialog() {

        dialog?.window?.setLayout(getScreenWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);

        binding.imgCloseDialog.setOnClickListener {
            bottomDialog?.dismiss()
        }

        viewModel.email.observe(viewLifecycleOwner, Observer {
            binding.btnChangePassword.isEnabled =
                android.util.Patterns.EMAIL_ADDRESS.matcher(it).matches()
        })

        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        viewModel.stateUI.observe(viewLifecycleOwner, Observer {

            val singleLiveEventIfNoHandled = it.getContentIfNotHandled()
            dialogProgressFragment?.dismiss()
            when (singleLiveEventIfNoHandled?.stateUI) {

                StateUI.SUCCESS -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.message  ?: getString(R.string.EmailHasBeenSentWithPasswordRecovery),
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                    Handler().postDelayed({
                        snackbar.dismiss()
                        dismiss()
                    }, 3000)
                }
                StateUI.LOADING -> {
                    dialogProgressFragment =
                        DialogProgressFragment.newInstance(getString(singleLiveEventIfNoHandled.messageResId!!))
                    dialogProgressFragment?.show(
                        fragmentManager!!,
                        DialogProgressFragment.DIALOG_TAG
                    )
                }
                StateUI.FAIL -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.message?.let { it }
                            ?: run { getString(R.string.ChangePasswordErrorMessage) },
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            }
        })
    }

    private fun getScreenHeigth(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }
}
