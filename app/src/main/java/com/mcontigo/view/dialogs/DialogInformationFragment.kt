package com.mcontigo.view.dialogs


import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.mcontigo.R
import com.mcontigo.databinding.FragmentInfoDialogLayoutBinding

class DialogInformationFragment(
    private val title: String,
    private val content: String,
    private val drawableResource: Int? = null
) : DialogFragment() {

    private lateinit var binding: FragmentInfoDialogLayoutBinding
    private var bottomDialog: Dialog? = null

    companion object {
        const val DIALOG_TAG = "bottomFastChecked"
        fun newInstance(
            title: String,
            contentBibliography: String,
            drawableResource: Int? = null
        ): DialogInformationFragment {
            return DialogInformationFragment(
                title,
                contentBibliography,
                drawableResource
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_info_dialog_layout,
            container,
            false
        )
        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        bottomDialog = dialog
        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpDialog()
    }

    private fun setUpDialog() {


        dialog?.window?.setLayout(getScreenWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);

        drawableResource?.also {
            binding.imgDialog.setImageDrawable(ContextCompat.getDrawable(activity!!, it))
        } ?: run {
            binding.imgDialog.visibility = View.GONE
        }

        binding.tvDialogTitle.text = title
        binding.tvPostContentRich.text = content


        binding.imgClose2.setOnClickListener {
            bottomDialog?.dismiss()
        }
    }

    private fun getScreenHeigth(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }
    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }


}
