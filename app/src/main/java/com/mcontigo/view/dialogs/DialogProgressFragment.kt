package com.mcontigo.view.dialogs


import android.app.Dialog
import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.mcontigo.R
import com.mcontigo.databinding.FragmentDialogProgressLoadingLayoutBinding
import com.mcontigo.databinding.FragmentInfoDialogLayoutBinding

class DialogProgressFragment(
    private val message: String
) : DialogFragment() {

    private lateinit var binding: FragmentDialogProgressLoadingLayoutBinding
    private var bottomDialog: Dialog? = null

    companion object {
        const val DIALOG_TAG = "dialogProgress"
        fun newInstance(
            message: String
        ): DialogProgressFragment {
            return DialogProgressFragment(
                message
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_dialog_progress_loading_layout,
            container,
            false
        )
        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        bottomDialog = dialog
        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpDialog()
    }

    private fun setUpDialog() {

        dialog?.window?.setLayout(getScreenWidth(), ViewGroup.LayoutParams.WRAP_CONTENT)

        binding.tvMessage.text = message

        binding



    }

    private fun getScreenHeigth(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }
    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }


}
