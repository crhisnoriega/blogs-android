package com.mcontigo.view.dialogs


import android.app.Dialog
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.mcontigo.R
import com.mcontigo.databinding.DialogLogoutLayoutBinding

class DialogQuestionFragment(
    private val title: String,
    private val message: String,
    private val textButtonLeft: String? = null,
    private val textButtonRight: String? = null,
    private val buttonLeftAction: (() -> Unit)?,
    private val buttonRightAction: (() -> Unit)?
) : DialogFragment() {

    private lateinit var binding: DialogLogoutLayoutBinding
    private var bottomDialog: Dialog? = null

    companion object {
        const val DIALOG_TAG = "dialogQuestion"
        fun newInstance(
            title: String,
            message: String,
            textButtonLeft: String? = null,
            textButtonRight: String? = null,
            buttonLeftAction: (() -> Unit)?,
            buttonRightAction: (() -> Unit)?
        ): DialogQuestionFragment {
            return DialogQuestionFragment(
                title,
                message,
                textButtonLeft,
                textButtonRight,
                buttonLeftAction,
                buttonRightAction
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_logout_layout,
            container,
            false
        )
        return binding.root
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        bottomDialog = dialog
        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpDialog()
    }

    private fun setUpDialog() {

        dialog?.window?.setLayout(getScreenWidth(), ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));

        binding.tvTitle.text = title
        binding.tvMsg.text = message


        textButtonLeft?.also {
            binding.btnLeft.text = it
        } ?: run {
            binding.btnLeft.text = getString(R.string.lbl_yes)
        }

        textButtonRight?.also {
            binding.btnRight.text = it
        } ?: run {
            binding.btnRight.text = getString(R.string.lbl_no)
        }

        buttonLeftAction?.also { action ->
            binding.btnLeft.visibility = View.VISIBLE
            binding.btnLeft.setOnClickListener {
                action()
                dismiss()
            }
        }?: run {
            binding.btnLeft.visibility = View.GONE
        }

        buttonRightAction?.also { action ->
            binding.btnRight.visibility = View.VISIBLE
            binding.btnRight.setOnClickListener {
                action()
                dismiss()
            }
        }?: run {
            binding.btnRight.visibility = View.GONE
        }


    }

    private fun getScreenHeigth(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }

    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }


}
