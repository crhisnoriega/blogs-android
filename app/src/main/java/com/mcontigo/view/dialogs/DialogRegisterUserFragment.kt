package com.mcontigo.view.dialogs


import android.app.Dialog
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.FragmentInfoDialogLayoutBinding
import com.mcontigo.databinding.RegisterUserFragmentBinding
import com.mcontigo.utils.StateUI
import com.mcontigo.viewmodel.RegisterUserViewModel

class DialogRegisterUserFragment : DialogFragment() {

    val RC_SIGN_IN = 905
    private val TAG = this@DialogRegisterUserFragment.javaClass.simpleName

    private val viewModel by lazy{
        ViewModelProviders.of(this).get(RegisterUserViewModel::class.java)
    }
    private lateinit var binding: RegisterUserFragmentBinding
    private var bottomDialog: Dialog? = null
    private var dialogProgressFragment: DialogProgressFragment? = null
    lateinit var callbackManager: CallbackManager
    lateinit var googleSignIn: GoogleSignInClient

    companion object {
        const val DIALOG_TAG = "dialogRegisterUser"
        fun newInstance(): DialogRegisterUserFragment {
            return DialogRegisterUserFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.register_user_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)

    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        bottomDialog = dialog
        return dialog
    }


    override fun onResume() {
        super.onResume()
        setUpDialog()
    }

    private fun setUpDialog() {

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestEmail()
        .build()

        binding.btnGoogle.setOnClickListener{
            val signInIntent = googleSignIn.signInIntent;
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        googleSignIn = GoogleSignIn.getClient(activity!!, gso)
        dialog?.window?.setWindowAnimations(R.style.AppTheme_Slide)
        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        binding.imgCloseDialog.setOnClickListener {
            bottomDialog?.dismiss()
        }

        binding.btnHaveAccount.setOnClickListener {
            bottomDialog?.dismiss()
            fragmentManager?.also{
                DialogConnectUserFragment.newInstance().show(it, DialogConnectUserFragment.DIALOG_TAG)
            }
        }


        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        callbackManager = CallbackManager.Factory.create()


        binding.btnFacebook.fragment = this
        binding.btnFacebook.setReadPermissions("email")

        binding.btnFacebook.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    viewModel.authFacebook(loginResult.accessToken)
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    Log.e(DIALOG_TAG, exception.toString())
                    val snackbar = Snackbar.make(
                        binding.root,
                        getString(R.string.lbl_user_fail_register),
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            })

        viewModel.stateUI.observe(viewLifecycleOwner, Observer {

            val singleLiveEventIfNoHandled = it.getContentIfNotHandled()
            dialogProgressFragment?.dismiss()
            when (singleLiveEventIfNoHandled?.stateUI) {

                StateUI.SUCCESS -> {
                    dismiss()
                }
                StateUI.LOADING -> {
                    dialogProgressFragment = DialogProgressFragment.newInstance(getString(singleLiveEventIfNoHandled.messageResId!!))
                    dialogProgressFragment?.show(fragmentManager!!, DialogProgressFragment.DIALOG_TAG)
                }
                StateUI.FAIL -> {
                    val snackbar = Snackbar.make(
                        binding.root,
                        singleLiveEventIfNoHandled.messageResId?.let { getString(it) }  ?: run {getString(R.string.lbl_user_fail_register)},
                        Snackbar.LENGTH_SHORT
                    )
                    snackbar.view.setBackgroundColor(
                        ContextCompat.getColor(
                            activity!!,
                            R.color.colorWhite
                        )
                    )
                    snackbar.show()
                }
            }
        })
    }

    private fun getScreenHeigth(): Int {
        return Resources.getSystem().displayMetrics.heightPixels
    }
    private fun getScreenWidth(): Int {
        return Resources.getSystem().displayMetrics.widthPixels
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }


        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            account?.let {
                viewModel.googleAuth(it)
            }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.e(TAG, e.toString())
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
        }

    }


}
