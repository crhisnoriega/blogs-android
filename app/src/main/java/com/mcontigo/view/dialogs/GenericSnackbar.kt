package com.mcontigo.view.dialogs

import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.mcontigo.R
import com.mcontigo.databinding.SnackbarGenericLayoutBinding
import java.lang.Exception

class GenericSnackbar {

    companion object {
        private val TAG = this@Companion.javaClass.simpleName
        const val EVENT_DEFAULT = 0
        const val EVENT_SUCCESS = 1
        const val EVENT_FAIL = 2

        fun make(
            view: View,
            event: Int = 0,
            msg: String,
            icon: Drawable? = null,
            customBackgroundColor: Int? = null,
            snackbarTime: Int = Snackbar.LENGTH_LONG,
            actionText: String? = null,
            action: (() -> Unit)? = null
        ): Snackbar {
            val binding =
                DataBindingUtil.inflate<SnackbarGenericLayoutBinding>(
                    LayoutInflater.from(view.context),
                    R.layout.snackbar_generic_layout,
                    view.parent as ViewGroup,
                    false
                )

            val snackbar = Snackbar.make(
                view.parent as ViewGroup,
                "",
                snackbarTime
            )

            binding.text.text = msg

            icon?.also {
                binding.image.setImageDrawable(it)
            } ?: run {
                binding.image.visibility = View.GONE
            }

            action?.also { actionEvent ->
                binding.buttonSnackAction.visibility = View.VISIBLE

                actionText?.also {
                    binding.buttonSnackAction.text = it
                } ?: run {
                    binding.buttonSnackAction.text =
                        view.context.getString(R.string.RecoveyEmailButtonSend)
                }

                binding.buttonSnackAction.setOnClickListener {
                    actionEvent()
                    snackbar.dismiss()
                }

            } ?: run {
                binding.buttonSnackAction.visibility = View.GONE

            }

            val layout = snackbar.view as Snackbar.SnackbarLayout
            layout.setPadding(0, 0, 0, 0)
            val layoutParams = layout.layoutParams as FrameLayout.LayoutParams
            layoutParams.setMargins(0,0,0, view.context.resources.getDimension(R.dimen.bottom_navigation_toolbar).toInt())
            layout.addView(binding.root)

            when (event) {
                EVENT_SUCCESS -> {
                    binding.toastLayoutRoot.setBackgroundColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.success_custom_snackbar
                        )
                    )
                    binding.image.setImageDrawable(
                        ContextCompat.getDrawable(
                            view.context,
                            R.drawable.ic_check_circle
                        )
                    )
                    binding.image.visibility = View.VISIBLE
                }
                EVENT_FAIL -> {
                    binding.toastLayoutRoot.setBackgroundColor(
                        ContextCompat.getColor(
                            view.context,
                            R.color.error_custom_snackbar
                        )
                    )
                    binding.image.setImageDrawable(
                        ContextCompat.getDrawable(
                            view.context,
                            R.drawable.ic_snackbar_error
                        )
                    )

                    binding.image.visibility = View.VISIBLE
                }
                else -> {
                    customBackgroundColor?.also {
                        try {
                            binding.toastLayoutRoot.setBackgroundColor(
                                ContextCompat.getColor(
                                    view.context,
                                    it
                                )
                            )
                        } catch (e: Exception) {
                            Log.e(TAG, e.toString())
                        }
                    }
                }
            }

            return snackbar
        }
    }
}