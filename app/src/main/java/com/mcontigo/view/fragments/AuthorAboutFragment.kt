package com.mcontigo.view.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil

import com.mcontigo.R
import com.mcontigo.databinding.FragmentAuthorAboutBinding
import com.mcontigo.utils.HtmlUtil


class AuthorAboutFragment : Fragment() {

    private lateinit var _binding: FragmentAuthorAboutBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_author_about, container, false)
        return _binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        _binding.tvAuthorDescription.text =
            HtmlUtil.fromHtml(arguments?.getString(AUTHOR_ABOUT) ?: "")
    }

    companion object {
        private const val AUTHOR_ABOUT = "authorAbout"
        fun newInstance(authorAbout: String) = AuthorAboutFragment().apply {
            arguments = Bundle().apply {
                putString(AUTHOR_ABOUT, authorAbout)
            }
        }
    }


}
