package com.mcontigo.view.fragments


import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.mcontigo.R
import com.mcontigo.adapters.ItensPostAdapter
import com.mcontigo.androidwpmodule.dao.post.Author
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.FragmentAuthorPostsBinding
import com.mcontigo.enums.State
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.view.PostDetailsActivity
import com.mcontigo.viewmodel.AuthorPostsViewModel

class AuthorPostsFragment : Fragment(), PostContentRvInterface {


    private val TAG = this@AuthorPostsFragment.javaClass.simpleName
    private lateinit var _binding: FragmentAuthorPostsBinding
    private val _viewModel by lazy {
        ViewModelProviders.of(this).get(AuthorPostsViewModel::class.java)
    }
    private var _adapterItens: ItensPostAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_author_posts, container, false)
        return _binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        _viewModel.getPosts(arguments?.getParcelable<Author>(AUTHOR)?.id!!)

        _binding.refreshLayout.setOnRefreshListener {
            _viewModel.refresh()
        }

        _viewModel.itensPagedList.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "${it.size}")
            _adapterItens = ItensPostAdapter(
                this@AuthorPostsFragment, ""
            ) { _viewModel.retry() }


            val layoutManager = LinearLayoutManager(activity)
            _binding.rvItensPost.layoutManager = layoutManager


            _binding.rvItensPost.adapter = _adapterItens
            _adapterItens?.submitList(it)
            _binding.refreshLayout.isRefreshing = false

        })


        _viewModel.getState().observe(viewLifecycleOwner, Observer {
            Log.d(TAG, it.toString())

            if (it == State.LOADING && (_viewModel.listIsEmpty() || _binding.refreshLayout.isRefreshing)) {
                _binding.shimmerViewContainer.startShimmer()
                _binding.errorLayout.errorLayout.visibility = View.GONE
                _binding.rvItensPost.visibility = View.GONE
                _binding.shimmerViewContainer.visibility = View.VISIBLE
            } else {
                _binding.shimmerViewContainer.stopShimmer()
                _binding.shimmerViewContainer.visibility = View.GONE
                if (it == State.ERROR && _viewModel.listIsEmpty()) {
                    _binding.errorLayout.errorLayout.visibility = View.VISIBLE
                } else {
                    _binding.rvItensPost.visibility = View.VISIBLE
                    _adapterItens?.setState(it ?: State.DONE)
                }
            }
        })


        _binding.errorLayout.btnTryAgain.setOnClickListener {
            _viewModel.retry()
        }

    }


    companion object {
        private const val AUTHOR = "author"
        fun newInstance(author: Author) = AuthorPostsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(AUTHOR, author)
            }
        }
    }


    override fun onItemClick(itemPost: Post) {
        Log.d(TAG, itemPost.toString())
        itemPost.id?.also {
            PostDetailsActivity.openPostActivity(
                activity!!,
                it,
                itemPost.featuredMedia?.large,
                TextUtils.htmlEncode(itemPost.categories?.get(0)?.name)
            )
            activity?.finish()
        }

    }


}
