package com.mcontigo.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mcontigo.R
import android.widget.Button
import com.mcontigo.view.dialogs.DialogConnectToPay

class BookmarkEmptyFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bookmark_empty_fragment, container, false)
    }
}
