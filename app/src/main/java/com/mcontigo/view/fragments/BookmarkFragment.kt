package com.mcontigo.view.fragments

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.mcontigo.R
import com.mcontigo.viewmodel.SettingsViewModel
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.opengl.Visibility
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.adapters.BookmarkPostAdapter
import com.mcontigo.androidauthmodule.model.User
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.enums.State
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.utils.*
import com.mcontigo.view.PostDetailsActivity
import com.mcontigo.view.custom.RecyclerItemTouchHelper
import com.mcontigo.viewholders.BookmarkViewHolder
import com.mcontigo.viewmodel.BookmarkViewModel


class BookmarkFragment : Fragment(), PostContentRvInterface, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {
    private lateinit var viewModel: BookmarkViewModel
    private lateinit var binding: com.mcontigo.databinding.BookmarkFragmentBinding
    private var user: User? = null
    private var adapter: BookmarkPostAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.bookmark_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        binding.toolbar.toolbarLogo.visibility = View.GONE
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.appBarLayoutCustom.elevation = 4f
        binding.toolbar.toolbarTitle.text = getString(R.string.BookmarkNavigationTitle)
        binding.toolbar.imgButtonMenu.visibility = View.VISIBLE
        binding.toolbar.imgButtonMenu.setOnClickListener {
            CustomToolbarUtil.observerOpenDrawer.postValue(true)
        }

        user = UserUtil.user.value

        UserUtil.user.observe(this, Observer {
            user = it
            updateUI()
        })

        viewModel = ViewModelProviders.of(this).get(BookmarkViewModel::class.java)
        binding.viewModel = viewModel

        updateUI()
    }

    fun updateUI(){
        val mFragmentManager = childFragmentManager
        mFragmentManager?.also {
            if(user == null){
                binding.refreshLayout.setOnRefreshListener {
                    binding.refreshLayout.isRefreshing = false
                }
                binding.flContent.visibility = View.VISIBLE
                binding.rvBookmark.visibility = View.GONE
                it.beginTransaction()
                   .replace(R.id.fl_content, BookmarkSignoutFragment(), "BookmarkSignoutFragment")
                   .addToBackStack(null)
                   .commit()
            }else{
                viewModel.fetchBookmarks()
                binding.refreshLayout.setOnRefreshListener {
                    viewModel.fetchBookmarks()
                    BookmarkUtil.shouldUpdateBookmarkList()
                }
                viewModel.getPostList().observe(this, Observer {lt ->
                    //Add code to list of bookmarks
                    binding.refreshLayout.isRefreshing = false
                    if(lt == null || lt.isEmpty()){
                        binding.flContent.visibility = View.VISIBLE
                        binding.rvBookmark.visibility = View.GONE
                        it.beginTransaction()
                            .replace(R.id.fl_content, BookmarkEmptyFragment(), "BookmarkEmptyFragment")
                            .addToBackStack(null)
                            .commit()
                    }else{
                        binding.flContent.visibility = View.GONE
                        binding.rvBookmark.visibility = View.VISIBLE
                        if(adapter == null){
                            adapter = BookmarkPostAdapter(lt, this)
                            adapter?.notifyDataSetChanged()
                            binding.rvBookmark.adapter = adapter
                            binding.rvBookmark.layoutManager = LinearLayoutManager(this.context)
                            binding.rvBookmark.itemAnimator = DefaultItemAnimator()

                            var itemTouchHelperCallback = RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this, binding.refreshLayout)
                            ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(binding.rvBookmark)
                        }else{
                            adapter?.setListPost(lt)
                            adapter?.notifyDataSetChanged()
                        }
                    }
                })
                val scrollListener = object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)
                        val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                        var listSize = 0
                        viewModel.getPostList().value?.also{
                            listSize = it.size
                        }
                        if(layoutManager.findLastCompletelyVisibleItemPosition() > listSize-5 && viewModel.canLoadMore()){
                            viewModel.fetchBookmarks(true)
                        }
                    }
                }
                binding.rvBookmark.addOnScrollListener(scrollListener)
                it.beginTransaction()
                    .replace(R.id.fl_content, BookmarkEmptyFragment(), "BookmarkEmptyFragment")
                    .addToBackStack(null)
                    .commit()
            }
        }
    }

    override fun onItemClick(postContent: Post) {
        PostDetailsActivity.openPostActivity(activity!!, postContent.slug!!)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        var index = viewHolder.adapterPosition
        viewModel.removeBookmark(index, context!!)
    }

}
