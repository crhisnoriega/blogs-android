package com.mcontigo.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.mcontigo.R
import android.widget.Button
import com.mcontigo.view.dialogs.DialogConnectToPay

class BookmarkSignoutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.bookmark_signout_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val getPremium = view?.findViewById<Button>(R.id.btn_sign_out_get_premium)
        getPremium?.setOnClickListener {
            DialogConnectToPay.newInstance().show(activity!!.supportFragmentManager, DialogConnectToPay.DIALOG_TAG)
        }
    }
}
