package com.mcontigo.view.fragments

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.mcontigo.R
import com.mcontigo.adapters.PageAdapter
import com.mcontigo.androidwpmodule.dao.menu.Item
import com.mcontigo.databinding.HomeFragmentBinding
import com.mcontigo.utils.AppBarStateChangeListener
import com.mcontigo.utils.CustomToolbarUtil
import com.mcontigo.utils.MenuItensUtil
import com.mcontigo.viewmodel.HomeViewModel
import java.util.concurrent.TimeUnit

const val OFFSCREEN_PAGE_LIMIT_VIEW_PAGER_LOAD = 3

class HomeFragment : Fragment() {
    private val TAG = this@HomeFragment.javaClass.simpleName
    private lateinit var viewModel: HomeViewModel
    private lateinit var binding: HomeFragmentBinding
    private lateinit var listCategory: MutableList<Item>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        lifecycle.addObserver(viewModel)
        binding.viewModel = viewModel


        binding.layoutTabs.visibility = View.GONE

        binding.toolbarLogo.visibility = View.VISIBLE

        binding.imgButtonMenu.setOnClickListener {
            CustomToolbarUtil.observerOpenDrawer.postValue(true)
        }


        binding.toolbarLogo.setOnClickListener {
            MenuItensUtil.itemSelected.postValue(0)
        }

        binding.appBarLayout.addOnOffsetChangedListener(object : AppBarStateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout, state: State) {
                when (state) {
                    State.COLLAPSED -> {
                        binding.tabs.visibility = View.GONE
                        binding.tabsindicator.visibility = View.VISIBLE
                        binding.tvCategoryTabSelected.visibility = View.VISIBLE
                    }
                    State.EXPANDED -> {
                        binding.tabs.visibility = View.VISIBLE
                        binding.tabsindicator.visibility = View.GONE
                        binding.tvCategoryTabSelected.visibility = View.GONE
                    }
                }
            }

        })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.toolbar.elevation = 0f
        }


        MenuItensUtil.listItens.observe(viewLifecycleOwner, Observer {
            configurarTabs(it)
        })
    }

    private fun configurarTabs(it: List<Item>) {

        listCategory = mutableListOf()

        listCategory.addAll(it)
        val pageAdapter = PageAdapter(childFragmentManager, listCategory)
        binding.pager.adapter = pageAdapter
        binding.pager.offscreenPageLimit = OFFSCREEN_PAGE_LIMIT_VIEW_PAGER_LOAD

        binding.layoutTabs.setBackgroundColor(
            ContextCompat.getColor(
                context!!,
                R.color.colorPrimary
            )
        )



        binding.tabs.setViewPager(binding.pager)
        binding.tabs.setBackgroundColor(ContextCompat.getColor(context!!, R.color.colorPrimary))

        binding.tabsindicator.setViewPager(binding.pager)
        binding.tabsindicator.setBackgroundColor(
            ContextCompat.getColor(
                context!!,
                R.color.colorPrimary
            )
        )


        binding.tabs.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                MenuItensUtil.itemSelected.value = position
            }

        })



        Handler().postDelayed({
            MenuItensUtil.itemSelected.postValue(MenuItensUtil.itemSelected.value)
        }, 1000)


        MenuItensUtil.itemSelected.observe(this, Observer {
            try {
                binding.pager.currentItem = it
                binding.tvCategoryTabSelected.text = listCategory[it].name
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })

        binding.layoutTabs.visibility = View.VISIBLE
    }

}
