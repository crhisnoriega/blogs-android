package com.mcontigo.view.fragments

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.mcontigo.R
import com.mcontigo.adapters.HomeSectionsAdapter
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.HomeGridFragmentBinding
import com.mcontigo.interfaces.ItemPostRvInterface
import com.mcontigo.utils.ClearCacheUtil
import com.mcontigo.view.PostDetailsActivity
import com.mcontigo.viewmodel.HomeGridViewModel

class HomeGridFragment : Fragment(), ItemPostRvInterface {


    private val TAG = this@HomeGridFragment.javaClass.simpleName
    private lateinit var viewModel: HomeGridViewModel
    private lateinit var binding: HomeGridFragmentBinding
    private lateinit var adapterItens: HomeSectionsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_grid_fragment, container, false)
        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(HomeGridViewModel::class.java)
        lifecycle.addObserver(viewModel)


        binding.refreshLayout.setOnRefreshListener {
            ClearCacheUtil.deleteCache(activity!!)
            viewModel.refresh()
        }

        viewModel.getItensPost().observe(viewLifecycleOwner, Observer {
            binding.rvItensPost.recycledViewPool.clear()
            adapterItens = HomeSectionsAdapter(it, this@HomeGridFragment)
            binding.rvItensPost.adapter = adapterItens
            binding.rvItensPost.itemAnimator = null
        })


        viewModel.getLoadingItens().observe(viewLifecycleOwner, Observer {
            if (it.first) {
                binding.shimmerViewContainer.startShimmer()
                binding.errorLayout.errorLayout.visibility = View.GONE
                binding.rvItensPost.visibility = View.GONE
                binding.shimmerViewContainer.visibility = View.VISIBLE
            } else {
                binding.refreshLayout.isRefreshing = false;
                binding.shimmerViewContainer.stopShimmer()
                binding.shimmerViewContainer.visibility = View.GONE
                if (it.second) {
                    binding.rvItensPost.visibility = View.VISIBLE
                } else {
                    binding.errorLayout.errorLayout.visibility = View.VISIBLE
                }
            }
        })


        binding.errorLayout.btnTryAgain.setOnClickListener {
            viewModel.getPostHomeGridApi()
        }
    }


    override fun onItemClick(itemPost: PostContentHome) {
        Log.d(TAG, itemPost.toString())
        itemPost.slug?.let {
            PostDetailsActivity.openPostActivity(activity!!, it)
        }
    }

}
