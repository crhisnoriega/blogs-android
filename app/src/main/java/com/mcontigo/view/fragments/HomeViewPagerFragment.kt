package com.mcontigo.view.fragments


import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mcontigo.R
import com.mcontigo.adapters.BottomPageAdapter
import com.mcontigo.databinding.FragmentHomeViewPagerBinding
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.KeyboardUtil
import com.mcontigo.utils.MenuItensUtil
import com.mcontigo.view.*

class HomeViewPagerFragment : Fragment() {


    private lateinit var binding: FragmentHomeViewPagerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_home_view_pager, container,
            false
        )

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupBottomNavigation()
    }

    private fun setupBottomNavigation() {
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(object :
            BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
                if (menuItem.isChecked) return false
                activity?.also {
                    KeyboardUtil.hideKeyboard(it)
                }
                binding.pager.currentItem = when (menuItem.itemId) {
                    R.id.searchFragment -> SEARCH_SCREEN
                    R.id.homeFragment -> HOME_SCREEN
                    R.id.bookmarkFragment -> BOOKMARK_SCREEN
                    R.id.settingsFragment -> SETTINGS_SCREEN
                    else -> HOME_SCREEN
                }
                MenuItensUtil.bottomPageSelected = menuItem.itemId

                return true
            }
        })

        val pageAdapter = BottomPageAdapter(this@HomeViewPagerFragment)
        if(!BookmarkUtil.isEnabled()){
            binding.bottomNavigationView.menu.removeItem(R.id.bookmarkFragment)
        }
        binding.pager.visibility = View.INVISIBLE
        binding.pager.adapter = pageAdapter
        binding.pager.isUserInputEnabled = false
        binding.pager.offscreenPageLimit = TOTAL_PAGES

//        binding.pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
//            override fun onPageSelected(position: Int) {
//                super.onPageSelected(position)
//            }
//        })


        Handler(Looper.getMainLooper()).postDelayed({
            binding.pager.visibility = View.VISIBLE
            binding.pager.currentItem = HOME_SCREEN
            binding.bottomNavigationView.selectedItemId = R.id.homeFragment
            binding.pager.setCurrentItem(HOME_SCREEN, false)
        }, 100)



        MenuItensUtil.bottomNavigationSelected.observe(viewLifecycleOwner, Observer {
            binding.pager.setCurrentItem(it, false)
            val menuItem = when (it) {
                SEARCH_SCREEN -> R.id.searchFragment
                HOME_SCREEN -> R.id.homeFragment
                SETTINGS_SCREEN -> R.id.settingsFragment
                else -> R.id.homeFragment
            }
            binding.bottomNavigationView.selectedItemId = menuItem
        })

        MenuItensUtil.itemSelected.observe(viewLifecycleOwner, Observer {
            binding.bottomNavigationView.selectedItemId = R.id.homeFragment
            binding.pager.currentItem = HOME_SCREEN
        })

        MenuItensUtil.bottomPageForceSelector.observe(viewLifecycleOwner, Observer {
            binding.pager.setCurrentItem(it, true)
            binding.bottomNavigationView.selectedItemId = when(it){
                SEARCH_SCREEN -> R.id.searchFragment
                HOME_SCREEN -> R.id.homeFragment
                BOOKMARK_SCREEN -> R.id.bookmarkFragment
                SETTINGS_SCREEN -> R.id.settingsFragment
                else -> R.id.homeFragment
            }
        })
    }
}
