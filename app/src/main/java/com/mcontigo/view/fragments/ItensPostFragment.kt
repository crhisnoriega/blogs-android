package com.mcontigo.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mcontigo.R
import com.mcontigo.adapters.ItensPostAdapter
import com.mcontigo.adapters.PageAdapter
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.ItensPostFragmentBinding
import com.mcontigo.enums.State
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.utils.ClearCacheUtil
import com.mcontigo.view.PostDetailsActivity
import com.mcontigo.viewmodel.ItensPostViewModel


class ItensPostFragment : Fragment(), PostContentRvInterface {

    private val TAG = this@ItensPostFragment.javaClass.simpleName
    private lateinit var viewModel: ItensPostViewModel
    private lateinit var binding: ItensPostFragmentBinding
    private var adapterItens: ItensPostAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.itens_post_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ItensPostViewModel::class.java)



        arguments?.let {
            viewModel.categoryId.set(it.getInt(PageAdapter.CATEGORY_SLUG))
            viewModel.categoryTitle.set(it.getString(PageAdapter.CATEGORY_NAME))
        }


        binding.refreshLayout.setOnRefreshListener {
            ClearCacheUtil.deleteCache(activity!!)
            viewModel.refresh()
        }



        viewModel.itensPagedList.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "${it.size}")
            adapterItens = ItensPostAdapter(
                this@ItensPostFragment, viewModel.categoryTitle.get() ?: ""
            ) { viewModel.retry() }


            val layoutManager = LinearLayoutManager(activity)
            binding.rvItensPost.layoutManager = layoutManager


            binding.rvItensPost.adapter = adapterItens
            adapterItens?.submitList(it)
            binding.refreshLayout.isRefreshing = false

        })


        viewModel.getState().observe(viewLifecycleOwner, Observer {

            Log.d(TAG, it.toString())

            if (it == State.LOADING && (viewModel.listIsEmpty() || binding.refreshLayout.isRefreshing)) {
                binding.shimmerViewContainer.startShimmer()
                binding.errorLayout.errorLayout.visibility = View.GONE
                binding.rvItensPost.visibility = View.GONE
                binding.shimmerViewContainer.visibility = View.VISIBLE
            } else {
                binding.shimmerViewContainer.stopShimmer()
                binding.shimmerViewContainer.visibility = View.GONE
                if (it == State.ERROR && viewModel.listIsEmpty()) {
                    binding.errorLayout.errorLayout.visibility = View.VISIBLE
                } else {
                    binding.rvItensPost.visibility = View.VISIBLE
                    adapterItens?.setState(it ?: State.DONE)
                }
            }
        })


        binding.errorLayout.btnTryAgain.setOnClickListener {
            viewModel.retry()
        }

    }


    override fun onItemClick(postContent: Post) {
            postContent.slug?.let {
                PostDetailsActivity.openPostActivity(activity!!, it)
            }
    }

}
