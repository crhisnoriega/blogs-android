package com.mcontigo.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.mcontigo.R
import com.mcontigo.adapters.ItensPostAdapter
import com.mcontigo.adapters.PageAdapter
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.ItensPostsSideMenuFragmentBinding
import com.mcontigo.enums.State
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.viewmodel.ItensPostsSideMenuViewModel

class ItensPostsSideMenuFragment : Fragment(), PostContentRvInterface {

    private lateinit var viewModel: ItensPostsSideMenuViewModel
    private lateinit var binding: ItensPostsSideMenuFragmentBinding
    private val TAG = this@ItensPostsSideMenuFragment.javaClass.simpleName
    private lateinit var adapterItens: ItensPostAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.itens_posts_side_menu_fragment,
            container,
            false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ItensPostsSideMenuViewModel::class.java)



        arguments?.let {
            viewModel.categoryId.set(it.getInt(PageAdapter.CATEGORY_SLUG))
            viewModel.categoryTitle.set(it.getString(PageAdapter.CATEGORY_NAME))
        }

        viewModel.itensPagedList.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "${it.size}")
            adapterItens = ItensPostAdapter(
                this@ItensPostsSideMenuFragment, viewModel.categoryTitle.get() ?: ""
            ) { viewModel.retry() }

            binding.rvItensPost.adapter = adapterItens
            adapterItens.submitList(it)
        })


        viewModel.getState().observe(viewLifecycleOwner, Observer {

            Log.d(TAG, it.toString())

            if (it == State.LOADING && viewModel.listIsEmpty()) {
                binding.shimmerViewContainer.startShimmer()
                binding.errorLayout.errorLayout.visibility = View.GONE
                binding.rvItensPost.visibility = View.GONE
                binding.shimmerViewContainer.visibility = View.VISIBLE
            } else {
                binding.shimmerViewContainer.stopShimmer()
                binding.shimmerViewContainer.visibility = View.GONE
                if (it == State.ERROR && viewModel.listIsEmpty()) {
                    binding.errorLayout.errorLayout.visibility = View.VISIBLE
                } else {
                    binding.rvItensPost.visibility = View.VISIBLE
                    adapterItens.setState(it ?: State.DONE)
                }
            }
        })

        binding.errorLayout.btnTryAgain.setOnClickListener {
            viewModel.retry()
        }
    }

    override fun onItemClick(postContent: Post) {
        Log.d(TAG, postContent.toString())
        val directions =
            postContent.id?.let {
                ItensPostsSideMenuFragmentDirections.actionItensPostsSideMenuFragmentToPostDetailsActivity()
                    .setPostId(it)
                    .setCategoryName(viewModel.categoryTitle.get())
                    .setLinkImgThumbnail(postContent.featuredMedia?.large)


            }
        directions?.let { findNavController().navigate(it) }
    }

}
