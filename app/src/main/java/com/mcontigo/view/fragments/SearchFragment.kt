package com.mcontigo.view.fragments

import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.mcontigo.R
import com.mcontigo.adapters.ItensPostSearchAdapter
import com.mcontigo.adapters.SearchTagSuggestionAdapter
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.SearchFragmentBinding
import com.mcontigo.enums.State
import com.mcontigo.interfaces.ItemCategorySuggetionInterface
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.utils.CustomToolbarUtil
import com.mcontigo.utils.KeyboardUtil
import com.mcontigo.viewmodel.SearchViewModel
import com.mcontigo.utils.SpacesItemDecorator


class SearchFragment : Fragment(), PostContentRvInterface, ItemCategorySuggetionInterface {
    private val TAG = this@SearchFragment.javaClass.simpleName
    private lateinit var viewModel: SearchViewModel
    private lateinit var binding: SearchFragmentBinding
    private var adapterItens: ItensPostSearchAdapter? = null
    private lateinit var adapterSearchTagsSuggestions: SearchTagSuggestionAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.search_fragment, container, false)
        binding.lifecycleOwner = this
        binding.searchEditText.isFocusable = true
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)


        binding.toolbar.toolbarLogo.visibility = View.GONE
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.toolbar.appBarLayoutCustom.elevation = 4f
        }
        binding.toolbar.toolbarTitle.text = getString(R.string.search)

        binding.toolbar.imgButtonMenu.visibility = View.VISIBLE

        binding.toolbar.imgButtonMenu.setOnClickListener {
            CustomToolbarUtil.observerOpenDrawer.postValue(true)
        }

        viewModel.itensTagSuggestionPagedList.observe(viewLifecycleOwner, Observer {
            adapterSearchTagsSuggestions = SearchTagSuggestionAdapter(this@SearchFragment)
            binding.rvCategorias.adapter = adapterSearchTagsSuggestions

            val layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL)
            layoutManager.gapStrategy =
                StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
            binding.rvCategorias.layoutManager = layoutManager


            val decoration = SpacesItemDecorator(8)
            binding.rvCategorias.addItemDecoration(decoration)

            adapterSearchTagsSuggestions.submitList(it)
        })


        binding.imgClearSearch.setOnClickListener {
            binding.searchEditText?.text?.clear()
        }


        binding.searchEditText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                KeyboardUtil.hideKeyboard(activity!!)
            }

            true
        }

        binding.searchEditText.doOnTextChanged { text, start, count, after ->
            binding.rvCategorias.visibility = View.VISIBLE
            binding.progressBarPosts.visibility = View.VISIBLE
            Log.d(TAG, "$text")
            if (!text.isNullOrBlank()) {
                binding.rvCategorias.visibility = View.GONE
                viewModel.searchItens(text.toString())
                binding.imgClearSearch.visibility = View.VISIBLE

                viewModel.getStatePagingPosts().observe(viewLifecycleOwner, Observer { state ->
                    if (!text.isNullOrBlank()) {
                        adapterItens?.setState(state ?: State.DONE)

                        when (state) {
                            State.DONE -> {
                                binding.progressBarPosts.visibility = View.GONE
                                binding.rvItensSearchPost.visibility = View.VISIBLE
                            }
                            State.ERROR -> {
                                binding.progressBarPosts.visibility = View.GONE

                            }
                            State.LOADING -> {
                                if (viewModel.listIsEmpty()) {
                                    binding.progressBarPosts.visibility = View.VISIBLE
                                    binding.rvItensSearchPost.visibility = View.GONE
                                }

                            }
                        }

                    }
                })

                viewModel.itensPagedList.observe(viewLifecycleOwner, Observer {
                    Log.d(TAG, "${it.size}")
                    adapterItens =
                        ItensPostSearchAdapter(this@SearchFragment) { viewModel.retryPosts() }
                    binding.rvItensSearchPost.adapter = adapterItens
                    adapterItens?.submitList(it)
                })
            } else {
                binding.imgClearSearch.visibility = View.GONE
                binding.rvItensSearchPost.visibility = View.INVISIBLE
                binding.progressBarPosts.visibility = View.GONE
            }
        }
    }

    override fun onItemClick(itemPost: Post) {
        Log.d(TAG, itemPost.toString())
        val directions =
            itemPost.id?.let {
                HomeViewPagerFragmentDirections.actionHomeFragmentToPostDetailsActivity()
                    .setPostId(it)
                    .setLinkImgThumbnail(itemPost.featuredMedia?.large)
                    .setCategoryName(itemPost.categories?.get(0)?.name ?: "")
            }
        directions?.let { findNavController().navigate(it) }
    }

    override fun onSelectSuggestion(title: String, tagId: Int?) {
        viewModel.tagIdSelecionado = tagId
        viewModel.tagTitleSelecionado = title
        binding.searchEditText.setText(title)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
            binding.rvCategorias.visibility = View.VISIBLE
            binding.imgClearSearch.visibility = View.VISIBLE
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
            binding.rvCategorias.visibility = View.GONE
            binding.imgClearSearch.visibility = View.GONE
        }
    }

}
