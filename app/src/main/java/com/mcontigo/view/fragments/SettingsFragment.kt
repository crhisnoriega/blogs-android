package com.mcontigo.view.fragments

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.mcontigo.R
import com.mcontigo.utils.SharedPreferencesUtil
import com.mcontigo.viewmodel.SettingsViewModel
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.opengl.Visibility
import android.provider.Settings
import android.util.Log
import com.google.ads.consent.ConsentForm
import com.google.ads.consent.ConsentFormListener
import com.google.ads.consent.ConsentInformation
import com.google.ads.consent.ConsentStatus
import com.google.ads.mediation.admob.AdMobAdapter
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.mcontigo.BuildConfig
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.utils.UserUtil
import com.mcontigo.view.AboutUsActivity
import com.mcontigo.view.ChangeLocaleActivity
import com.mcontigo.view.ConnectUserActivity
import com.mcontigo.view.RegisterUserActivity
import com.mcontigo.view.UserProfileActivity
import com.mcontigo.view.dialogs.DialogQuestionFragment
import kotlinx.android.synthetic.main.activity_author.view.*
import java.net.MalformedURLException
import java.net.URL
import java.util.*

import android.content.Context
import android.content.res.Configuration 
import com.zeugmasolutions.localehelper.LocaleHelper 


class SettingsFragment : Fragment() {

    private lateinit var viewModel: SettingsViewModel
    private lateinit var binding: com.mcontigo.databinding.SettingsFragmentBinding
    private lateinit var sharedPreferencesUtil: SharedPreferencesUtil
    private var consentForm: ConsentForm? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.settings_fragment, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SettingsViewModel::class.java)
        binding.viewModel = viewModel

        binding.tvValueVersion.text = viewModel.versionBuild
        binding.toolbar.toolbarLogo.visibility = View.GONE
        binding.toolbar.toolbarTitle.visibility = View.VISIBLE
        binding.toolbar.imgButtonMenu.visibility = View.INVISIBLE
        binding.toolbar.appBarLayoutCustom.elevation = 4f
        binding.toolbar.toolbarTitle.text = getString(R.string.settings)

        if (ConfigurationLib.LOGIN_LOCALE_FILTER.contains(Locale.getDefault().language)
                && ConfigurationLib.LOGIN_FLAVOR_FILTER.contains(BuildConfig.FLAVOR)) {
            UserUtil.user.observe(viewLifecycleOwner, Observer {
                it?.also {
                    binding.buttonLogout.root.visibility = View.VISIBLE
                    binding.buttonCreateAccount.root.visibility = View.GONE
                    binding.buttonLogin.root.setOnClickListener {
                        UserProfileActivity.open(activity!!)
                    }

                    binding.buttonLogout.root.setOnClickListener {
                        DialogQuestionFragment.newInstance(
                            getString(R.string.lbl_signOff),
                            getString(R.string.lbl_msg_question_logout),
                            buttonLeftAction = {
                                viewModel.logoutUser()
                            },
                            buttonRightAction = {

                            }
                        ).show(fragmentManager!!, DialogQuestionFragment.DIALOG_TAG)
                    }
                    binding.buttonLogin.lblButton = it.email

                } ?: run {
                    binding.buttonCreateAccount.root.visibility = View.VISIBLE
                    binding.buttonLogout.root.visibility = View.GONE
                    binding.buttonLogin.root.setOnClickListener {
                        ConnectUserActivity.open(activity!!)
                    }
                    binding.buttonCreateAccount.root.setOnClickListener {
                        RegisterUserActivity.open(activity!!)
                    }
                    binding.buttonLogin.lblButton = getString(R.string.SettingsLogin)
                }
            })
        } else {
            binding.tvAccount.visibility = View.GONE
            binding.buttonLogin.root.visibility = View.GONE
            binding.buttonLogout.root.visibility = View.GONE
            binding.buttonCreateAccount.root.visibility = View.GONE
            binding.buttonLogin.root.visibility = View.GONE
        }

        binding.buttonAboutUs.root.setOnClickListener {
            AboutUsActivity.open(activity!!, binding.buttonAboutUs.tvButton.text.toString() ,getString(R.string.url) + getString(R.string.urlWeAre))
        }
        binding.buttonTermConditions.root.setOnClickListener {
            AboutUsActivity.open(activity!!, binding.buttonTermConditions.tvButton.text.toString() ,getString(R.string.url) + getString(R.string.urlTerm))
        }
        var locale = LocaleHelper.getLocale(activity!!).language
        var country = LocaleHelper.getLocale(activity!!).country
        binding.buttonChangeLocale.tvButton.setText(retrieveString(activity!!, R.string.app_name, locale, country))
        binding.buttonChangeLocale.root.setOnClickListener {
            val intent = Intent(activity!!, ChangeLocaleActivity::class.java)
            startActivity(intent)
        }
        
        

        val prefs = this.context?.getSharedPreferences("consentAds", 0)
        if (prefs?.getBoolean("LocationInEeaOrUnknown", false)!!) {
            binding.buttonConsentAds.root.visibility = View.GONE
        } else {
            binding.buttonConsentAds.root.setOnClickListener {
                loadConsentForm()
            }
        }

        viewModel.linkFacebook.observe(viewLifecycleOwner, Observer { url ->
            if (url.isEmpty())
                binding.instagram.visibility = View.GONE
            else
                binding.facebook.setOnClickListener {
                    val i = Intent(ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
        })

        viewModel.linkInstagram.observe(viewLifecycleOwner, Observer { url ->
            if (url.isEmpty())
                binding.instagram.visibility = View.GONE
            else
                binding.instagram.setOnClickListener {
                    val i = Intent(ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
        })
        viewModel.linkPinterest.observe(viewLifecycleOwner, Observer { url ->
            if (url.isEmpty())
                binding.instagram.visibility = View.GONE
            else
                binding.pinterest.setOnClickListener {
                    val i = Intent(ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
        })
        viewModel.linkYouTube.observe(viewLifecycleOwner, Observer { url ->
            if (url.isEmpty())
                binding.instagram.visibility = View.GONE
            else
                binding.youtube.setOnClickListener {
                    val i = Intent(ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
        })

    }
    
     private fun retrieveString(context: Context, resourceId: Int, locale: String, country: String) : String{
        var config = Configuration(context.getResources().getConfiguration());
        config.setLocale(Locale(locale, country));
        return context.createConfigurationContext(config).getText(resourceId).toString();
    }

    private fun loadConsentForm() {
        val privacyUrl: URL
        val mContext = this.context
        try {
            privacyUrl = URL(getString(R.string.url) + getString(R.string.urlTerm))
            consentForm = ConsentForm.Builder(mContext, privacyUrl)
                .withListener(object : ConsentFormListener() {

                    override fun onConsentFormLoaded() {
                        // Consent form loaded successfully.
                        Log.d("loadConsentForm", "onConsentFormLoaded")
                        showConsentForm()
                    }

                    override fun onConsentFormOpened() {
                        // Consent form was displayed.
                        Log.d("loadConsentForm", "onConsentFormOpened")
                    }

                    override fun onConsentFormClosed(
                        consentStatus: ConsentStatus,
                        userPrefersAdFree: Boolean
                    ) {
                        // Consent form was closed.
                        Log.d("loadConsentForm", "onConsentFormClosed")
                        when (consentStatus) {
                            ConsentStatus.PERSONALIZED -> setNonPersonalizedAds(false)
                            ConsentStatus.NON_PERSONALIZED -> setNonPersonalizedAds(true)
                            else -> setNonPersonalizedAds(true)
                        }
                    }

                    override fun onConsentFormError(errorDescription: String) {
                        // Consent form error.
                        Log.d("loadConsentForm", "onConsentFormError")
                        setNonPersonalizedAds(true)
                    }
                })
                .withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build()
            consentForm?.load()
        } catch (e: MalformedURLException) {

        }
    }

    private fun showConsentForm() {
        consentForm?.show()
    }

    private fun setNonPersonalizedAds(nonPersonalizedAds: Boolean) {
        val prefs = this.context?.getSharedPreferences("consentAds", 0)
        val editor = prefs?.edit()
        editor?.putBoolean("nonPersonalizedAds", nonPersonalizedAds)
        editor?.apply()
    }
}
