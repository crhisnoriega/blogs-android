package com.mcontigo.viewholders

import android.content.Context
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.ItemBookmarkLeftRecycleviewBinding
import com.mcontigo.databinding.ItemBookmarkRecycleviewBinding
import com.mcontigo.databinding.ItemPostImgRightRecycleviewBinding
import com.mcontigo.databinding.ItemPostRecycleviewBinding
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.HtmlUtil
import com.mcontigo.utils.SharePostUtil

class BookmarkLeftViewHolder(
    val binding: ItemBookmarkLeftRecycleviewBinding,
    val context: Context,
    val listener: PostContentRvInterface
) :
    RecyclerView.ViewHolder(binding.root) {

    var viewBackground: ConstraintLayout? = null
    var viewForeground: ConstraintLayout? = null
    var itemPost: Post? = null

    fun bind(itemPost: Post, categoryName: String? = null) {

        viewBackground = binding.clBgDelete
        viewForeground = binding.clCard

        categoryName?.let {
            binding.tvCategory.text = it
        }

        val encodedTitle = itemPost.title?.let { HtmlUtil.fromHtml(it) }

        binding.tvAbout.text = encodedTitle

        itemPost.link?.let { link ->
            binding.imgBtnShare.setOnClickListener {
                SharePostUtil.sharePost(context, link)
            }
        } ?: run {
            binding.imgBtnShare.visibility = View.GONE
        }

        binding.root.setOnClickListener {
            listener.onItemClick(itemPost)
        }

        itemPost.featuredMedia?.let {
            Glide.with(binding.root)
                .load(it.thumbnail).centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.ivArticle)
        } ?: run {
            binding.ivArticle.background = ContextCompat.getDrawable(context, android.R.color.darker_gray)
        }
    }
}