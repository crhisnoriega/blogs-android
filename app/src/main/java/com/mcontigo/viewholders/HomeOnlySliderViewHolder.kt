package com.mcontigo.viewholders

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.adapters.ItemPostSliderAdapter
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.HomeOnlySliderBinding
import com.mcontigo.interfaces.ItemPostRvInterface

class HomeOnlySliderViewHolder(
    val itemBinding: HomeOnlySliderBinding,
    val context: Context,
    val listener: ItemPostRvInterface
) :
    RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(list: List<PostContentHome>) {
        val listAdd = mutableListOf<PostContentHome>()
        for (i in 0 until list.size) listAdd.add(list[i])
        val adapter = ItemPostSliderAdapter(listAdd, listener)
        itemBinding.rvSlider.adapter = adapter
    }


}