package com.mcontigo.viewholders

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.adapters.HomeGridPostAdapter
import com.mcontigo.androidwpmodule.dao.Options
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.HomeGridSectionLayoutBinding
import com.mcontigo.interfaces.ItemPostRvInterface

class HomeSectionViewHolder(
    val itemBinding: HomeGridSectionLayoutBinding,
    val context: Context,
    val listener: ItemPostRvInterface,
    val postStyleDefault : Int = HomeGridPostAdapter.POST_STYLE_WITH_SPOTLIGHT_AND_HORIZONTAL_SCROLL
) :
    RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(
        pair: Pair<Options, List<PostContentHome>>
    ) {

        itemBinding.tvHeaderSection.text = pair.first.title
        val adapter = HomeGridPostAdapter(
            pair.second, listener,
            pair.first.postStyle?.toInt() ?: postStyleDefault,
            pair.first.contentType ?: HomeGridPostAdapter.CONTENT_TYPE_CATEGORY,
            pair.first.widgetType?.toInt() ?: HomeGridPostAdapter.WIDGET_DEFAULT
        )

        val style = pair.first.postStyle?.toInt() ?: HomeGridPostAdapter.POST_STYLE_WITH_SPOTLIGHT_AND_HORIZONTAL_SCROLL

        itemBinding.rvItens.adapter = adapter
    }


}