package com.mcontigo.viewholders

import android.content.Context
import android.text.TextUtils
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mcontigo.R
import com.mcontigo.adapters.ItemPostSliderAdapter
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.HomeGridSliderBinding
import com.mcontigo.interfaces.ItemPostRvInterface
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.HtmlUtil
import com.mcontigo.utils.SharePostUtil
import kotlinx.coroutines.Dispatchers

class HomeSliderViewHolder(
    val itemBinding: HomeGridSliderBinding,
    val context: Context,
    val listener: ItemPostRvInterface
) :
    RecyclerView.ViewHolder(itemBinding.root) {

    fun bind(list: List<PostContentHome>) {

        val itemPost = list[0]

        val encodedCategory = TextUtils.htmlEncode(itemPost.category?.get(0)?.name)
        val encodedTitle = itemPost.title?.let { HtmlUtil.fromHtml(it) }


        itemBinding.itemSpot.tvCategory.text = encodedCategory
        itemBinding.itemSpot.tvAbout.text = encodedTitle

        itemBinding.itemSpot.tvDescription.visibility = View.GONE

        itemPost.link?.let { link ->
            itemBinding.itemSpot.imgBtnShare.setOnClickListener {
                SharePostUtil.sharePost(context, link)
            }
        } ?: run {
            itemBinding.itemSpot.imgBtnShare.visibility = View.GONE
        }

        if(BookmarkUtil.isEnabled()){
            itemBinding.itemSpot.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                BookmarkUtil.toggleBookmark(context, itemPost)
                itemBinding.itemSpot.imgBtnBookmark.isEnabled = false
            })
            BookmarkUtil.getPostListIds().observe(context as LifecycleOwner, Observer {
                itemBinding.itemSpot.imgBtnBookmark.isEnabled = true
                if(it.contains(itemPost.id)){
                    itemBinding.itemSpot.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark))
                }else{
                    itemBinding.itemSpot.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark_off))
                }
            })
        }else{
            itemBinding.itemSpot.imgBtnBookmark.visibility = View.GONE
        }

        itemBinding.root.setOnClickListener {
            listener.onItemClick(itemPost)
        }

        itemPost.featuredMedia?.let {
            Glide.with(itemBinding.root)
                .load(it.large)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemBinding.itemSpot.ivArticle)
        } ?: run {
            itemBinding.itemSpot.ivArticle.background = ContextCompat.getDrawable(context, android.R.color.darker_gray)
        }

        val listAdd = mutableListOf<PostContentHome>()
        for (i in 1 until list.size) listAdd.add(list[i])
        val adapter = ItemPostSliderAdapter(listAdd, listener)
        itemBinding.rvSlider.adapter = adapter
    }


}