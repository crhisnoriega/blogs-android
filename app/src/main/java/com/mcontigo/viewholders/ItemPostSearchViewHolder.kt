package com.mcontigo.viewholders

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.ItemPostRecyclerviewSpotlightBinding
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.HtmlUtil
import com.mcontigo.utils.SharePostUtil

class ItemPostSearchViewHolder(
    val itemBinding: ItemPostRecyclerviewSpotlightBinding,
    val context: Context,
    val listener: PostContentRvInterface
) : RecyclerView.ViewHolder(itemBinding.root){
    fun bind(itemPost : Post){
        itemBinding.tvCategory.text = ""
        itemBinding.tvAbout.text = itemPost.title?.let { HtmlUtil.fromHtml(it) }


        itemPost.link?.let { link ->
            itemBinding.imgBtnShare.setOnClickListener {
                SharePostUtil.sharePost(context, link)
            }
        } ?: run {
            itemBinding.imgBtnShare.visibility = View.GONE
        }

        if(BookmarkUtil.isEnabled()){
            itemBinding.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                BookmarkUtil.toggleBookmark(context, itemPost)
                itemBinding.imgBtnBookmark.isEnabled = false
            })
            BookmarkUtil.getPostListIds().observe(context as LifecycleOwner, Observer {
                itemBinding.imgBtnBookmark.isEnabled = true
                if(it.contains(itemPost.id)){
                    itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark))
                }else{
                    itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark_off))
                }
            })
        }else{
            itemBinding.imgBtnBookmark.visibility = View.GONE
        }


        itemBinding.tvDescription.visibility = View.GONE


        itemBinding.root.setOnClickListener {
            listener.onItemClick(itemPost)
        }


        if(itemPost.categories?.isNotEmpty() == true)
        itemBinding.tvCategory.text = itemPost.categories?.get(0)?.name


        itemPost.featuredMedia?.let {
            Glide.with(itemBinding.root)
                .load(it.medium)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemBinding.ivArticle)
        } ?: run{
            itemBinding.ivArticle.background = ContextCompat.getDrawable(context,android.R.color.darker_gray)
        }

    }
}