package com.mcontigo.viewholders

import android.content.Context
import android.text.TextUtils
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.databinding.ItemPostRecyclerviewSpotlightBinding
import com.mcontigo.interfaces.ItemPostRvInterface
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.HtmlUtil
import com.mcontigo.utils.SharePostUtil


class ItemPostSpotlightViewHolder(
    val itemBinding: ItemPostRecyclerviewSpotlightBinding,
    val context: Context,
    val listener: ItemPostRvInterface
) :
    RecyclerView.ViewHolder(itemBinding.root) {
    fun bind(itemPost: PostContentHome, showText : Boolean = false) {

        val encodedCategory = TextUtils.htmlEncode(itemPost.category?.get(0)?.name)
        val encodedTitle = itemPost.title?.let { HtmlUtil.fromHtml(it) }


        itemBinding.tvCategory.text = encodedCategory
        itemBinding.tvAbout.text = encodedTitle

        if(showText){
            itemBinding.tvDescription.text = itemPost.excerpt?.let { HtmlUtil.fromHtml(it) }
            itemBinding.tvDescription.visibility = View.VISIBLE
        }
        else{
            itemBinding.tvDescription.visibility = View.GONE
        }


        itemPost.link?.let { link ->
            itemBinding.imgBtnShare.setOnClickListener {
                SharePostUtil.sharePost(context, link)
            }
        } ?: run {
            itemBinding.imgBtnShare.visibility = View.GONE
        }

        if(BookmarkUtil.isEnabled()){
            itemBinding.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                BookmarkUtil.toggleBookmark(context, itemPost)
                itemBinding.imgBtnBookmark.isEnabled = false
            })
            BookmarkUtil.getPostListIds().observe(context as LifecycleOwner, Observer {
                itemBinding.imgBtnBookmark.isEnabled = true
                if(it.contains(itemPost.id)){
                    itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark))
                }else{
                    itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark_off))
                }
            })
        }else{
            itemBinding.imgBtnBookmark.visibility = View.GONE
        }

        itemBinding.root.setOnClickListener {
            listener.onItemClick(itemPost)
        }


        itemPost.featuredMedia?.let {
            Glide.with(itemBinding.root)
                .load(it.large)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(itemBinding.ivArticle)
        } ?: run {
            itemBinding.ivArticle.background = ContextCompat.getDrawable(context, android.R.color.darker_gray)
        }


    }
}