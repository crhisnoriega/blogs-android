package com.mcontigo.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.R

class ItemSlideDescriptionViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    companion object {
        fun create(parent: ViewGroup): ItemSlideDescriptionViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_description_info_swipe, parent, false)
            return ItemSlideDescriptionViewHolder(view)
        }
    }
}