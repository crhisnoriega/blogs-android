package com.mcontigo.viewholders

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.databinding.ItemPostImgRightRecycleviewBinding
import com.mcontigo.databinding.ItemPostRecycleviewBinding
import com.mcontigo.interfaces.PostContentRvInterface
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.HtmlUtil
import com.mcontigo.utils.SharePostUtil

class PostContentDetailsViewHolder(
    val itemBinding: ViewDataBinding,
    val context: Context,
    val listener: PostContentRvInterface
) :
    RecyclerView.ViewHolder(itemBinding.root) {
    fun bind(itemPost: Post, categoryName: String? = null) {


        if (itemBinding is ItemPostRecycleviewBinding) {

            val binding = itemBinding as ItemPostRecycleviewBinding

            categoryName?.let {
                binding.tvCategory.text = it
            }

            val encodedTitle = itemPost.title?.let { HtmlUtil.fromHtml(it) }

            binding.tvAbout.text = encodedTitle

            itemPost.link?.let { link ->
                binding.imgBtnShare.setOnClickListener {
                    SharePostUtil.sharePost(context, link)
                }
            } ?: run {
                binding.imgBtnShare.visibility = View.GONE
            }
            if(BookmarkUtil.isEnabled()){
                binding.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                    BookmarkUtil.toggleBookmark(context, itemPost)
                    binding.imgBtnBookmark.isEnabled = false
                })
                BookmarkUtil.getPostListIds().observe(context as LifecycleOwner, Observer {
                    binding.imgBtnBookmark.isEnabled = true
                    if(it.contains(itemPost.id)){
                        itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark))
                    }else{
                        itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark_off))
                    }
                })
            }else{
                binding.imgBtnBookmark.visibility = View.GONE
            }

            binding.root.setOnClickListener {
                listener.onItemClick(itemPost)
            }


            itemPost.featuredMedia?.let {
                Glide.with(binding.root)
                    .load(it.thumbnail).centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.ivArticle)
            } ?: run {
                binding.ivArticle.background = ContextCompat.getDrawable(context, android.R.color.darker_gray)
            }
        } else if (itemBinding is ItemPostImgRightRecycleviewBinding) {
            val binding = itemBinding as ItemPostImgRightRecycleviewBinding

            categoryName?.let {
                binding.tvCategory.text = it
            }

            val encodedTitle = itemPost.title?.let { HtmlUtil.fromHtml(it) }

            binding.tvAbout.text = encodedTitle

            itemPost.link?.let { link ->
                binding.imgBtnShare.setOnClickListener {
                    SharePostUtil.sharePost(context, link)
                }
            } ?: run {
                binding.imgBtnShare.visibility = View.GONE
            }
            if(BookmarkUtil.isEnabled()){
                binding.imgBtnBookmark.setOnClickListener(View.OnClickListener {
                    BookmarkUtil.toggleBookmark(context, itemPost)
                    binding.imgBtnBookmark.isEnabled = false
                })
                BookmarkUtil.getPostListIds().observe(context as LifecycleOwner, Observer {
                    binding.imgBtnBookmark.isEnabled = true
                    if(it.contains(itemPost.id)){
                        itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark))
                    }else{
                        itemBinding.imgBtnBookmark.setImageDrawable(context.getDrawable(R.drawable.ic_bookmark_off))
                    }
                })
            }else{
                binding.imgBtnBookmark.visibility = View.GONE
            }

            binding.root.setOnClickListener {
                listener.onItemClick(itemPost)
            }


            itemPost.featuredMedia?.let {
                Glide.with(binding.root)
                    .load(it.thumbnail).centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.ivArticle)
            } ?: run {
                binding.ivArticle.background = ContextCompat.getDrawable(context, android.R.color.darker_gray)
            }
        }


    }
}