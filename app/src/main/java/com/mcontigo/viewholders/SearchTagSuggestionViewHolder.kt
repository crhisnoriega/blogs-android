package com.mcontigo.viewholders

import androidx.recyclerview.widget.RecyclerView
import com.mcontigo.androidwpmodule.dao.TagSuggestion
import com.mcontigo.databinding.ItemCategorySearchRecycleviewBinding
import com.mcontigo.interfaces.ItemCategorySuggetionInterface

class SearchTagSuggestionViewHolder(
    val itemBinding: ItemCategorySearchRecycleviewBinding
) :
    RecyclerView.ViewHolder(itemBinding.root) {
    fun bind(
        item: TagSuggestion,
        listener: ItemCategorySuggetionInterface
    ) {
        item.name?.let { name ->
            itemBinding.tvCategory.text = name
            itemBinding.root.setOnClickListener {
                listener.onSelectSuggestion(name, item.id)
            }
        }


    }
}