package com.mcontigo.viewholders


import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.ads.doubleclick.PublisherAdRequest
import com.google.android.gms.ads.doubleclick.PublisherAdView
import com.mcontigo.BuildConfig
import com.mcontigo.databinding.CardBannerExpressAdContainerBinding
import com.mcontigo.databinding.CardMultplesSizeBannerExpressAdContainerBinding


class UnifiedBannerAdViewHolder(view: CardBannerExpressAdContainerBinding, adRequest : PublisherAdRequest) : RecyclerView.ViewHolder(view.root) {

    init {
       val adView = if(BuildConfig.DEBUG) view.publisherAdViewDebug else  view.publisherAdView

        adView.visibility = View.VISIBLE

        // The MediaView will display a video asset if one is present in the ad, and the
        // first image asset otherwise.
        val mPublisherAdView: PublisherAdView = adView
        mPublisherAdView.loadAd(adRequest)

    }

}