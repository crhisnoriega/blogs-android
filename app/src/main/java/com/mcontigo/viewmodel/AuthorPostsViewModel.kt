package com.mcontigo.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.enums.State
import com.mcontigo.paging.datasources.PostAuthorDataSorce
import com.mcontigo.paging.factorys.PostAuthorDataSourceFactory
import com.mcontigo.utils.ConfigurationLib

class AuthorPostsViewModel : ViewModel(), LifecycleObserver {


    private val postEndPoints = PostsRepository(ConfigurationLib.baseUrl)
    var categoryId = ObservableField<Int>()
    var categoryTitle = ObservableField<String>()

    private lateinit var postDataSourceFactory: PostAuthorDataSourceFactory
    private val pageSize = 3
    lateinit var itensPagedList: LiveData<PagedList<Post>>

    fun getPosts(idAuthor: Int, reviewed: Boolean = false) {
        postDataSourceFactory =
            PostAuthorDataSourceFactory(
                postEndPoints,
                viewModelScope,
                idAuthor,
                reviewed
            )
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .setEnablePlaceholders(true)
            .build()
        itensPagedList = LivePagedListBuilder(postDataSourceFactory, config).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap<PostAuthorDataSorce,
            State>(postDataSourceFactory.postDataSourceLiveData, PostAuthorDataSorce::state)

    fun retry() {
        postDataSourceFactory.postDataSourceLiveData.value?.also {
            it.retry
        }
    }

    fun listIsEmpty(): Boolean {
        return itensPagedList.value?.isEmpty() ?: true
    }


    fun refresh(){
        postDataSourceFactory.postDataSourceLiveData.value?.invalidate()
    }


}