package com.mcontigo.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mcontigo.androidwpmodule.dao.post.Author
import com.mcontigo.androidwpmodule.repository.AuthorRepository
import com.mcontigo.utils.ConfigurationLib
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class AuthorViewModel : ViewModel(){

    private val TAG = this@AuthorViewModel.javaClass.simpleName
    private val authorRepositoryApi = AuthorRepository(ConfigurationLib.baseUrl)

    private val _author = MutableLiveData<Author>()
    val author get() = _author

    fun getAuthor(id : Int){
        viewModelScope.launch(Dispatchers.IO) {
            val author = authorRepositoryApi.getAuthor(id)
            author?.also {
                _author.postValue(it)
            } ?: run{

            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}