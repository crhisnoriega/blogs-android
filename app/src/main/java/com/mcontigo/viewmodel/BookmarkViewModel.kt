package com.mcontigo.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.communication.RetrofitProvider
import com.mcontigo.enums.BookmarkState
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.utils.UserUtil
import com.mcontigo.view.dialogs.CustomSnackbarDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class BookmarkViewModel(application: Application) : AndroidViewModel(application) {

    private val retrofitProvider = RetrofitProvider(ConfigurationLib.authUrl)
    private val postList = MutableLiveData<List<Post>>()
    private var lastRemovedBookmark: Post? = null
    private var page : Int = 1
    private var reachLastPage: Boolean = false
    private var status: BookmarkState = BookmarkState.OFF

    fun fetchBookmarks(nextPage:Boolean = false){
        if(status == BookmarkState.LOADING){
            return
        }
        status = BookmarkState.LOADING
        if(nextPage && reachLastPage) {
            status = BookmarkState.DONE
            return
        }else if(nextPage) {
            page++
        }else {
            page = 1
            reachLastPage = false
        }
        val params = hashMapOf(
            "page" to page.toString(),
            "per_page" to "10",
            "orderby" to "date"
        )
        viewModelScope.launch(Dispatchers.IO){
            val response
                    = retrofitProvider
                .bookmark()
                .find(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), params)
            if(response.isSuccessful){
                var list : ArrayList<Post> = ArrayList()
                if(nextPage){
                    postList.value?.let { list.addAll(it) }
                }
                response.body()?.let{ list.addAll(it) }
                if(response.body()?.size == 0){
                    reachLastPage = true
                }
                postList.postValue(list)
            }
            status = BookmarkState.DONE
        }
    }

    private fun undoLastPostRemove(context: Context){
        lastRemovedBookmark?.also {
            addBookmark(it, context)
        }
    }

    private fun addBookmark(post: Post, context: Context){
        val snackbarSuccess = CustomSnackbarDialog.newDialogBookmarkAdd(context)
        val snackbarError = CustomSnackbarDialog.newDialogBookmarkError(context)
        viewModelScope.launch(Dispatchers.IO){
            val response
                    = retrofitProvider
                .bookmark()
                .create(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), post)
            if(response.isSuccessful){
                var list : ArrayList<Post> = ArrayList()
                postList.value?.let { list.addAll(it) }
                list.add(post)
                postList.postValue(list)
                snackbarSuccess.show()
                BookmarkUtil.shouldUpdateBookmarkList()
            }else{
                CustomSnackbarDialog.setErrorCodeToDialog(snackbarError, response.code().toString()).show()
            }
        }
    }

    fun removeBookmark(index: Int, context: Context){
        lastRemovedBookmark = postList.value!![index]
        val id = lastRemovedBookmark?.id
        val snackbarSuccess = CustomSnackbarDialog.newDialogBookmarkRemoved(context, Runnable { undoLastPostRemove(context) })
        val snackbarError = CustomSnackbarDialog.newDialogBookmarkError(context)
        viewModelScope.launch(Dispatchers.IO){
            val response
                    = retrofitProvider
                .bookmark()
                .remove(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), id!!)
            if(response.isSuccessful){
                var list : ArrayList<Post> = postList.value as ArrayList<Post>
                list = list.filter { post -> post.id != id} as ArrayList<Post>
                postList.postValue(list)
                snackbarSuccess.show()
                BookmarkUtil.shouldUpdateBookmarkList()
            }else{
                CustomSnackbarDialog.setErrorCodeToDialog(snackbarError, response.code().toString()).show()
            }
        }
    }

    fun canLoadMore(): Boolean{
        return status == BookmarkState.DONE && !reachLastPage
    }

    fun getPostList(): LiveData<List<Post>> = postList
}
