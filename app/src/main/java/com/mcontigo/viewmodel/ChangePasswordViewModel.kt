package com.mcontigo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mcontigo.R
import com.mcontigo.androidauthmodule.body.NewPasswordBody
import com.mcontigo.androidauthmodule.callback.CallBack
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.utils.ResponseStateUI
import com.mcontigo.utils.SingleLiveEvent
import com.mcontigo.utils.StateUI

class ChangePasswordViewModel(application: Application) : AndroidViewModel(application){

    private val _authUtil = AuthUtil(application, ConfigurationLib.authUrl)

    val oldPassword = MutableLiveData<String>("")
    val newPassword = MutableLiveData<String>("")
    val repeatNewPassword = MutableLiveData<String>("")

    private val _stateUI = MutableLiveData<SingleLiveEvent<ResponseStateUI<String>>>()
    val stateUI: LiveData<SingleLiveEvent<ResponseStateUI<String>>> = _stateUI



    fun changePassword() {

        newPassword.value?.also {
            if (newPassword.value != repeatNewPassword.value){
                _stateUI.postValue(
                    SingleLiveEvent(
                        ResponseStateUI(
                            StateUI.FAIL,
                            messageResId = R.string.ValidationPasswordDoesNotMatch
                        )
                    )
                )

                return
            }
            else if (it.length <= 7){
                _stateUI.postValue(
                    SingleLiveEvent(
                        ResponseStateUI(
                            StateUI.FAIL,
                            messageResId = R.string.ValidationInvalidPassword
                        )
                    )
                )
            }
        }


        val newPasswordBody = NewPasswordBody(
            oldPassword.value ?: "", newPassword.value ?: ""
        )


        _authUtil.changePassword(
            ConfigurationLib.getHeaderWithAuthorizationToken(_authUtil.getTokenAccess() ?: ""),
            newPasswordBody,
            object : CallBack {
                override fun onBegin() {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(
                                StateUI.LOADING,
                                messageResId = R.string.waiting
                            )
                        )
                    )
                }

                override fun onSuccess(msg: String) {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(
                                StateUI.SUCCESS,
                                messageResId = R.string.ChangePasswordSucessMessage
                            )
                        )
                    )
                }

                override fun onFailed(error: String) {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(
                                StateUI.FAIL,
                                messageResId = R.string.ChangePasswordErrorMessage
                            )
                        )
                    )
                }

            })
    }

}