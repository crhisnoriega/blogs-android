package com.mcontigo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.mcontigo.R
import com.mcontigo.androidauthmodule.body.UserSignInBody
import com.mcontigo.androidauthmodule.callback.AuthCallBack
import com.mcontigo.androidauthmodule.model.UserResponse
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.utils.*

class ConnectUserViewModel(application: Application) : AndroidViewModel(application) {

    private val TAG = this@ConnectUserViewModel.javaClass.simpleName

    private val _stateUI = MutableLiveData<SingleLiveEvent<ResponseStateUI<String>>>()
    val stateUI: LiveData<SingleLiveEvent<ResponseStateUI<String>>> = _stateUI
    private val _authUtil = AuthUtil(getApplication(), ConfigurationLib.authUrl)


    val displayName = MutableLiveData<String>("")
    val email = MutableLiveData<String>("")
    val password = MutableLiveData<String>("")


    fun signInEmailAndPassword() {

        val user = UserSignInBody(email.value ?: "", password.value ?: "")

        val hashUser = hashMapOf(
            Pair("email", user.email),
            Pair("password", user.password)
        )

        _authUtil.userEmailAuth(ConfigurationLib.getHeaderDefault(), user, object :
            AuthCallBack {
            override fun onSuccess(userResponse: UserResponse) {
                UserUtil.user.postValue(userResponse.user)
                userResponse.token?.also {
                    UserUtil.userAuthToken = it
                }
                _stateUI.postValue(
                    SingleLiveEvent(
                        ResponseStateUI(
                            StateUI.SUCCESS
                        )
                    )
                )
            }

            override fun onFailed(error: String) {
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, message = error)))
            }

            override fun onBegin() {
                _stateUI.postValue(
                    SingleLiveEvent(
                        ResponseStateUI(
                            StateUI.LOADING,
                            messageResId = R.string.waiting
                        )
                    )
                )
            }
        })


    }



    fun authFacebook(accessToken: AccessToken) {
        _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.LOADING, messageResId = R.string.waiting)))
        LoginSocialNetworksUtil.authFacebook(
            _authUtil, accessToken
        ){userResponse ->
            userResponse?.token?.also {
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.SUCCESS, messageResId = R.string.UserSuccessfullyRegistered)))
                UserUtil.user.postValue(userResponse.user)
                UserUtil.userAuthToken = it

            }?: run{
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, messageResId = R.string.erro_login_facebook)))
            }
        }
    }

    fun googleAuth(googleSignInAccount : GoogleSignInAccount){
        _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.LOADING, messageResId = R.string.waiting)))
        LoginSocialNetworksUtil.googleAuth(_authUtil, googleSignInAccount){ userResponse ->
            userResponse?.token?.also {
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.SUCCESS, messageResId = R.string.UserSuccessfullyRegistered)))
                UserUtil.user.postValue(userResponse.user)
                UserUtil.userAuthToken = it

            }?: run{
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, messageResId = R.string.erro_login_facebook)))
            }
        }
    }

}
