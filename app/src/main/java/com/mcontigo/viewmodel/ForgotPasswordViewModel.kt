package com.mcontigo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mcontigo.R
import com.mcontigo.androidauthmodule.callback.CallBack
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.utils.ResponseStateUI
import com.mcontigo.utils.SingleLiveEvent
import com.mcontigo.utils.StateUI

class ForgotPasswordViewModel(application: Application) : AndroidViewModel(application){


    private val _stateUI = MutableLiveData<SingleLiveEvent<ResponseStateUI<String>>>()
    val stateUI: LiveData<SingleLiveEvent<ResponseStateUI<String>>> = _stateUI
    private val _authUtil = AuthUtil(getApplication(), ConfigurationLib.authUrl)

    val email = MutableLiveData<String>("")


    fun recovery(){
        email.value?.also {
            _authUtil.forgotPasswordMessage(ConfigurationLib.getHeaderDefault(), it, object : CallBack{
                override fun onBegin() {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(
                                StateUI.LOADING,
                                messageResId = R.string.waiting
                            )
                        )
                    )
                }

                override fun onSuccess(msg: String) {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(
                                StateUI.SUCCESS,
                                message = msg
                            )
                        )
                    )
                }

                override fun onFailed(error: String) {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(
                                StateUI.FAIL,
                                message = error
                            )
                        )
                    )
                }

            })
        }

    }



}