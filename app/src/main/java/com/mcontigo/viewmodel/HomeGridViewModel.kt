package com.mcontigo.viewmodel

import androidx.lifecycle.*
import com.mcontigo.androidwpmodule.dao.Options
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.repository.HomeRepository
import com.mcontigo.utils.ConfigurationLib
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeGridViewModel : ViewModel(), LifecycleObserver {

    private val TAG = this@HomeGridViewModel.javaClass.simpleName
    private val homeRepository = HomeRepository(ConfigurationLib.baseUrl)
    private val itensLiveData = MutableLiveData<List<Pair<Options, List<PostContentHome>>>>()
    private val loadingItens = MutableLiveData<Pair<Boolean, Boolean>>() // loadingItens, success
    private val listPostContent = mutableListOf<Pair<Options, List<PostContentHome>>>()


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        getPostHomeGridApi()
    }

    fun refresh() {
        loadingItens()
        listPostContent.clear()
        getPostHomeGridApi()
    }

    fun getPostHomeGridApi() {

        viewModelScope.launch(Dispatchers.IO) {

            val home = homeRepository.getHome()

            home?.also {
                it.forEach {
                    listPostContent.add(it)
                }
                itensLiveData.postValue(listPostContent)
                successLoading()

            } ?: run {
                failLoading()
            }

        }
    }

    fun loadingItens() {
        loadingItens.postValue(Pair(true, false))
    }

    fun failLoading() {
        loadingItens.postValue(Pair(false, false))
    }

    fun successLoading() {
        loadingItens.postValue(Pair(false, true))
    }

    fun getItensPost(): LiveData<List<Pair<Options, List<PostContentHome>>>> = itensLiveData

    fun getLoadingItens(): LiveData<Pair<Boolean, Boolean>> = loadingItens

}
