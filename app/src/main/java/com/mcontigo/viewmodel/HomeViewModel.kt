package com.mcontigo.viewmodel

import androidx.lifecycle.*
import com.mcontigo.androidwpmodule.dao.category.Category
import com.mcontigo.androidwpmodule.repository.CategoriesRepository
import com.mcontigo.utils.ConfigurationLib
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel(), LifecycleObserver {

    private val TAG = this@HomeViewModel.javaClass.simpleName
    private val categoriesRepository = CategoriesRepository(ConfigurationLib.baseUrl)
    private val loadingItens = MutableLiveData<Pair<Boolean, Boolean>>() // loadingItens, success
    private val categories = MutableLiveData<List<Category>>()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        getCategoriesApi()
    }


    fun getCategoriesApi() {

        viewModelScope.launch(Dispatchers.IO) {

            loadingItens()

            val listCategories = categoriesRepository.getCategories()

            listCategories?.also {
                categories.postValue(it)
            } ?: run {
                failLoading()
            }

        }
    }

    fun loadingItens() {
        loadingItens.postValue(Pair(true, false))
    }

    fun failLoading() {
        loadingItens.postValue(Pair(false, false))
    }

    fun successLoading() {
        loadingItens.postValue(Pair(false, true))
    }


    fun getLoadingItens(): LiveData<Pair<Boolean, Boolean>> = loadingItens
    fun getCategories(): LiveData<List<Category>> = categories

}
