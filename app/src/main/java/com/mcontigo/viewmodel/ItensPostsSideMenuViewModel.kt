package com.mcontigo.viewmodel

import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.enums.State
import com.mcontigo.paging.datasources.PostDataSorce
import com.mcontigo.paging.factorys.PostDataSourceFactory
import com.mcontigo.utils.ConfigurationLib

class ItensPostsSideMenuViewModel : ViewModel(), LifecycleObserver {

    private val TAG = this@ItensPostsSideMenuViewModel.javaClass.simpleName
    private val postRepository = PostsRepository(ConfigurationLib.baseUrl)
    var categoryId = ObservableField<Int>()
    var categoryTitle = ObservableField<String>()

    private lateinit var postDataSourceFactory: PostDataSourceFactory
    private val pageSize = 10
    lateinit var itensPagedList: LiveData<PagedList<Post>>


    init {
        categoryId.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                getPostsApi()
            }
        })
    }

    fun getPostsApi() {
        categoryId.get()?.let {
            postDataSourceFactory =
                PostDataSourceFactory(
                    postRepository,
                    viewModelScope,
                    it
                )
            val config = PagedList.Config.Builder()
                .setPageSize(pageSize)
                .setInitialLoadSizeHint(pageSize)
                .setEnablePlaceholders(true)
                .build()
            itensPagedList = LivePagedListBuilder<Int, Post>(postDataSourceFactory, config).build()
        }

    }


    fun getState(): LiveData<State> = Transformations.switchMap<PostDataSorce,
            State>(postDataSourceFactory.postDataSourceLiveData, PostDataSorce::state)

    fun retry() {
        postDataSourceFactory.postDataSourceLiveData.value?.also {
            it.retry
        }
    }

    fun listIsEmpty(): Boolean {
        return itensPagedList.value?.isEmpty() ?: true
    }


}
