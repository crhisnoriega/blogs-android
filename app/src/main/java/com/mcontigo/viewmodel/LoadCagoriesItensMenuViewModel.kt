package com.mcontigo.viewmodel

import android.app.Application
import androidx.lifecycle.*
import com.mcontigo.androidauthmodule.callback.CallBack
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.androidwpmodule.dao.menu.Item
import com.mcontigo.androidwpmodule.dao.menu.Menu
import com.mcontigo.androidwpmodule.repository.MenuRepository
import com.mcontigo.utils.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import com.mcontigo.R

class LoadCagoriesItensMenuViewModel(application: Application) : AndroidViewModel(application),
    LifecycleObserver {
    private val TAG = this@LoadCagoriesItensMenuViewModel.javaClass.simpleName
    private val menuRepository = MenuRepository(ConfigurationLib.baseUrl)
    private val MAIN_MENU = "main"
    private val itensMenu = MutableLiveData<List<Item>>()
    private val loadingItens = MutableLiveData<Pair<Boolean, Boolean>>() // loadingItens, success


    private val _stateUI = MutableLiveData<SingleLiveEvent<ResponseStateUI<String>>>()
    val stateUI: LiveData<SingleLiveEvent<ResponseStateUI<String>>> = _stateUI


    private val authUtil = AuthUtil(application, ConfigurationLib.authUrl)

    fun getMenu() {
        viewModelScope.launch {
            val listMenu = menuRepository.getMenus()
            listMenu?.also {
                loadItensMenu(it)
            } ?: run {
                delay(5000)
                getMenu()
            }
        }
    }


    private fun loadItensMenu(result: List<Menu>) {
        viewModelScope.launch {
            result.forEach {
                if (it.id == MAIN_MENU) {
                    val listItensMenu = menuRepository.getItemMenu(it.id!!)
                    listItensMenu?.also {
                        itensMenu.postValue(it.filter { it.type != "post_type" })
                    } ?: run {
                        getMenu()
                    }
                    return@forEach
                }
            }
        }

    }


    fun sendAuthenticationEmail() {
        authUtil.confirmUser(
            ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken),
            UserUtil.user?.value?.email ?: "",
            object : CallBack {
                override fun onBegin() {

                }

                override fun onSuccess(msg: String) {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(
                                StateUI.SUCCESS,
                                 messageResId = R.string.EmailConfirmationSent
                            )

                        )
                    )
                }

                override fun onFailed(error: String) {

                }

            }
        )
    }

    fun getItensMenu(): LiveData<List<Item>> = itensMenu


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        getMenu()
    }

}