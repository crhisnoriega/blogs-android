package com.mcontigo.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel

class MainHostViewModel : ViewModel(), LifecycleObserver {

    private val TAG = this@MainHostViewModel.javaClass.simpleName
//    private val wordPressUtil = WordPressUtil(ConfigurationLibsUrls.baseUrl)
//    private val MAIN_MENU = "main"
//    private val itensMenu = MutableLiveData<List<Item>>()
//
//    init {
//        wordPressUtil.getMenus(object : JsonArrayCallBack {
//            override fun onBegin() {
//                Log.d(TAG, "onBegin load menu")
//            }
//
//            override fun onSuccess(result: JsonArray) {
//                val compositeDisposable = CompositeDisposable()
//                compositeDisposable.add(
//                    JsonUtil.getMenus(result).subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .doOnTerminate { compositeDisposable.clear() }
//                        .subscribe {
//                            loadItensMenu(it)
//                        }
//                )
//            }
//
//            override fun onError(error: String) {
//                Log.e(TAG, "menu load error -> $error")
//            }
//
//        })
//    }
//
//
//    private fun loadItensMenu(result: List<Menu>) {
//
//        result.forEach {
//            if (it.location == MAIN_MENU) {
//                wordPressUtil.getItemMenu(it.location, object : JsonArrayCallBack {
//                    override fun onBegin() {
//                        Log.d(TAG, "onBegin load menu")
//                    }
//
//                    override fun onSuccess(result: JsonArray) {
//                        val compositeDisposable = CompositeDisposable()
//                        compositeDisposable.add(
//                            JsonUtil.getItemMenu(result).subscribeOn(Schedulers.io())
//                                .observeOn(AndroidSchedulers.mainThread())
//                                .doOnTerminate { compositeDisposable.clear() }
//                                .subscribe {
//                                    itensMenu.postValue(it)
//                                    MenuItensUtil.listItens = it
//                                })
//
//                    }
//
//                    override fun onError(error: String) {
//                        Log.e(TAG, "menu itens load error -> $error")
//                    }
//
//                })
//                return@forEach
//            }
//        }
//
//
//    }
//
//    fun getItensMenu(): LiveData<List<Item>> = itensMenu
}