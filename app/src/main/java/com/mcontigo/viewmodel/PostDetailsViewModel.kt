package com.mcontigo.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.mcontigo.R
import com.mcontigo.androidwpmodule.dao.Options
import com.mcontigo.androidwpmodule.dao.PostContentHome
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.androidwpmodule.util.JsonUtil
import com.mcontigo.communication.RetrofitProvider
import com.mcontigo.enums.BookmarkState
import com.mcontigo.enums.State
import com.mcontigo.utils.BookmarkUtil
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.utils.UserUtil
import com.mcontigo.view.dialogs.CustomSnackbarDialog
import io.sentry.Sentry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class PostDetailsViewModel(application: Application) : AndroidViewModel(application) {

    private val TAG = this@PostDetailsViewModel.javaClass.simpleName
    private val postRepository = PostsRepository(ConfigurationLib.baseUrl)
    private val post = MutableLiveData<Post>()
    private val bookmarkState = MutableLiveData<BookmarkState>()
    val postDiv = MutableLiveData<Post>()
    private val state = MutableLiveData<State>()
    private val itensLiveData = MutableLiveData<List<Pair<Options, List<PostContentHome>>>>()
    private val retrofitProvider = RetrofitProvider(ConfigurationLib.authUrl)

    var postId: Int? = null
    var thumbnailLink: String? = null
    var categoryName: String? = null
    var url: String? = null

    fun getPostDetails(postId: Int) {


        viewModelScope.launch(Dispatchers.IO) {

            state.postValue(State.LOADING)
            bookmarkState.postValue(BookmarkState.LOADING)

            val postContent = postRepository.getPostById(postId)

            postContent?.also {
                post.postValue(it)
                it.sidebars?.also {
                    val jsonRight = Gson().toJsonTree(it).asJsonObject
                    JsonUtil.getRecomendedArticles(jsonRight)?.also {
                        itensLiveData.postValue(it)
                    }
                    state.postValue(State.DONE)
                } ?: run {
                    state.postValue(State.DONE)
                }
            } ?: run {
                state.postValue(State.ERROR)
                Sentry.capture("try open post by id | post not found -> URL: $postId |  language: ${Locale.getDefault().getLanguage()} ")
            }


        }
    }

    fun getPostDetails(url: String) {

        viewModelScope.launch(Dispatchers.IO) {

            val slug = url.substringAfter(getApplication<Application>().getString(R.string.url))
                .substringAfter("/").substringBefore("/")

            state.postValue(State.LOADING)
            val postContent = postRepository.getPostBySlug(slug)

            postContent?.also {

                thumbnailLink = it.featuredMedia?.thumbnail
                categoryName = it.categories?.get(0)?.name
                post.postValue(it)
                Log.d(TAG, "onsuccess get post -> $slug")
                Log.d(TAG, "${it.sidebars}")
                it.sidebars?.also {
                    val jsonRight = Gson().toJsonTree(it).asJsonObject
                    JsonUtil.getRecomendedArticles(jsonRight)?.also {
                        itensLiveData.postValue(it)
                    }
                    state.postValue(State.DONE)

                } ?: run {
                    state.postValue(State.DONE)
                }

            } ?: run {
                state.postValue(State.ERROR)
                Sentry.capture("try open post with url (Push Notification) | not found -> URL: $url \n SLUG: $slug |  language: ${Locale.getDefault().getLanguage()}")
            }

        }

    }

    fun getPostDiv(postId: String) {


        viewModelScope.launch {

            val postContentDiv = postRepository.getPostById(postId.toInt())
            postContentDiv?.also {
                postDiv.postValue(it)
            }

        }
    }

    fun fetchBookmark(){
        if(UserUtil.user.value == null){
            bookmarkState.postValue(BookmarkState.SIGNOUT)
        }else{
            viewModelScope.launch(Dispatchers.IO){
                postId = post.value?.id
                val response
                        = retrofitProvider
                    .bookmark()
                    .find(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), postId!!)
                if(response.isSuccessful){
                    bookmarkState.postValue(BookmarkState.ON)
                }else if(response.code() == 404){
                    bookmarkState.postValue(BookmarkState.OFF)
                }else{
                    bookmarkState.postValue(BookmarkState.ERROR)
                }
            }
        }
    }

    fun setBookmark(active: Boolean, context: Context){
        if(active){
            addBookmark(context)
        }else{
            removeBookmark(context)
        }
    }

    private fun addBookmark(context:Context){
        viewModelScope.launch(Dispatchers.IO){
            val response
                    = retrofitProvider
                .bookmark()
                .create(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), post.value!!)
            if(response.isSuccessful){
                bookmarkState.postValue(BookmarkState.ON)
                withContext(Dispatchers.Main.immediate) {
                    CustomSnackbarDialog.newDialogBookmarkAdd(context).show()
                }
            }else{
                bookmarkState.postValue(BookmarkState.ERROR)
                withContext(Dispatchers.Main.immediate){
                    var snackbarError = CustomSnackbarDialog.newDialogBookmarkError(context)
                    CustomSnackbarDialog.setErrorCodeToDialog(snackbarError, response.code().toString()).show()
                }
            }
            BookmarkUtil.shouldUpdateBookmarkList()
        }
    }

    private fun removeBookmark(context:Context){
        viewModelScope.launch(Dispatchers.IO){
            postId = post.value?.id
            val response
                    = retrofitProvider
                .bookmark()
                .remove(ConfigurationLib.getHeaderWithAuthorizationToken(UserUtil.userAuthToken), postId!!)
            if(response.isSuccessful) {
                bookmarkState.postValue(BookmarkState.OFF)
                withContext(Dispatchers.Main.immediate) {
                    CustomSnackbarDialog.newDialogBookmarkRemoved(context, Runnable { setBookmark(true, context) }).show()
                }
            }else{
                bookmarkState.postValue(BookmarkState.ERROR)
                withContext(Dispatchers.Main.immediate){
                    var snackbarError = CustomSnackbarDialog.newDialogBookmarkError(context)
                    CustomSnackbarDialog.setErrorCodeToDialog(snackbarError, response.code().toString()).show()
                }
            }
            BookmarkUtil.shouldUpdateBookmarkList()
        }
    }

    fun getPostDiv(): LiveData<Post> = postDiv
    fun getPostLiveData(): LiveData<Post> = post
    fun getRecomendedPost(): LiveData<List<Pair<Options, List<PostContentHome>>>> = itensLiveData
    fun getBookmarkState(): LiveData<BookmarkState> = bookmarkState
    fun getState(): LiveData<State> = state

}
