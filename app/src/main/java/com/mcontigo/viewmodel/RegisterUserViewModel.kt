package com.mcontigo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.facebook.AccessToken
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.mcontigo.R
import com.mcontigo.androidauthmodule.body.UserSignUpBody
import com.mcontigo.androidauthmodule.callback.AuthCallBack
import com.mcontigo.androidauthmodule.model.UserResponse
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.utils.*

class RegisterUserViewModel(application: Application) : AndroidViewModel(application) {

    private val TAG = this@RegisterUserViewModel.javaClass.simpleName

    private val _stateUI = MutableLiveData<SingleLiveEvent<ResponseStateUI<String>>>()
    val stateUI: LiveData<SingleLiveEvent<ResponseStateUI<String>>> = _stateUI
    private val _authUtil = AuthUtil(getApplication(), ConfigurationLib.authUrl)


    val displayName = MutableLiveData<String>("")
    val email = MutableLiveData<String>("")
    val password = MutableLiveData<String>("")
    val passwordConfirmation = MutableLiveData<String>("")



    fun signUpEmailAndPassword() {

        val user = UserSignUpBody(
            displayName.value ?: "",
            email.value ?: "",
            password.value ?: ""
        )



        if(password.value.isNullOrEmpty() || passwordConfirmation.value.isNullOrEmpty() || email.value.isNullOrEmpty()){
            _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, messageResId = R.string.lbl_error_fill_fields)))
            return
        }


        if (passwordConfirmation.value != password.value) {
            _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, messageResId = R.string.ValidationPasswordDoesNotMatch)))
            return
        }


        _authUtil.userEmailRegister(
            ConfigurationLib.getHeaderDefault(),
            user,
            object : AuthCallBack{

                override fun onBegin() {
                    _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.LOADING, messageResId = R.string.waiting)))
                }

                override fun onSuccess(userResponse: UserResponse) {
                    _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.SUCCESS, messageResId = R.string.UserSuccessfullyRegistered)))
                }

                override fun onFailed(error: String) {
                    _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, message = error)))
                }
            })

    }

    fun googleAuth(googleSignInAccount : GoogleSignInAccount){
        _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.LOADING, messageResId = R.string.waiting)))
        LoginSocialNetworksUtil.googleAuth(_authUtil, googleSignInAccount){ userResponse ->
            userResponse?.token?.also {
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.SUCCESS, messageResId = R.string.UserSuccessfullyRegistered)))
                UserUtil.user.postValue(userResponse.user)
                UserUtil.userAuthToken = it

            }?: run{
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, messageResId = R.string.erro_login_facebook)))
            }
        }
    }


    fun authFacebook(accessToken: AccessToken) {
        _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.LOADING, messageResId = R.string.waiting)))
        LoginSocialNetworksUtil.authFacebook(
            _authUtil, accessToken
        ){userResponse ->
            userResponse?.token?.also {
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.SUCCESS, messageResId = R.string.UserSuccessfullyRegistered)))
                UserUtil.user.postValue(userResponse.user)
                UserUtil.userAuthToken = it

            }?: run{
                _stateUI.postValue(SingleLiveEvent(ResponseStateUI(StateUI.FAIL, messageResId = R.string.erro_login_facebook)))
            }
        }
    }
}
