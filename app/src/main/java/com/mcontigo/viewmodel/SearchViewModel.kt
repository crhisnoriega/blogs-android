package com.mcontigo.viewmodel

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.mcontigo.androidwpmodule.dao.TagSuggestion
import com.mcontigo.androidwpmodule.dao.post.Post
import com.mcontigo.androidwpmodule.repository.PostsRepository
import com.mcontigo.enums.State
import com.mcontigo.paging.datasources.PostSearchDataSource
import com.mcontigo.paging.datasources.TagSuggestionDataSource
import com.mcontigo.paging.factorys.PostSearchSourceFactory
import com.mcontigo.paging.factorys.TagSuggestionDataSourceFactory
import com.mcontigo.utils.ConfigurationLib

class SearchViewModel : ViewModel(), LifecycleObserver {

    private val TAG = this@SearchViewModel.javaClass.simpleName
    private val postRepository = PostsRepository(ConfigurationLib.baseUrl)

    var tagIdSelecionado: Int? = null
    var tagTitleSelecionado: String = ""

    private lateinit var postSearchDataSourceFactory: PostSearchSourceFactory
    private var tagSuggestionDataSourceFactory: TagSuggestionDataSourceFactory
    private val pageSize = 10
    private val config = PagedList.Config.Builder()
        .setPageSize(pageSize)
        .setInitialLoadSizeHint(pageSize)
        .setEnablePlaceholders(true)
        .build()


    lateinit var itensPagedList: LiveData<PagedList<Post>>
    var itensTagSuggestionPagedList: LiveData<PagedList<TagSuggestion>>


    init {

        val configTagPaged = PagedList.Config.Builder()
            .setPageSize(6)
            .setInitialLoadSizeHint(6)
            .setEnablePlaceholders(true)
            .build()
        tagSuggestionDataSourceFactory =
            TagSuggestionDataSourceFactory(postRepository, viewModelScope)
        itensTagSuggestionPagedList =
            LivePagedListBuilder<Int, TagSuggestion>(
                tagSuggestionDataSourceFactory,
                configTagPaged
            ).build()

    }

    fun searchItens(stringValue: String) {
        var stringSearch: String? = stringValue
        if (tagTitleSelecionado != stringValue) {
            tagIdSelecionado = null

        } else {
            stringSearch = null
        }


        Log.d("SEARCH", "${stringSearch} ,  $tagIdSelecionado")

        postSearchDataSourceFactory = PostSearchSourceFactory(
            postRepository,
            viewModelScope,
            stringSearch,
            tagIdSelecionado
        )
        itensPagedList =
            LivePagedListBuilder<Int, Post>(postSearchDataSourceFactory, config).build()
    }

    fun getStatePagingPosts(): LiveData<State> = Transformations.switchMap<PostSearchDataSource,
            State>(
        postSearchDataSourceFactory.postSearchDataSourceLiveData,
        PostSearchDataSource::state
    )

    fun getStatePagingTags(): LiveData<State> = Transformations.switchMap<TagSuggestionDataSource,
            State>(
        tagSuggestionDataSourceFactory.tagSuggestionDataSourceLiveData,
        TagSuggestionDataSource::state
    )


    fun retryTags() {
        tagSuggestionDataSourceFactory.tagSuggestionDataSourceLiveData.value?.also {
            it.retry
        }
    }


    fun retryPosts() {
        postSearchDataSourceFactory.postSearchDataSourceLiveData.value?.also {
            it.retry
        }
    }

    fun listIsEmpty(): Boolean {
        return itensPagedList?.value?.isEmpty() ?: true
    }


    override fun onCleared() {
        super.onCleared()
    }


}
