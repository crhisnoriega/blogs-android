package com.mcontigo.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.androidwpmodule.repository.SocialRepository
import com.mcontigo.utils.ConfigurationLib
import com.mcontigo.utils.UserUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SettingsViewModel(application: Application) : AndroidViewModel(application) {

    var versionBuild: String = ""
    private val socialRepository = SocialRepository(ConfigurationLib.baseUrl)
    private var authUtil = AuthUtil(application, ConfigurationLib.authUrl)


    val linkFacebook = MutableLiveData<String>()
    val linkYouTube = MutableLiveData<String>()
    val linkInstagram = MutableLiveData<String>()
    val linkPinterest = MutableLiveData<String>()

    init {
        val pinfo = application.packageManager.getPackageInfo(application.packageName, 0)
        versionBuild = pinfo.versionName

        viewModelScope.launch(Dispatchers.IO) {
            val socialNetworks = socialRepository.getSocialsNetwork()
            socialNetworks?.also {
                it.forEach {
                    when (it.title) {
                        "Facebook" -> linkFacebook.postValue(it.link)
                        "Instagram" -> linkInstagram.postValue(it.link)
                        "Pinterest" -> linkPinterest.postValue(it.link)
                        "YouTube" -> linkYouTube.postValue(it.link)
                    }
                }
            }
        }


    }


    fun logoutUser(){
        authUtil.logout()
        UserUtil.user.postValue(null)
    }


}
