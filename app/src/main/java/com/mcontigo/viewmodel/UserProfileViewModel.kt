package com.mcontigo.viewmodel

import android.app.Application
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.facebook.AccessToken
import com.google.gson.annotations.SerializedName
import com.mcontigo.R
import com.mcontigo.androidauthmodule.callback.CallBack
import com.mcontigo.androidauthmodule.model.Address
import com.mcontigo.androidauthmodule.model.Billing
import com.mcontigo.androidauthmodule.model.User
import com.mcontigo.androidauthmodule.util.AuthUtil
import com.mcontigo.utils.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.io.File
import javax.security.auth.callback.Callback

class UserProfileViewModel(application: Application) : AndroidViewModel(application) {

    private var user: User? = null
    private var authUtil = AuthUtil(application, ConfigurationLib.authUrl)

    private val _stateUI = MutableLiveData<SingleLiveEvent<ResponseStateUI<String>>>()
    val stateUI: LiveData<SingleLiveEvent<ResponseStateUI<String>>> = _stateUI


    val displayName = MutableLiveData<String>()
    val firstName = MutableLiveData<String>()
    val lastName = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val imgAvatar = MutableLiveData<Bitmap>()

    val city = MutableLiveData<String>()
    val country = MutableLiveData<String>()
    val province = MutableLiveData<String>()
    val street = MutableLiveData<String>()
    val zipCode = MutableLiveData<String>()

    init {
        user = UserUtil.user.value
        user?.also {
            displayName.postValue(it.displayName)
            firstName.postValue(it.firstName)
            lastName.postValue(it.lastName)
            email.postValue(it.email)
            city.postValue(it.address?.billing?.city)
            country.postValue(it.address?.billing?.country)
            province.postValue(it.address?.billing?.province)
            street.postValue(it.address?.billing?.street)
            zipCode.postValue(it.address?.billing?.zipCode)
          
            it.picture?.also {
                if (it.isNotEmpty()){
                    var bitmap = UserUtil.loadPicture(it, application)
                    Log.i("crhisn", "bitmap: " + bitmap)
                    imgAvatar.postValue(bitmap)
                }

            }
        }
    }


    fun updateUserInfo() {

        authUtil.getTokenAccess()?.also { token ->
            user?.also { user ->

                user.firstName = firstName.value
                user.lastName = lastName.value
                user.email = email.value
                user.displayName = displayName.value

                val biling = Billing()
                val address = Address()

                biling.city = city.value
                biling.country = country.value
                biling.province = province.value
                biling.street = street.value
                biling.zipCode = zipCode.value
                address.billing = biling
                user.address = address

                imgAvatar.value?.also {
                    val stream = ByteArrayOutputStream();
                    it.compress(Bitmap.CompressFormat.PNG, 100, stream)
                    val byteArray = stream.toByteArray()
                    // it.recycle()
                    user.picture = Base64.encodeToString(byteArray, Base64.DEFAULT)
                }



                _stateUI.postValue(
                    SingleLiveEvent(
                        ResponseStateUI(
                            StateUI.LOADING,
                            messageResId = R.string.waiting
                        )
                    )
                )

                updateInfos(token, user)
            }
        }


    }

    private fun updateInfos(token: String, user: User) {
        authUtil.updateUser(
            ConfigurationLib.getHeaderWithAuthorizationToken(token),
            user,
            object : CallBack {
                override fun onBegin() {
                }

                override fun onSuccess(msg: String) {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(StateUI.DONE)
                        )
                    )

                    UserUtil.user.postValue(user)
                }

                override fun onFailed(error: String) {
                    _stateUI.postValue(
                        SingleLiveEvent(
                            ResponseStateUI(StateUI.FAIL)
                        )
                    )
                }

            })
    }


    fun logout() {
        authUtil.logout()
        UserUtil.user.postValue(null)
        _stateUI.postValue(
            SingleLiveEvent(
                ResponseStateUI(StateUI.DONE)
            )
        )
    }


}