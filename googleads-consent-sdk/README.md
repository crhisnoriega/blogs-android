# googleads consent sdk

# Google Mobile Ads Consent SDK

Under the Google [EU User Consent
Policy](//google.com/about/company/consentstaging.html), you must make certain
disclosures to your users in the European Economic Area (EEA) and obtain their
consent to use cookies or