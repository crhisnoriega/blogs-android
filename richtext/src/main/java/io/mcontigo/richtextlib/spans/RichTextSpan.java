package io.mcontigo.richtextlib.spans;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import io.mcontigo.parcelable.DynamicParcelable;
import io.mcontigo.richtextlib.ui.RichContentView;
import io.mcontigo.richtextlib.ui.RichContentViewDisplay;
import io.mcontigo.richtextlib.util.Utils;

public interface RichTextSpan extends DynamicParcelable , View.OnAttachStateChangeListener {

    public  static  final  Parcelable.Creator<RichTextSpan> CREATOR  = new Parcelable.Creator<RichTextSpan>() {


        @Override
        public  RichTextSpan createFromParcel(Parcel source) {
            String type = source.readString();
            RichTextSpan obj = Utils.newInstance(type);
            obj.readFromParcel(source);
            return obj;
        }

        @Override
        public RichTextSpan[] newArray(int size) {
            return new RichTextSpan[size];
        }
    };

    int getType();
   /// void setType(int type);


    void onSpannedSetToView(RichContentView view);
    void onAttachedToWindow(RichContentViewDisplay view);
    void onDetachedFromWindow(RichContentViewDisplay view);



}
