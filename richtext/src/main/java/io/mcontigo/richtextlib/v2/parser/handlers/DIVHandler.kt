/*
 * Copyright (c) 2015. Roberto  Prato <https://github.com/robertoprato>
 *
 *  *
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */
package io.mcontigo.richtextlib.v2.parser.handlers

import com.google.gson.Gson
import com.google.gson.JsonObject
import io.mcontigo.DivUtil.postData
import io.mcontigo.DivUtil.postId
import io.mcontigo.richtextlib.v2.content.RichTextDocumentElement
import io.mcontigo.richtextlib.v2.parser.MarkupContext
import io.mcontigo.richtextlib.v2.parser.MarkupTag
import io.mcontigo.richtextlib.v2.parser.TagHandler
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class DIVHandler : TagHandler() {

    private val mProcessContent = true
    private val parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.IO
    private val scope = CoroutineScope(coroutineContext)

    override fun onTagOpen(
        context: MarkupContext,
        tag: MarkupTag,
        out: RichTextDocumentElement
    ) {


        val attributes = tag.attributes
        val elementClass = attributes.getValue("", "class")
        val elementId = attributes.getValue("", "id")
        val elementDataPost = attributes.getValue("", "data-post")
        if (elementClass.equals("mc-featured-container card recommended-post", ignoreCase = true)) {
            if (elementDataPost != null) {
                postData.postValue(
                    Gson().fromJson(
                        elementDataPost,
                        JsonObject::class.java
                    )
                )
            } else if (elementId != null) {
                scope.launch {
                    postId.postValue(elementId.substring(5))
                }
            }
        }
    }

    override fun processContent(): Boolean {
        return mProcessContent
    }

    override fun onTagClose(
        context: MarkupContext,
        tag: MarkupTag,
        out: RichTextDocumentElement
    ) {
    }
}