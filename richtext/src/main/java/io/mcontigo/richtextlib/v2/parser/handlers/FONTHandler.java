/*
 * Copyright (c) 2015. Roberto  Prato <https://github.com/robertoprato>
 *
 *  *
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package io.mcontigo.richtextlib.v2.parser.handlers;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.Spannable;
import android.text.TextUtils;

import io.mcontigo.richtextlib.v2.content.RichTextDocumentElement;
import io.mcontigo.richtextlib.spans.ForegroundColorSpan;
import io.mcontigo.richtextlib.spans.TextAppearanceSpan;
import io.mcontigo.richtextlib.v2.parser.MarkupContext;
import io.mcontigo.richtextlib.v2.parser.MarkupTag;
import io.mcontigo.richtextlib.v2.parser.TagHandler;

public class FONTHandler extends TagHandler {

    @Override
    public void onTagOpen(MarkupContext context, MarkupTag tag, RichTextDocumentElement out) {

        String color = tag.attributes.getValue("", "color");
        String face = tag.attributes.getValue("", "face");
        int len = out.length();
        out.setSpan(new Markers.Font(color, face), len, len, Spannable.SPAN_MARK_MARK);

    }

    @Override
    public void onTagClose(MarkupContext context, MarkupTag tag, RichTextDocumentElement out) {

        int len = out.length();
        Markers.Font f = out.getLastSpan(Markers.Font.class);
        int where = out.getSpanStart(f);

        out.removeSpan(f);

        if (where != len) {

            if (!TextUtils.isEmpty(f.mColor)) {
                if (f.mColor.startsWith("@")) {
                    Resources res = Resources.getSystem();
                    String name = f.mColor.substring(1);
                    int colorRes = res.getIdentifier(name, "color", "android");
                    if (colorRes != 0) {
                        ColorStateList colors = res.getColorStateList(colorRes);
                        out.setSpan(new TextAppearanceSpan(null, 0, 0, colors, null),
                                where, len,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                } else {
                    int c = Color.parseColor(f.mColor);
                    if (c != -1) {
                        out.setSpan(new ForegroundColorSpan(c | 0xFF000000),
                                where, len,
                                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }
            }
            out.setFontFamily(f.mFace, where, len);
        }


    }
}
